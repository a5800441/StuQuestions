'''
Script to fine-tune the models on the lecture transcripts and questions dataset
Created by: Adam Vere
Date: 06 June 2023

# This code was adaptaed from Ros et al.'s code: https://github.com/kevinros/INLG2022StudentQuestions/blob/main/transformers_mod/src/prefix.ipynb
'''

import sys
import os
import platform

currentOS = platform.system()

path = "" # CHANGE TO RELEVENT PATH (Path to main directory)
ModelsPath = path + "\Models" # Path to where models are stored

sys.path.insert(0, path)

import torch
torch.cuda.init()
from torch.utils.data import Dataset, DataLoader
import pandas as pd
from transformers.models.t5 import T5Tokenizer, T5ForConditionalGeneration
import metrics
import pickle
from pickle import load
import random
import numpy as np
#import spacy

metrics.main(path) # Pass directory to metrics 


#pip install spacy
#python -m spacy download en
# Not used
'''LINGUISTIC_FEATURES = True
nlp = None
if LINGUISTIC_FEATURES:
  nlp = spacy.load("en_core_web_sm")

def LinguisticFeatures(text, tokenizer):
  doc = nlp(text)

  pos_tags = ["<pos>"]
  ent_tags = ["<ner>"]
  morph_tags = ["<mor>"]
  lemm_tags = ["<lem>"]

  pos_tags += [token.pos_ for token in doc]
  lemm_tags += [token.lemma_ for token in doc]

  for token in doc:
    found = False
    label = None

  for token in doc:
    found = False
    label = None
    for ent in doc.ents:
      if token in ent:
        found = True
        label = ent.label_
        break

    if found:
        ent_tags.append(label)
    else:
      ent_tags.append("<pad>")

    if token.morph.get("Tense") != []:
      morph_tags.append(token.morph.get("Tense")[0])
    else:
      morph_tags.append("<pad>")

  # Define special tokens
  special_tokens = {
    "additional_special_tokens": ['<pos>', '<ner>', '<mor>', '<lem>']
  }

  # Add special tokens to the tokenizer's vocabulary
  num_added_tokens = tokenizer.add_special_tokens(special_tokens)

  all_tags = " ".join(pos_tags[:64] + ent_tags[:64] + lemm_tags[:64] + morph_tags[:64])

  linguistic_features = tokenizer.batch_encode_plus([all_tags], skip_special_tokens=True, pad_to_max_length=False, truncation=True, return_tensors='pt')
  linguistic_features["attention_mask"][0] = torch.zeros(linguistic_features["input_ids"][0].size(dim=0))

  return linguistic_features
'''

'''
Dataset class
dataframe = dataset
tokenizer = T5tokenizer used to tokenize both the source and target input
source_len = max context length in tokens
target_len = max question length in tokens
source_text = key for context ('context', 'document')
target_text = key for question ('question', 'query')
'''
# If running with prefix fine-tuning, make sure to update the source mask below to reflect the length of the prefix (currently set to 100)
class DataSet(Dataset):
  def __init__(self, dataframe, tokenizer, source_len, target_len, source_text, target_text):
    self.tokenizer = tokenizer
    self.data = dataframe
    self.source_len = source_len
    self.summ_len = target_len
    self.target_text = self.data[target_text] # Gold Question
    self.source_text = self.data[source_text] # Context

  def __len__(self):
    return len(self.target_text)

  def __getitem__(self, index):
    source_text = str(self.source_text[index])
    target_text = str(self.target_text[index])

    source_text = ' '.join(source_text.split())
    target_text = ' '.join(target_text.split())

    source = self.tokenizer.batch_encode_plus([source_text], max_length= self.source_len, pad_to_max_length=True, truncation=True, padding="max_length", return_tensors='pt')
    target = self.tokenizer.batch_encode_plus([target_text], max_length= self.summ_len, pad_to_max_length=True, truncation=True, padding="max_length", return_tensors='pt')

    '''if LINGUISTIC_FEATURES:
      source_linguistic_features = LinguisticFeatures(source_text, self.tokenizer)
      target_linguistic_features = LinguisticFeatures(target_text, self.tokenizer)

      input_ids = torch.cat([source['input_ids'][0], source_linguistic_features['input_ids'][0]], dim=0)
      attention_mask = torch.cat([source['attention_mask'][0], source_linguistic_features['attention_mask'][0]], dim=0)

      source['input_ids'] = input_ids
      source['attention_mask'] = attention_mask

      self.source_len = source['input_ids'][0].size

      input_ids = torch.cat([target['input_ids'][0], target_linguistic_features['input_ids'][0]], dim=0)
      attention_mask = torch.cat([target['attention_mask'][0], target_linguistic_features['attention_mask'][0]], dim=0)

      target['input_ids'] = input_ids
      target['attention_mask'] = attention_mask'''

    source_ids = source['input_ids'].squeeze()
    source_mask = source['attention_mask'].squeeze()
    source_mask = torch.ones(100 + self.source_len)  # UPDATE FOR PREFIX FINE TUNING
    target_ids = target['input_ids'].squeeze()
    target_mask = target['attention_mask'].squeeze()

    return {
        'source_ids': source_ids.to(dtype=torch.long), 
        'source_mask': source_mask.to(dtype=torch.long), 
        'target_ids': target_ids.to(dtype=torch.long),
        'target_ids_y': target_ids.to(dtype=torch.long)
    }

'''
Function to train the model one epoch
epoch = current epoch
path = path to results file
'''
def train(epoch, tokenizer, model, device, loader, optimizer, path):
  model.train()

  for _,data in enumerate(loader, 0):
    y = data['target_ids'].to(device, dtype = torch.long)
    y_ids = y[:, :-1].contiguous()
    lm_labels = y[:, 1:].clone().detach()
    lm_labels[y[:, 1:] == tokenizer.pad_token_id] = -100
    ids = data['source_ids'].to(device, dtype = torch.long)
    mask = data['source_mask'].to(device, dtype = torch.long)

    outputs = model(input_ids = ids, attention_mask = mask, decoder_input_ids=y_ids, labels=lm_labels)
    loss = outputs[0]

    # Log results, current epoch and loss - every 100 samples
    if _%100==0:
      print(str(epoch+1) + " " + str(_) + " " + str(loss))
      file = open(path + "results.txt", "a")
      file.write('\n' + str(epoch+1) + " " + str(_) + " " + str(loss) + '\n')
      file.close()

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

'''
Function to run validation on the model
epoch = current epoch
fold_idx = current fold index for cross validation
path = path to results file
'''
def validation(epoch, fold_idx, tokenizer, model, device, loader, path):
  model.eval()

  with torch.no_grad():
    total_loss = 0
    for _,data in enumerate(loader, 0):
        y = data['target_ids'].to(device, dtype = torch.long)
        y_ids = y[:, :-1].contiguous()
        lm_labels = y[:, 1:].clone().detach()
        lm_labels[y[:, 1:] == tokenizer.pad_token_id] = -100
        ids = data['source_ids'].to(device, dtype = torch.long)
        mask = data['source_mask'].to(device, dtype = torch.long)

        outputs = model(input_ids = ids, attention_mask = mask, decoder_input_ids=y_ids, labels=lm_labels)
        total_loss += float(outputs[0])
    
    # Log results
    total_loss /= len(loader)
    print({'epoch': epoch, 'fold_idx': fold_idx, 'val_loss': total_loss})
    file = open(path + "results.txt", "a")
    file.write('\nValidation: ' + str(epoch+1) + " " + str(fold_idx) + " " + str(total_loss) + '\n')
    file.close()

    return total_loss

'''
Function to test the model
'''
def test(tokenizer, model, device, loader):
  model.eval()
  predictions = []
  actuals = []

  with torch.no_grad():
      for _, data in enumerate(loader, 0):
          y = data['target_ids'].to(device, dtype = torch.long)
          ids = data['source_ids'].to(device, dtype = torch.long)
          mask = data['source_mask'].to(device, dtype = torch.long)

          generated_ids = model.generate(
              input_ids = ids,
              attention_mask = mask, 
              max_length=150, 
              num_beams=2,
              repetition_penalty=2.5, 
              length_penalty=1.0, 
              early_stopping=True
              )
          
          # Decode tokenized generated and gold question
          preds = [tokenizer.decode(g, skip_special_tokens=True, clean_up_tokenization_spaces=True) for g in generated_ids]
          target = [tokenizer.decode(t, skip_special_tokens=True, clean_up_tokenization_spaces=True)for t in y]

          predictions.extend(preds)
          actuals.extend(target)

  # Return generated and gold questions       
  return predictions, actuals

'''
Function to fine-tune the model
test_pairs = dataset of lecture transcript and questions
val_idx = cross validation index
source_text = key for source text
target_text = key for target text
model_params = model paramters
mode = tune for cross validation, test for regular fine-tuning
outputPath = path to output folder
'''
def FineTune(test_pairs, val_idx, source_text, target_text, model_params, device, mode="tune", outputPath=""):
  modelPath = ModelsPath + "/" + model_params["MODEL"] # Path to model folder
  
  # Set random seeds and deterministic pytorch for reproducibility
  torch.manual_seed(model_params["SEED"]) # pytorch random seed
  np.random.seed(model_params["SEED"]) # numpy random seed
  torch.backends.cudnn.deterministic = True

  print(f"""[Model]: Loading {model_params["MODEL"]}...\n""")

  # tokenzier for encoding the text
  tokenizer = T5Tokenizer.from_pretrained(modelPath, local_files_only=True)
  modelInfo = None

  # Defining the model. We are using t5-base model and added a Language model layer on top for generation of Summary. 
  # Further this model is sent to device (GPU/TPU) for using the hardware.
  if model_params["MODEL"] == 'doc2query-t5-base-msmarco' or model_params["MODEL"] == 't5-base':
    model = T5ForConditionalGeneration.from_pretrained(modelPath, local_files_only=True)
  else:
    if model_params["EPOCH_SETTING"] == "":
      modelInfo = torch.load(modelPath + "/" + model_params["MODEL"] + '.pth')
    else:
      modelInfo = torch.load(modelPath + "/" + model_params["MODEL"] + "_" + model_params["EPOCH_SETTING"] + '.pth')
    model = T5ForConditionalGeneration.from_pretrained(pretrained_model_name_or_path=None, config=modelPath + "/" + "config.json", state_dict=modelInfo["model_state_dict"], local_files_only=True)
  
  model = model.to(device)
  del modelInfo
  
  # for prefix tuning (comment out for regular fine tuning)
  model._modules.requires_grad=False
  model._modules['encoder'].embedding.requires_grad=True
  model._modules['encoder'].linear1.requires_grad=True
  model._modules['encoder'].linear2.requires_grad=True
  
  # Defining the optimizer that will be used to tune the weights of the network in the training session. 
  optimizer = torch.optim.Adam(params=filter(lambda p: p.requires_grad, model.parameters()),
                               lr=model_params["LEARNING_RATE"])

  print(f"[Data]: Reading data...\n")

  shuff_lec_names = list(test_pairs.keys())
  random.shuffle(shuff_lec_names)

  # Create test test
  test_questions = []
  test_lecture_text = []
  for lecture in shuff_lec_names[85:]:
    for question_unit in test_pairs[lecture]:
        test_questions.append(question_unit['text'])
        test_lecture_text.append(question_unit['lecture_text'])
  test_dataset = pd.DataFrame({'lecture': test_lecture_text, 'question': test_questions})

  # Create validation set
  val_questions = []
  val_lecture_text = []
  for lecture in shuff_lec_names[val_idx:val_idx+10]:
    for question_unit in test_pairs[lecture]:
        val_questions.append(question_unit['text'])
        val_lecture_text.append(question_unit['lecture_text'])
  val_dataset = pd.DataFrame({'lecture': val_lecture_text, 'question': val_questions})

  # Create train set
  train_questions = []
  train_lecture_text = []
  for lecture in shuff_lec_names[0:val_idx] + shuff_lec_names[val_idx+10:85]:
    for question_unit in test_pairs[lecture]:
        train_questions.append(question_unit['text'])
        train_lecture_text.append(question_unit['lecture_text'])
  train_dataset = pd.DataFrame({'lecture': train_lecture_text, 'question': train_questions})

  if mode == "test":
      # merge val and train data sets
      train_questions = train_questions + val_questions
      train_lecture_text = train_lecture_text + val_lecture_text
      train_dataset = pd.DataFrame({'lecture': train_lecture_text, 'question': train_questions})

  print(f"TRAIN Dataset: {train_dataset.shape}")
  print(f"VAL Dataset: {val_dataset.shape}")
  print(f"TEST Dataset: {test_dataset.shape}\n")

  # Creating the Training and Validation dataset for further creation of Dataloader
  training_set = DataSet(train_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], source_text, target_text)
  val_set = DataSet(val_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], source_text, target_text)
  test_set = DataSet(test_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], source_text, target_text)

  # Defining the parameters for creation of dataloaders
  train_params = {
      'batch_size': model_params["TRAIN_BATCH_SIZE"],
      'shuffle': True,
      'num_workers': 0,
      }

  val_params = {
      'batch_size': model_params["VALID_BATCH_SIZE"],
      'shuffle': False,
      'num_workers': 0,
      }

  test_params = {
      'batch_size': model_params["TEST_BATCH_SIZE"],
      'shuffle': False,
      'num_workers': 0,
      }

  # Creation of Dataloaders for testing and validation. This will be used down for training and validation stage for the model.
  training_loader = DataLoader(training_set, **train_params)
  val_loader = DataLoader(val_set, **val_params)
  test_loader = DataLoader(test_set, **test_params)
  del train_dataset
  del val_dataset
  del test_dataset

  print(f'[Initiating Fine Tuning]...\n')

  prev_loss = []
  prev_rouge1f = []
  prev_rouge2f = []
  prev_rouge1lf = []

  configuration = {"python version": sys.version, "OS" : currentOS, "pytorch": torch.__version__}

  # Create results text file and save model paramters and configuration
  if os.path.exists(outputPath + 'results.txt') == False:
    file = open(outputPath + "results.txt", "w")
    file.write(str(configuration) + "\n")
    file.write(str(model_params) + "\n")
    file.close()

  # Training loop
  for epoch in range(model_params["TRAIN_EPOCHS"]):
      # Train one epoch
      train(epoch, tokenizer, model, device, training_loader, optimizer, outputPath)

      if mode == "test":
          continue
      
      # Cross validation
      loss = validation(epoch, val_idx, tokenizer, model, device, val_loader, outputPath)
      preds, acts = test(tokenizer, model, device, val_loader)
      rouge1_f, rouge2_f, rougeL_f = metrics.rougeScore(preds, acts, 'val', outputPath)

      prev_loss.append(loss)
      prev_rouge1f.append(rouge1_f['fmeasure'])
      prev_rouge2f.append(rouge2_f['fmeasure'])
      prev_rouge1lf.append(rougeL_f['fmeasure'])

      # Early stopping
      if len(prev_rouge1f) > 2 and prev_rouge1f[-1] < prev_rouge1f[-2] and prev_rouge1f[-2] < prev_rouge1f[-3]:
          break
  
  # Run on test set and fenerate automatic metrics, save model parameters
  if mode == "test":
      preds, acts = test(tokenizer, model, device, test_loader)
      metrics.rougeScore(preds, acts, 'test', outputPath)
      metrics.bleuScore(preds, acts, 'test', outputPath)
      metrics.meteorScore(preds, acts, 'test', outputPath)
      metrics.gleuScore(preds, acts, 'test', outputPath)

      pickle.dump(preds, open(outputPath + "/" + model_params["NAME"] + "_preds.pkl", 'wb'))
      pickle.dump(acts, open(outputPath + "/" + model_params["NAME"] + "_acts.pkl", 'wb'))

      torch.save({"model_state_dict": model.state_dict(), 'optimizer_state_dict': optimizer.state_dict()}, outputPath + model_params["NAME"] + ".pth")
      tokenizer.save_pretrained(outputPath)
      model.save_pretrained(outputPath)
  else:
    preds, acts = test(tokenizer, model, device, test_loader)
    metrics.rougeScore(preds, acts, 'test', outputPath)
  
  del model
  if mode == "tune":
    return max(prev_rouge1f), max(prev_rouge2f), max(prev_rouge1lf)
  else:
    return
  
def main():
  random.seed(42)

  device = 'cuda' if torch.cuda.is_available() else 'cpu'
  
  test_pairs = load(open(path + '/data/text_pairs.pkl', 'rb')) # Get lecutre-question data
  
  # changes to the prefix model should be made at transformers_mod/src/transformers/models/t5/modeling_t5.py lines 841-852 and 953-965
  # and the model_params should be updated to reflect the changes

  mode = "test"
  model_params={
      "MODEL":"t5-base",            # Base-model (i.e. the name of the models' folder in the Models directory)
      "EPOCH_SETTING":"",           # Epoch setting of the model, e.g. 5, 10, 15, 20, 25
      "NAME":"",                    # Name of the model
      "TRAIN_BATCH_SIZE":1,          # training batch size
      "VALID_BATCH_SIZE":1,          # validation batch size
      "TEST_BATCH_SIZE":1,           # validation batch size
      "TRAIN_EPOCHS":7,              # number of training epochs
      "VAL_EPOCHS":1,                # number of validation epochs
      "LEARNING_RATE":1e-5,          # learning rate
      "MAX_SOURCE_TEXT_LENGTH":512,  # max length of source text
      "MAX_TARGET_TEXT_LENGTH":64,   # max length of target text
      "SEED": 42,                    # set seed for reproducibility 
      "type": 'prefix',
      "mode": mode,
      "input_dim": "768*20",
      "hidden": "800",
      "output_dim": "768*100",
      "DESC": 'Test'
  }

  outputPath = path + "/output/" + model_params["NAME"] # Path to output folder

  if os.path.exists(outputPath) == False:
    os.mkdir(outputPath)

  # Cross validation
  if mode == "tune":
      all_f1s_1 = []
      all_f1s_2 = []
      all_f1s_L = []
      for fold_idx in [0,10,20]:
          f1, f1_2, f1_L = FineTune(test_pairs=test_pairs, val_idx=fold_idx,
              source_text="lecture", target_text="question", model_params=model_params, device=device, outputPath=outputPath)
          all_f1s_1.append(f1)
          all_f1s_2.append(f1_2)
          all_f1s_L.append(f1_L)
      
      # Calcuate and save average ROUGE scores
      avg_f1_1 = sum(all_f1s_1) / 3
      print(avg_f1_1)
      avg_f1_2 = sum(all_f1s_2) / 3
      print(avg_f1_2)
      avg_f1_L = sum(all_f1s_L) / 3
      print(avg_f1_L)
      file = open(outputPath + "results.txt", "a") 
      file.write('\n' + 'avg_f1_1: ' + str(avg_f1_1) + '\n')
      file.write('\n' + 'avg_f1_2: ' + str(avg_f1_2) + '\n')
      file.write('\n' + 'avg_f1_L: ' + str(avg_f1_L) + '\n')
      file.close()

  # Regular finetuning
  elif mode == "test":
      FineTune(test_pairs=test_pairs, val_idx=0,
              source_text="lecture", target_text="question", model_params=model_params, device=device, mode=mode, outputPath=outputPath)

if __name__ == "__main__":     
    main()