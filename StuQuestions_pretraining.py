'''
Script to train ('pre-train') t5-base and docT5query on the three datasets
Created by: Adam Vere
Date: 18 June 2023

# This code was adaptaed from Ros et al.'s code: https://github.com/kevinros/INLG2022StudentQuestions/blob/main/transformers_mod/src/prefix.ipynb
'''

import sys
import os
from os import walk
import platform

currentOS = platform.system()

path = "" # CHANGE TO RELEVENT PATH (Path to main directory)

sys.path.insert(0, path)

import torch
from torch.utils.data import Dataset, DataLoader
import pandas as pd
#from transformers.models.t5 import T5Tokenizer, T5ForConditionalGeneration
from transformers.models.t5_alt import T5Tokenizer, T5ForConditionalGeneration # Without prefix-tuning
import random
import numpy as np
import metrics
import json
import pickle

metrics.main(path)

'''
Dataset class
dataframe = dataset
tokenizer = T5tokenizer used to tokenize both the source and target input
source_len = max context length in tokens
target_len = max question length in tokens
source_text = key for context ('context', 'document')
target_text = key for question ('question', 'query')
'''
class DataSet(Dataset):
  def __init__(self, dataframe, tokenizer, source_len, target_len, source_text, target_text):
    self.tokenizer = tokenizer
    self.data = dataframe
    self.source_len = source_len
    self.summ_len = target_len
    self.target_text = self.data[target_text] # Gold Question
    self.source_text = self.data[source_text] # Context

  def __len__(self):
    return len(self.target_text)

  def __getitem__(self, index):
    source_text = str(self.source_text[index])
    target_text = str(self.target_text[index])

    source_text = ' '.join(source_text.split())
    target_text = ' '.join(target_text.split())
    
    source = self.tokenizer.batch_encode_plus([source_text], max_length= self.source_len, pad_to_max_length=True, truncation=True, padding="max_length", return_tensors='pt')
    target = self.tokenizer.batch_encode_plus([target_text], max_length= self.summ_len, pad_to_max_length=True, truncation=True, padding="max_length", return_tensors='pt')

    source_ids = source['input_ids'].squeeze()
    source_mask = source['attention_mask'].squeeze()
    source_mask = torch.ones(self.source_len)
    target_ids = target['input_ids'].squeeze()
    target_mask = target['attention_mask'].squeeze()

    return {
        'source_ids': source_ids.to(dtype=torch.long), 
        'source_mask': source_mask.to(dtype=torch.long), 
        'target_ids': target_ids.to(dtype=torch.long),
        'target_ids_y': target_ids.to(dtype=torch.long)
    }

'''
Function to train the model one epoch
epoch = current epoch
path = path to results file
'''
def train(epoch, tokenizer, model, device, loader, optimizer, path):
  model.train()

  for _,data in enumerate(loader, 0):
    y = data['target_ids'].to(device, dtype = torch.long)
    y_ids = y[:, :-1].contiguous()
    lm_labels = y[:, 1:].clone().detach()
    lm_labels[y[:, 1:] == tokenizer.pad_token_id] = -100
    ids = data['source_ids'].to(device, dtype = torch.long)
    mask = data['source_mask'].to(device, dtype = torch.long)

    outputs = model(input_ids = ids, attention_mask = mask, decoder_input_ids=y_ids, labels=lm_labels)
    loss = outputs[0]

    # Log results, current epoch and loss - every 1000 samples
    if _%1000==0:
      print(str(epoch+1) + " " + str(_) + " " + str(loss))
      file = open(path + "results.txt", "a")
      file.write('\n' + str(epoch+1) + " " + str(_) + " " + str(loss) + '\n')
      file.close()

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

'''
Function to run validation on the model
epoch = current epoch
fold_idx = current fold index for cross validation
path = path to results file
'''
def validation(epoch, tokenizer, model, device, loader, path):
  model.eval()

  with torch.no_grad():
    total_loss = 0
    for _,data in enumerate(loader, 0):
        y = data['target_ids'].to(device, dtype = torch.long)
        y_ids = y[:, :-1].contiguous()
        lm_labels = y[:, 1:].clone().detach()
        lm_labels[y[:, 1:] == tokenizer.pad_token_id] = -100
        ids = data['source_ids'].to(device, dtype = torch.long)
        mask = data['source_mask'].to(device, dtype = torch.long)

        outputs = model(input_ids = ids, attention_mask = mask, decoder_input_ids=y_ids, labels=lm_labels)
        total_loss += float(outputs[0])
    
    # Log results
    total_loss /= len(loader)
    print({'epoch': epoch, 'val_loss': total_loss})
    file = open(path + "results.txt", "a")
    file.write('\nValidation: ' + str(epoch+1) + " " + str(total_loss) + '\n')
    file.close()

    return total_loss

'''
Function to test the model
'''
def test(tokenizer, model, device, loader):
  model.eval()
  predictions = []
  actuals = []

  with torch.no_grad():
      for _, data in enumerate(loader, 0):
          y = data['target_ids'].to(device, dtype = torch.long)
          ids = data['source_ids'].to(device, dtype = torch.long)
          mask = data['source_mask'].to(device, dtype = torch.long)

          generated_ids = model.generate(
              input_ids = ids,
              attention_mask = mask, 
              max_length=150, 
              num_beams=2,
              repetition_penalty=2.5, 
              length_penalty=1.0, 
              early_stopping=True
              )
          
          # Decode tokenized generated and gold question
          preds = [tokenizer.decode(g, skip_special_tokens=True, clean_up_tokenization_spaces=True) for g in generated_ids]
          target = [tokenizer.decode(t, skip_special_tokens=True, clean_up_tokenization_spaces=True)for t in y]

          predictions.extend(preds)
          actuals.extend(target)

  # Return generated and gold questions 
  return predictions, actuals

'''
Function to load the datasets
'''
def loadData(model_params, tokenizer, mode):
  # Paths to the datasets
  learning_q = path + "/QC_Datasets/learningq_data.json"
  ir_small = next(walk(path + "/IR_Datasets/IR_small/"), (None, None, []))[2]
  reading_comp = [path + "/QC_Datasets/drop_data.json", path + "/QC_Datasets/narrativeqa_data.json", path + "/QC_Datasets/squad_v2_data.json", path + "/QC_Datasets/triviaqa_data1.json"]
  
  IR_Datasets = []
  QC_Datasets = []
  QC_Data = []
  IR_Data = []

  IR_training_set = None
  IR_val_set = None
  IR_test_set = None
  IR_training_loader = None
  IR_val_loader = None
  IR_test_loader = None

  QC_training_set = None
  QC_val_set = None
  QC_test_set = None
  QC_training_loader = None
  QC_val_loader = None
  QC_test_loader = None

  # Defining the parameters for creation of dataloaders
  train_params = {
      'batch_size': model_params["TRAIN_BATCH_SIZE"],
      'shuffle': True,
      'num_workers': 0,
      }

  val_params = {
      'batch_size': model_params["VALID_BATCH_SIZE"],
      'shuffle': False,
      'num_workers': 0,
      }

  test_params = {
      'batch_size': model_params["TEST_BATCH_SIZE"],
      'shuffle': False,
      'num_workers': 0,
      }

  if mode == 'QC':
    for dataset in model_params['QC_DATASETS']:
      if dataset == 'learning-q':
        QC_Datasets.append(learning_q)
      elif dataset == 'reading-comp':
        for set in reading_comp:
          QC_Datasets.append(set)
    
    test_questions = []
    test_context = []
    val_questions = []
    val_context = []
    train_questions = []
    train_context = []

    # Load dataset
    for dataset in QC_Datasets:
      file = json.load(open(dataset))
      print("loading dataset:", file['dataset'], file['length'])
      count = 0

      for entry in file['data']:
        if len(entry['question'].split()) <= 2 or len(entry['context'].split()) <= 20:
          count += 1
          continue
        QC_Data.append(entry)

      if (file['dataset'] == "triviaqa_Part1"):
        file1 = json.load(open(path + "/QC_Datasets/triviaqa_data2.json"))
        for entry in file1['data']:
          if len(entry['question'].split()) <= 2 or len(entry['context'].split()) <= 20:
            count += 1
            continue
        QC_Data.append(entry)

      # shuffle dataset
      random.shuffle(QC_Data)
      length = len(QC_Data)

      # split dataset and add to the three sets
      for entry in QC_Data[:int(0.1*length)]:
        test_questions.append(entry['question'])
        test_context.append(entry['context'])

      for entry in QC_Data[int(0.1*length):int(0.2*length)]:
        val_questions.append(entry['question'])
        val_context.append(entry['context'])

      for entry in QC_Data[int(0.2*length):]:
        train_questions.append(entry['question'])
        train_context.append(entry['context'])

      QC_Data = []
      print("entries ignored:", count)
      print("entries added:", (file['length']-count))
    
    # Create data frames
    test_dataset = pd.DataFrame({'context': test_context, 'question': test_questions})
    del test_questions
    del test_context

    val_dataset = pd.DataFrame({'context': val_context, 'question': val_questions})
    del val_questions
    del val_context

    train_dataset = pd.DataFrame({'context': train_context, 'question': train_questions})
    del train_questions
    del train_context

    print(f"TRAIN Dataset: {train_dataset.shape}")
    print(f"VAL Dataset: {val_dataset.shape}")
    print(f"TEST Dataset: {test_dataset.shape}\n")

    # Creating the Training and Validation dataset for further creation of Dataloader
    QC_training_set = DataSet(train_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "context", "question")
    QC_val_set = DataSet(val_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "context", "question")
    QC_test_set = DataSet(test_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "context", "question")
    
    QC_training_loader = DataLoader(QC_training_set, **train_params)
    QC_val_loader = DataLoader(QC_val_set, **val_params)
    QC_test_loader = DataLoader(QC_test_set, **test_params)

    del train_dataset
    del val_dataset
    del test_dataset

  if mode == 'IR':
     # Select IR datasets (i.e. IR-small, IR-large, msmarco-passage)
    for dataset in model_params['IR_DATASETS']:
      if dataset == 'IR-small':
        dir = path + "/IR_Datasets/IR_small/"
        for entry in ir_small:
          IR_Datasets.append(dir+entry)

    test_queries = []
    test_document = []
    val_queries = []
    val_document = []
    train_queries = []
    train_document = []

    # Load dataset
    for dataset in IR_Datasets:
      file = json.load(open(dataset))
      print("loading dataset:", file['dataset'], file['length'])
      count = 0

      for entry in file['data']:
        if len(entry['query'].split()) <= 2 or len(entry['document'].split()) <= 20:
          count += 1
          continue
        IR_Data.append(entry)

      # Shuffle data
      random.shuffle(IR_Data)
      length = len(IR_Data)

      # split the individual dataset and add to the three sets
      for entry in IR_Data[:int(0.1*length)]:
        test_queries.append(entry['query'])
        test_document.append(entry['document'])

      for entry in IR_Data[int(0.1*length):int(0.2*length)]:
        val_queries.append(entry['query'])
        val_document.append(entry['document'])

      for entry in IR_Data[int(0.2*length):]:
        train_queries.append(entry['query'])
        train_document.append(entry['document'])

      IR_Data = []
      print("entries ignored:", count)
      print("entries added:", (file['length']-count))
    
    # Create dataframes
    test_dataset = pd.DataFrame({'document': test_document, 'query': test_queries})
    del test_queries
    del test_document

    val_dataset = pd.DataFrame({'document': val_document, 'query': val_queries})
    del val_queries
    del val_document

    train_dataset = pd.DataFrame({'document': train_document, 'query': train_queries})
    del train_queries
    del train_document

    print(f"TRAIN Dataset: {train_dataset.shape}")
    print(f"VAL Dataset: {val_dataset.shape}")
    print(f"TEST Dataset: {test_dataset.shape}\n")

    # Creating the Training and Validation dataset for further creation of Dataloader
    IR_training_set = DataSet(train_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "document", "query")
    IR_val_set = DataSet(val_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "document", "query")
    IR_test_set = DataSet(test_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "document", "query")
    
    IR_training_loader = DataLoader(IR_training_set, **train_params)
    IR_val_loader = DataLoader(IR_val_set, **val_params)
    IR_test_loader = DataLoader(IR_test_set, **test_params)
    
    del train_dataset
    del val_dataset
    del test_dataset

  del IR_Data
  del QC_Data

  return QC_training_loader, QC_val_loader, QC_test_loader, IR_training_loader, IR_val_loader, IR_test_loader

'''
Main function to run the pre-training
'''
def main():
  model_params={
      "MODEL":"t5-base",  # Base model, (t5-base, doc2query-t5-base-msmarco or the name of the models' folder in the Models directory)
      "NAME": "",                    # Name of the model
      "LAST_EPOCH":5,
      "TRAIN_BATCH_SIZE":4,          # training batch size
      "VALID_BATCH_SIZE":4,          # validation batch size
      "TEST_BATCH_SIZE":10,           # validation batch size
      "TRAIN_EPOCHS":10,           # number of training epochs 
      "VAL_EPOCHS":1,                # number of validation epochs
      "LEARNING_RATE":1e-5,          # learning rate
      "MAX_SOURCE_TEXT_LENGTH":612,  # max length of source text
      "MAX_TARGET_TEXT_LENGTH":64,   # max length of target text
      "SEED": 42,                    # set seed for reproducibility 
      "QC_DATASETS": ['learning-q'],           # Select QC Datasets (learning-q, reading-comp)
      "IR_DATASETS": ['IR-small'],           # Select IR datasets (IR-small)
      "DESC": 'Test',                    # Description of test
      "TYPE": 'QC',                    # Type of data (QC, IR)
  }

  modelPath = path + "/Models/" + model_params["MODEL"] # Path to model folder
  outputPath = path + "/output/" + model_params["NAME"] + "/" # Path to output folder

  # Set random seeds
  random.seed(model_params['SEED'])
  torch.manual_seed(model_params['SEED'])
  np.random.seed(model_params['SEED'])
  torch.backends.cudnn.deterministic = True
  device = 'cuda' if torch.cuda.is_available() else 'cpu'

  print(f"""[Model]: Loading {model_params["MODEL"]}...\n""")

  # tokenzier for encoding the text
  tokenizer = T5Tokenizer.from_pretrained(modelPath, local_files_only=True)

  # Continue training from last epoch
  if model_params["LAST_EPOCH"] != 0:
    modelInfo = torch.load(modelPath + "/" + model_params["MODEL"] + "_" + str(model_params["LAST_EPOCH"]) + '.pth')
    model = T5ForConditionalGeneration.from_pretrained(pretrained_model_name_or_path=None, config=modelPath + "/" + "config.json", state_dict=modelInfo["model_state_dict"], local_files_only=True)
  # Train from first epoch
  else:
    model = T5ForConditionalGeneration.from_pretrained(modelPath, local_files_only=True)
  
  model = model.to(device)

  print(f"[Data]: Reading data...\n")

  # Get sets
  QC_training_loader, QC_val_loader, QC_test_loader, IR_training_loader, IR_val_loader, IR_test_loader = loadData(model_params, tokenizer, model_params['TYPE'])

  optimizer = torch.optim.Adam(params=filter(lambda p: p.requires_grad, model.parameters()),
                               lr=model_params["LEARNING_RATE"])
  
  # Load optimizer state from last epoch 
  if model_params["LAST_EPOCH"] != 0:
    optimizer.load_state_dict(modelInfo['optimizer_state_dict'])
  
  del modelInfo

  # Create output folder if it does not exist
  if os.path.exists(outputPath) == False:
    os.mkdir(outputPath)

  configuration = {"python version": sys.version, "OS" : currentOS, "pytorch": torch.__version__} 

  # Create results text file and save model paramters and configuration
  if os.path.exists(outputPath + 'results.txt') == False:
    file = open(outputPath + "results.txt", "w")
    file.write(str(configuration) + "\n")
    file.write(str(model_params) + "\n")
    file.close()

  # Train on IR dataset
  if model_params["TYPE"] == "IR":
    for epoch in range(model_params["LAST_EPOCH"], model_params["TRAIN_EPOCHS"]):
        train(epoch, tokenizer, model, device, IR_training_loader, optimizer, outputPath)
        loss = validation(epoch, tokenizer, model, device, IR_val_loader, outputPath)

        # Save parameters ebvery 5th epoch
        if epoch in [4, 9, 14, 19, 24]:
          torch.save({"model_state_dict": model.state_dict(), 'optimizer_state_dict': optimizer.state_dict()}, outputPath + model_params["NAME"] + "_" + str(epoch+1) + ".pth")
          tokenizer.save_pretrained(outputPath)
          model.save_pretrained(outputPath)

    # Generate metrics
    preds, acts = test(tokenizer, model, device, IR_test_loader)
    metrics.rougeScore(preds, acts, 'IR', outputPath)
    metrics.bleuScore(preds, acts, 'IR', outputPath)
    metrics.meteorScore(preds, acts, 'IR', outputPath)
    metrics.gleuScore(preds, acts, 'IR', outputPath)
    pickle.dump(preds, open(outputPath + "/" + model_params["NAME"] + "_IR_preds.pkl", 'wb'))
    pickle.dump(acts, open(outputPath + "/" + model_params["NAME"] + "_IR_acts.pkl", 'wb'))

  # Train on QC dataset
  elif model_params["TYPE"] == "QC":
    for epoch in range(model_params["LAST_EPOCH"], model_params["TRAIN_EPOCHS"]):
        train(epoch, tokenizer, model, device, QC_training_loader, optimizer, outputPath)
        loss = validation(epoch, tokenizer, model, device, QC_val_loader, outputPath)

        # Save parameters ebvery 5th epoch
        if epoch in [4, 9, 14, 19, 24]:
          torch.save({"model_state_dict": model.state_dict(), 'optimizer_state_dict': optimizer.state_dict()}, outputPath + model_params["NAME"] + "_" + str(epoch+1) + ".pth")
          tokenizer.save_pretrained(outputPath)
          model.save_pretrained(outputPath)
    
    # Generate metrics
    preds, acts = test(tokenizer, model, device, QC_test_loader)
    metrics.rougeScore(preds, acts, 'QC', outputPath)
    metrics.bleuScore(preds, acts, 'QC', outputPath)
    metrics.meteorScore(preds, acts, 'QC', outputPath)
    metrics.gleuScore(preds, acts, 'QC', outputPath)
    pickle.dump(preds, open(outputPath + "/" + model_params["NAME"] + "_QC_preds.pkl", 'wb'))
    pickle.dump(acts, open(outputPath + "/" + model_params["NAME"] + "_QC_acts.pkl", 'wb'))
  
  # Save model parameters
  torch.save({"model_state_dict": model.state_dict(), 'optimizer_state_dict': optimizer.state_dict()}, outputPath + model_params["NAME"] + ".pth")
  tokenizer.save_pretrained(outputPath)
  model.save_pretrained(outputPath)

if __name__ == "__main__":     
    main()