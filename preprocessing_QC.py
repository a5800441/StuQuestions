'''
Script to process QC Datasets
Created by: Adam Vere
Date:

Question-Context datasets:
    MCTest
    DROP
    LearningQ
    SQuAD_v2
    NarrativeQA
    TriviaQA
    HotpotQA - Not used
'''

# Path to folder
path = "" # CHANGE TO RELEVENT PATH

from datasets import load_dataset
import json
import random
import sys
from transformers.models.t5_alt import T5Tokenizer

# T5 Tokenizer used to check how many contexts get truncated after tokenizing
tokenizer = T5Tokenizer.from_pretrained(path + "/Models/t5-base", local_files_only=True)
Tokenize = False # Set to True to tokenize and count the truncated contexts in the Inspect function (this can take a long time)

# CHANGE TO RELEVENT PATH
cache_dir = path + "data" # Directory automatically downloaded datasets will be saved to (from huggingface)
path += "/data/QC_Datasets" # Path to where the datasets are contained

# Clean the questions and contexts
def cleanData(question, context):
  # Split by words
  temp_question = question.split()
  temp_context = context.split()
  
  # Check for and remove extra whitespace in question
  for entry in temp_question:
    if entry == ' ':
      temp_question.remove(entry)
  
  # Check for and remove extra whitespace in context
  for entry in temp_context:
    if entry == ' ':
      temp_context.remove(entry)
    
  # Return the cleaned question and context
  return ' '.join(temp_question), ' '.join(temp_context)

# Process the Drop Dataset
def Drop():
  # Load dataset
  drop = load_dataset("drop", cache_dir=cache_dir)
  print("drop", drop)

  output_file = path + "/drop_data.json" # Path to json file

  file = open(output_file, "w")
  # Metadata
  jsonData = {"dataset": "drop", "length": 0, "source": "https://huggingface.co/datasets/drop/viewer/default/train", "data": None}

  data = []

  # Extract questions and contexts from train and validation sets
  for question, passage in zip(drop['train']['question'], drop['train']['passage']):
    cQuestion, cContext = cleanData(question, passage)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  for question, passage in zip(drop['validation']['question'], drop['validation']['passage']):
    cQuestion, cContext = cleanData(question, passage)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  jsonData['data'] = data
  jsonData['length'] = len(data)
  print(jsonData['dataset'], jsonData['length'])

  # Save data
  jsonString = json.dumps(jsonData)
  file.write(jsonString)
  file.close()

# Process the Drop Dataset
def MCTest():
  # Load dataset
  mctest = load_dataset("sagnikrayc/mctest", cache_dir=cache_dir)
  print("mctest", mctest)

  output_file = path + "/mctest_data.json" # Path to json file

  file = open(output_file, "w")
  # Metadata
  jsonData = {"dataset": "mctest", "length": 0, "source": "https://huggingface.co/datasets/sagnikrayc/mctest", "data": None}

  data = []

  # Extract questions and contexts from train, validation and test sets
  for question, passage in zip(mctest['train']['question'], mctest['train']['story']):
    cQuestion, cContext = cleanData(question, passage)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  for question, passage in zip(mctest['validation']['question'], mctest['validation']['story']):
    cQuestion, cContext = cleanData(question, passage)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  for question, passage in zip(mctest['test']['question'], mctest['test']['story']):
    cQuestion, cContext = cleanData(question, passage)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  jsonData['data'] = data
  jsonData['length'] = len(data)
  print(jsonData['dataset'], jsonData['length'])

  # Save data
  jsonString = json.dumps(jsonData)
  file.write(jsonString)
  file.close()

# Process the LearningQ Dataset
def LearningQ():
  # Load dataset
  learningq = load_dataset("sidovic/LearningQ-qg", revision="refs/convert/parquet", cache_dir=cache_dir)
  print("learningq", learningq)

  output_file = path + "/learningq_data.json" # Path to json file

  file = open(output_file, "w")
  # Metadata
  jsonData = {"dataset": "learningq", "length": 0, "source": "https://huggingface.co/datasets/sidovic/LearningQ-qg", "data": None}

  data = []

  # Extract questions and contexts from train, validation and test sets
  for question, passage in zip(learningq['train']['question'], learningq['train']['context']):
    cQuestion, cContext = cleanData(question, passage)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  for question, passage in zip(learningq['validation']['question'], learningq['validation']['context']):
    cQuestion, cContext = cleanData(question, passage)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)
  
  for question, passage in zip(learningq['test']['question'], learningq['test']['context']):
    cQuestion, cContext = cleanData(question, passage)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  jsonData['data'] = data
  jsonData['length'] = len(data)
  print(jsonData['dataset'], jsonData['length'])

  # Save data
  jsonString = json.dumps(jsonData)
  file.write(jsonString)
  file.close()

# Process the SQuAD v2 Dataset
def SQuAD():
  # Load dataset
  squad = load_dataset("squad_v2", cache_dir=cache_dir)
  print("squad", squad)

  output_file = path + "/squad_v2_data.json" # Path to json file

  file = open(output_file, "w")
  # Metadata
  jsonData = {"dataset": "SQuAD_v2", "length": 0, "source": "https://huggingface.co/datasets/squad_v2", "data": None}

  data = []

  # Extract questions and contexts
  for question, passage in zip(squad['train']['question'], squad['train']['context']):
    cQuestion, cContext = cleanData(question, passage)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  for question, passage in zip(squad['validation']['question'], squad['validation']['context']):
    cQuestion, cContext = cleanData(question, passage)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  #data = segment(data)
  jsonData['data'] = data
  jsonData['length'] = len(data)
  print(jsonData['dataset'], jsonData['length'])

  # Save Data
  jsonString = json.dumps(jsonData)
  file.write(jsonString)
  file.close()

# Process the NarrativeQA Dataset
def NarrativeQA():
  # Load dataset, strip out unneeded info to save memory
  narrativeqa = load_dataset("narrativeqa", cache_dir=cache_dir)
  flattened_narrativeqa = narrativeqa.flatten()
  flattened_narrativeqa = flattened_narrativeqa.remove_columns(["answers", "document.text", "document.summary.tokens", "question.tokens"])
  print("narrativeqa", flattened_narrativeqa)

  output_file = path + "/narrativeqa_data.json" # Path to json file

  file = open(output_file, "w")
  # Metadata
  jsonData = {"dataset": "narrativeqa", "length": 0, "source": "https://huggingface.co/datasets/narrativeqa", "data": None}
  data = []
    
    # Extract questions and contexts from train, validation and test sets
  for question, passage in zip(flattened_narrativeqa['train']['question.text'], flattened_narrativeqa['train']['document.summary.text']):
    cQuestion, cContext = cleanData(question, passage)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  for question, passage in zip(flattened_narrativeqa['validation']['question.text'], flattened_narrativeqa['validation']['document.summary.text']):
    cQuestion, cContext = cleanData(question, passage)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  for question, passage in zip(flattened_narrativeqa['test']['question.text'], flattened_narrativeqa['test']['document.summary.text']):
    cQuestion, cContext = cleanData(question, passage)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  #data = segment(data)
  jsonData['data'] = data
  jsonData['length'] = len(data)
  print(jsonData['dataset'], jsonData['length'])

  # Save Data
  jsonString = json.dumps(jsonData)
  file.write(jsonString)
  file.close()

# Process the HotpotQA Dataset (No longer used)
'''def Hotpot():
  # Load dataset
  hotpot_qa = load_dataset("hotpot_qa", 'distractor', cache_dir=cache_dir)
  print("hotpot_qa", hotpot_qa)

  output_file = path + "/hotpotqa_data.json"

  file = open(output_file, "w")
  # Metadata
  jsonData = {"dataset": "hotpotqa", "length": 0, "source": "https://huggingface.co/datasets/hotpot_qa", "data": None}

  data = []

  # Extract questions and contexts
  for question, passage in zip(hotpot_qa['train']['question'], hotpot_qa['train']['context']):
    context = ""
    for sentences in passage['sentences']:
      for sentence in sentences:
        context += sentence
      context += '\n'

    cQuestion, cContext = cleanData(question, context)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  for question, passage in zip(hotpot_qa['validation']['question'], hotpot_qa['validation']['context']):
    context = ""
    for sentences in passage['sentences']:
      for sentence in sentences:
        context += sentence
      context += '\n'
    
    cQuestion, cContext = cleanData(question, context)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  #data = segment(data)
  # Save Data
  jsonData['data'] = data
  jsonData['length'] = len(data)
  print(jsonData['dataset'], jsonData['length'])

  jsonString = json.dumps(jsonData)
  file.write(jsonString)
  file.close()
'''

# Process the TriviaQA Dataset into two part to save memory consumption
def TriviaQA():
  # Load data, strip out unneeded info to save memory
  trivia_qa = load_dataset("trivia_qa", 'rc', cache_dir=cache_dir)
  flattened_trivia_qa = trivia_qa.flatten()
  trivia_qa = trivia_qa.remove_columns(["answer", "question_source","question_id"])
  flattened_trivia_qa = trivia_qa.flatten()
  flattened_trivia_qa = flattened_trivia_qa.remove_columns(["search_results.filename", "search_results.title"])
  print("trivia_qa", flattened_trivia_qa)

  output_file = path + "/triviaqa_data1.json" # Path to json file part 1
  output_file1 = path + "/triviaqa_data2.json" # Path to json file part 2

  file = open(output_file, "w")
  file1 = open(output_file1, "w")
  
  # Metadata
  jsonData = {"dataset": "triviaqa_Part1", "length": 0, "source": "https://huggingface.co/datasets/trivia_qa", "data": None}
  jsonData1 = {"dataset": "triviaqa_Part2", "length": 0, "source": "https://huggingface.co/datasets/trivia_qa", "data": None}

  data = []
  
    # Extract questions and contexts from train set as part1 of the dataset
  for question, passage, passage1, passage2 in zip(flattened_trivia_qa['train']['question'], flattened_trivia_qa['train']['search_results.description'], flattened_trivia_qa['train']['search_results.search_context'], flattened_trivia_qa['train']['entity_pages.wiki_context']):
    context = ""
    if passage == [] or passage == "":
      continue

    for desc in passage: # Extract the summary
      context += desc

    for desc in passage1[0:int(0.15*len(passage1))]: # Extract 15% of the text
      context += desc

    for desc in passage2[0:int(0.15*len(passage2))]: # Extract 15% of the text
      context += desc

    if len(context.split()) < 60: # Ignore conexts less than 60 words (I.e. just a short summary)
      continue

    cQuestion, cContext = cleanData(question, context) # Clean sample
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)
  
  # Save the data
  jsonData['data'] = data
  print(jsonData['dataset'], len(jsonData['data']))
  jsonData['length'] = len(jsonData['data'])
  file.write(json.dumps(jsonData))
  file.close()
  data = []
    
    # Extract questions and contexts from validation and test set as part2 of the dataset
  for question, passage, passage1, passage2 in zip(flattened_trivia_qa['validation']['question'], flattened_trivia_qa['validation']['search_results.description'], flattened_trivia_qa['train']['search_results.search_context'], flattened_trivia_qa['validation']['entity_pages.wiki_context']):
    context = ""
    if passage == [] or passage == "":
      continue

    for desc in passage: # Extract the summary
      context += desc

    for desc in passage1[0:int(0.15*len(passage1))]: # Extract 15% of the text
      context += desc
    
    for desc in passage2[0:int(0.15*len(passage2))]: # Extract 15% of the text
      context += desc

    if len(context.split()) < 60: # Ignore conexts less than 60 words (I.e. just a short summary)
      continue

    cQuestion, cContext = cleanData(question, context)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)

  for question, passage, passage1, passage2 in zip(flattened_trivia_qa['test']['question'], flattened_trivia_qa['test']['search_results.description'], flattened_trivia_qa['train']['search_results.search_context'], flattened_trivia_qa['test']['entity_pages.wiki_context']):
    context = ""
    if passage == [] or passage == "":
      continue

    if passage1 == [] or passage1 == "":
      continue

    if passage2 == [] or passage2 == "":
      continue

    for desc in passage:
      context += desc

    for desc in passage1[0:int(0.15*len(passage1))]:
      context += desc
    
    for desc in passage2[0:int(0.15*len(passage2))]:
      context += desc

    if len(context.split()) < 60:
      continue

    cQuestion, cContext = cleanData(question, context)
    pair = {"question" : cQuestion, "context": cContext}
    data.append(pair)
  
  # Save the dataset to a json file
  jsonData1['data'] = data
  print(jsonData1['dataset'], len(jsonData1['data']))
  jsonData1['length'] = len(jsonData1['data'])
  file1.write(json.dumps(jsonData1))
  file1.close()

# Inspect the datasets
def Inspect():
  # File paths
  mctest = path + "/mctest_data.json"
  triviaqa_data1 = path + "/triviaqa_data1.json"
  triviaqa_data2 = path + "/triviaqa_data2.json"
  narrativeqa_data = path + "/narrativeqa_data.json"
  squad_data = path + "/squad_v2_data.json"
  learningq_data = path + "/learningq_data.json"
  drop_data = path + "/drop_data.json"

  file_list = [mctest, squad_data, drop_data, narrativeqa_data, triviaqa_data1, learningq_data]
    
  for file in file_list:
    jfile = None
    try:
      jfile = json.load(open(file)) # Load the JSON file
    except:
      continue
    # Variables for the stats
    emptyQ = 0
    emptyC = 0
    avg_Q_len = 0
    avg_C_len = 0
    min_Q_len = 1000000000
    min_C_len = 1000000000
    max_Q_len = -1
    max_C_len = -1
    count = 0
    count1 = 0
    count2 = 0

    print(jfile['dataset'])
    fileData = jfile['data']

    # Merge the Trivia dataset into one, requires around 20gb of memory
    if jfile['dataset'] == "triviaqa_Part1":
      jfile1 = json.load(open(triviaqa_data2))

      for data in jfile1['data']:
        fileData.append(data)


    del jfile['data'] # Clear memory
    
    for data in fileData:
      # Check for empty questions and contexts
      if (data['question'] == '' or data['question'] == None):
        emptyQ += 1
      if data['context'] == '' or data['context'] == None or data['context'] == []:
        emptyC += 1
      
      # Sum the lengths (in words)
      avg_Q_len += len(data['question'].split())
      avg_C_len += len(data['context'].split())
      
      if (Tokenize == True):
          # Tokenize the context
          source = tokenizer.batch_encode_plus([data['context']], truncation_strategy='do_not_truncate', return_tensors='pt')
          source_ids = source['input_ids'].squeeze()
          source_original_length = len(source_ids)
            
            # Count the number of contexts longer than these lengths
          if source_original_length > 512:
            count += 1

          if source_original_length > 612:
            count1 += 1

          if source_original_length > 1024:
            count2 += 1
      
      # Update min and max question and context length
      if len(data['question'].split()) < min_Q_len:
        min_Q_len = len(data['question'].split())

      if len(data['question'].split()) > max_Q_len:
        max_Q_len = len(data['question'].split())

      if len(data['context'].split()) < min_C_len:
        min_C_len = len(data['context'].split())

      if len(data['context'].split()) > max_C_len:
        max_C_len = len(data['context'].split())
    
    # Calculate average
    avg_Q_len /= len(fileData)
    avg_C_len /= len(fileData)

    # Print out stats
    print(len(fileData))
    print('empty questions:', emptyQ)
    print('empty context:', emptyC)
    print('min question length:', int(min_Q_len), 'words')
    print('min context length:', int(min_C_len), 'words')
    print('average question length:', int(avg_Q_len), 'words')
    print('average context length:', int(avg_C_len), 'words')
    print('max question length:', int(max_Q_len), 'words')
    print('max context length:', int(max_C_len), 'words')
    print('no. of context above 512 tokens:', int(count), 'contexts')
    print('no. of context above 612 tokens:', int(count1), 'contexts')
    print('no. of context above 1024 tokens:', int(count2), 'contexts')

    # Sample questions and corresponding contexts
    index = random.randint(0, jfile['length']-1)
    print("question:", fileData[index]['question'])
    print("context:", fileData[index]['context'], '\n')
    
    del jfile
    del fileData

# Uncomment dataset to process
if __name__ == "__main__":
	datasets = []
	# Get datasets
	for arg in sys.argv[1:]:
		datasets.append(arg.lower())
		datasets = set(datasets)

	# Run selected dataset functions
	for dataset in datasets:
		if dataset == "drop":
			Drop()
			
		if dataset == "mctest":
			MCTest()

		if dataset == "LearningQ":
			LearningQ()
			
		if dataset == "squad":
			SQuAD()
			
		if dataset == "narrativeqa":
			NarrativeQA()
			
		if dataset == "triviaqa":
			TriviaQA()

	Inspect()