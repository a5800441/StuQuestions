# StuQuestions

## Name
StuQuestions - Generating Student Questions from Lecture Transcripts

## Description
Investigating the use of pre-trained data-driven models for automatic question generation from lecture transcripts

## Installation
To install the nessessary packages to run the program, first download Python version 3.8.10 (https://tinyurl.com/python3810)

Then run the following command:
- [ ] pip install -r requirements.txt --extra-index-url https://download.pytorch.org/whl/cu113

To download the libraries for the replication, run the following command
- [ ] pip install -r rep_requirements.txt --extra-index-url https://download.pytorch.org/whl/cu113

Before running the programs:
- [ ] Unzip the example models in the Model folder
- [ ] Unzip the IR and QC datasets in the data folder

Base models can found on HuggingFace:
- [ ] T5-Base (https://huggingface.co/t5-base)
- [ ] docT5query (https://huggingface.co/castorini/doc2query-t5-base-msmarco)

# Folders
data:
- [ ] This folder contains all the datasets created and used

Models:
- [ ] This folder contains all the models

tests:
- [ ] Contains the results from our tests

output:
- [ ] Folder where all output goes to

transformers:
- [ ] This folder contains the T5 models and tokenizer scripts from huggingface
- [ ] transformers/models/t5 transformers/models/t5_alt is where the edited t5 scripts are contained

nltk_data:
- [ ] This folder contains the necessary files to run the nltk library automatic metrics

# Usage
- [ ] Before using any of the program, first cd to the main directory.
- [ ] At the top of each file are PATHS that have to be edited. Change the value to the directory this README is located

## preprocessing_QC.py
- [ ] Edit the path at line 17

- [ ] To run this script, use the following command: 
    - [ ] ./preprocessing_QC.py <arg1> <arg2> # argx = mctest, squad, learningq, narrativeqa, triviaqa and drop

- [ ] The arguments can the following values: mctest, squad, learningq, narrativeqa, triviaqa and drop. They are used to specfic which dataset to process
- [ ] At line 27 is the variable Tokenize, which is used to tell the Inspect method whether to check for how many contexts get truncated or not

## preprocessing_IR.py
- [ ] Edit the path at line 26

- [ ] To run this script, use the following command: 
    - [ ] ./preprocessing_IR.py <arg1> <arg2> # argx = lotte, antique, codec, beir, and msmarco

- [ ] The arguments can the following values: lotte, antique, codec, beir, and msmarco. They are used to specfic which dataset to process
- [ ] At line 36 is the variable Tokenize, which is used to tell the Inspect method whether to check for how many contexts get truncated or not

## StuQuestions_finetuning.py
- [ ] Edit the path at line 15

- [ ] At line 446 is the model parameters that can be edited:
    The MODEL parameter is which model gets finetuned
    The NAME parameter the name of the fine-tuned model
    The EPOCH_SETTING parameter can be used to fine-tune a specfic iteration of the model (5, 10, 15, 20, 25)
    The number of epochs, learning rate and batch sizes can be edited here as well

- [ ] After inputting the correct path and model parameters, the program can be run with the following command:
    python ./StuQuestions_finetuning.py

## StuQuestions_pretraining.py
- [ ] Edit the path at line 16

- [ ] At line 392 is the model parameters that can be edited:
    The MODEL parameter is which model gets finetuned
    The NAME parameter the name of the fine-tuned model
    The EPOCH_SETTING parameter can be used to fine-tune a specfic iteration of the model (5, 10, 15, 20, 25)
    The number of epochs, learning rate and batch sizes can be edited here as well

- [ ] After inputting the correct path and model parameters, the program can be run with the following command:
    python ./StuQuestions_pretraining.py

## GenerateQuestion.py
- [ ] Edit the path at line 14

- [ ] At line 345 is the program flags can be edited, these are used to tell what the program to do:
    printPredictions - Run the Predictions function for a model
    TestLearningQ - Run the LearningQ test set
    TestLearningReadingComp - Run the Reading-comp  test set
    TestIR - Run the IR-small test set
    TestSet - Run the Lecture-transcript and questions test set
    FullDataset - Run the full Lecture-transcript and questions dataset
    Evaluation - Run the evaluation on model (used for I.Q.A)
    Transcripts - Generate questions from the MIT transcripts

- [ ] At line 356 is the model parameters that can be edited:
    The MODEL parameter is which model gets finetuned

- [ ] After inputting the correct path and model parameters, the program can be run with the following command:
    python ./GenerateQuestion.py