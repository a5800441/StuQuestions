'''
Script to process all the IR datasets
Created by: Adam Vere
Date: 

IR-small datasets:
    Codec history
    Codec politics 
    Codec economics
    Lotte science
    Lotte writing
    Lotte technology
    Beir dbpedia
    Beir english
    Beir gaming
    Beir mathematica
    Beir physics
    Beir programmers
    Beir scifact
    Beir stats
    Beir unix
    Beir android
'''

# Path to folder
path = "" # CHANGE TO RELEVENT PATH

import ir_datasets
import json
import random
import sys
from transformers.models.t5_alt import T5Tokenizer

# T5 Tokenizer used to check how many contexts get truncated after tokenizing
tokenizer = T5Tokenizer.from_pretrained(path + "/Models/t5-base", local_files_only=True)
Tokenize = False # Set to True to tokenize and count the truncated contexts in the Inspect function (this can take a long time)

path += "/data/IR_Datasets" # Path to where the datasets are contained

# Clean the queries and documents
def cleanData(query, document):
  # Split by words
  temp_query = query.split()
  temp_document = document.split()

 # Check for and remove extra whitespace in query
  for entry in temp_query:
    if entry == ' ':
      temp_query.remove(entry)

  # Check for and remove extra whitespace in document
  for entry in temp_document:
    if entry == ' ':
      temp_document.remove(entry)

  # Return the cleaned query and document
  return ' '.join(temp_query), ' '.join(temp_document)

# Merge documents that correspond to the same query, 
# based on relevence judments there may be multiple entries of the same query but with different documents
def mergeDuplicates(data):
  print("merging")
  index = 0
  for entry in data:
    query = entry['query']
    document = entry['document']

    if index % 1000 == 0:
      print(index)

    found = False
    for other_entry in data[index+1:]:
      if other_entry['query'] == query:
        document += ' ' + other_entry['document'] # Append new document prefixed with a space
        data.pop(index+1) # Remove "duplicate"
        found = True
      else:
        break 
    
    # If there are duplicates, overwrite current value for the document
    if found == True:
      entry['document'] = document
    
    index += 1
  
  return data

# Process the Natural Questions Dataset (Only has Relevance scores of 1) (Not used)
'''def Natural():
  # Load the dataset
  n_questions = ir_datasets.load("natural-questions")
  n_questions_dev = ir_datasets.load("natural-questions/dev")
  n_questions_train = ir_datasets.load("natural-questions/train")

  output_file = path + "/natural_data.json"
  file = open(output_file, "w")
  # Metadata
  jsonData = {"dataset": "natural-questions", "length": 0, "source": "https://ir-datasets.com/natural-questions.html", "data": None}
  data = []

  # Load the documents
  docs = n_questions.docs_store()

  queries = {}
  queries1 = {}
  
  # Add queries to a dictionary for easier loopup
  for q in n_questions_train.queries_iter():
    queries[str(q[0])] = q[1]

  # Add queries to a dictionary for easier loopup
  for q in n_questions_dev.queries_iter():
    queries1[str(q[0])] = q[1]

  # Loop through relevance judgements for the dev set
  for qrel in n_questions_dev.qrels_iter():
    query_id = qrel[0]
    doc_id = qrel[1]

    # Check if document exists
    try:
      document = docs.get(doc_id)[1]
    except:
      continue

    query = queries1[str(query_id)] # Get query from dictionary

    # Add the query-document pair to a list
    cQuery, cDocument = cleanData(query, document)
    pair = {"query" : cQuery, "document": cDocument}
    data.append(pair)

  # Loop through relevance judgements for the train set
  for qrel in n_questions_train.qrels_iter():
    query_id = qrel[0]
    doc_id = qrel[1]

    try:
      document = docs.get(doc_id)[1]
    except:
      continue
    
    # Ignore empty documents
    if document == '':
      continue

    query = queries[str(query_id)] # Get query from

    cQuery, cDocument = cleanData(query, document)
    pair = {"query" : cQuery, "document": cDocument}
    data.append(pair)

  # Save data
  jsonData['data'] = data
  jsonData['length'] = len(data)
  print(jsonData['dataset'], jsonData['length'])

  file.write(json.dumps(jsonData))
  file.close()'''

# Process the Antique Dataset
def Antique():
  antique_test = ir_datasets.load("antique/test")
  antique_train = ir_datasets.load("antique/train")

  output_file = path + "/antique_data.json"
  file = open(output_file, "w")

  # Metadata
  jsonData = {"dataset": "antique", "length": 0, "source": "https://ir-datasets.com/antique.html", "data": None}
  data = []

  # Get docs
  docsList = [antique_test.docs_store(), antique_train.docs_store()]
  datasetList = [antique_test, antique_train]

  # Get query-document pairs that have a relevance score of 2 or 3
  for docs, dataset in zip(docsList, datasetList):
    for qrel in dataset.qrels_iter():
      query_id = qrel[0]
      doc_id = qrel[1]
      relevance = qrel[2]

      if relevance >= 2:
        document = docs.get(doc_id)[1]
        query = ""

        for q in dataset.queries_iter():
          if q[0] == query_id:
            query = q[1]
            break

        cQuery, cDocument = cleanData(query, document)
        pair = {"query" : cQuery, "document": cDocument}

        data.append(pair)

  print('before merge:', len(data))
  data = mergeDuplicates(data) # Merge the duplicate queries
  jsonData['data'] = data
  jsonData['length'] = len(data)
  print(jsonData['dataset'], jsonData['length'])

  # Save data
  file.write(json.dumps(jsonData))
  file.close()

# Process all the Beir datasets
def Beir():
  # Load all the datasets
  scifact_train = ir_datasets.load("beir/scifact/train")
  scifact_test = ir_datasets.load("beir/scifact/test")

  '''fever_train = ir_datasets.load("beir/fever/train")
  fever_test = ir_datasets.load("beir/fever/test")
  fever_dev = ir_datasets.load("beir/fever/dev")'''

  dbpedia_dev = ir_datasets.load("beir/dbpedia-entity/dev")
  dbpedia_test = ir_datasets.load("beir/dbpedia-entity/test")

  unix = ir_datasets.load("beir/cqadupstack/unix")
  stats = ir_datasets.load("beir/cqadupstack/stats")
  programmers = ir_datasets.load("beir/cqadupstack/programmers")
  physics = ir_datasets.load("beir/cqadupstack/physics")
  mathematica = ir_datasets.load("beir/cqadupstack/mathematica")
  gaming = ir_datasets.load("beir/cqadupstack/gaming")
  english = ir_datasets.load("beir/cqadupstack/english")
  android = ir_datasets.load("beir/cqadupstack/android")

  # Output files
  output_file = path + "/beir_scifact_data.json"
  #output_file_1 = path + "/beir_fever_data.json"
  output_file_2 = path + "/beir_dbpedia_data.json"
  output_file_3 = path + "/beir_unix_data.json"
  output_file_4 = path + "/beir_stats_data.json"
  output_file_5 = path + "/beir_programmers_data.json"
  output_file_6 = path + "/beir_physics_data.json"
  output_file_7 = path + "/beir_mathematica_data.json"
  output_file_8 = path + "/beir_gaming_data.json"
  output_file_9 = path + "/beir_english_data.json"
  output_file_10 = path + "/beir_android_data.json"

  file = open(output_file, "w")
  #file1 = open(output_file_1, "w")
  file2 = open(output_file_2, "w")
  file3 = open(output_file_3, "w")
  file4 = open(output_file_4, "w")
  file5 = open(output_file_5, "w")
  file6 = open(output_file_6, "w")
  file7 = open(output_file_7, "w")
  file8 = open(output_file_8, "w")
  file9 = open(output_file_9, "w")
  file10 = open(output_file_10, "w")

  # Metadata
  jsonData = {"dataset": "beir_scifact", "length": 0, "source": "https://ir-datasets.com/beir.html", "data": None}
  #jsonData1 = {"dataset": "beir_fever", "length": 0, "source": "https://ir-datasets.com/beir.html", "data": None}
  jsonData2 = {"dataset": "beir_dbpedia", "length": 0, "source": "https://ir-datasets.com/beir.html", "data": None}
  jsonData3 = {"dataset": "beir_unix", "length": 0, "source": "https://ir-datasets.com/beir.html", "data": None}
  jsonData4 = {"dataset": "beir_stats", "length": 0, "source": "https://ir-datasets.com/beir.html", "data": None}
  jsonData5 = {"dataset": "beir_programmers", "length": 0, "source": "https://ir-datasets.com/beir.html", "data": None}
  jsonData6 = {"dataset": "beir_physics", "length": 0, "source": "https://ir-datasets.com/beir.html", "data": None}
  jsonData7 = {"dataset": "beir_mathematica", "length": 0, "source": "https://ir-datasets.com/beir.html", "data": None}
  jsonData8 = {"dataset": "beir_gaming", "length": 0, "source": "https://ir-datasets.com/beir.html", "data": None}
  jsonData9 = {"dataset": "beir_english", "length": 0, "source": "https://ir-datasets.com/beir.html", "data": None}
  jsonData10 = {"dataset": "beir_android", "length": 0, "source": "https://ir-datasets.com/beir.html", "data": None}

  # Gets docs, list, the dataset, files, and json data
  docsList = [unix.docs_store(), stats.docs_store(), programmers.docs_store(), physics.docs_store(), mathematica.docs_store(), gaming.docs_store(), english.docs_store(), android.docs_store()]
  datasetList = [unix, stats, programmers, physics, mathematica, gaming, english, android]
  files = [file3, file4, file5, file6, file7, file8, file9, file10]
  jsonDataList = [jsonData3, jsonData4, jsonData5, jsonData6, jsonData7, jsonData8, jsonData9, jsonData10]
  data = []

  # Extract query-documents pairs with relevance score 1 from datasets 3-10 (all in the same format)
  for docs, dataset, outputFile, jsondata in zip(docsList, datasetList, files, jsonDataList):
    queries = {}
    for q in dataset.queries_iter():
      queries[str(q[0])] = q[1]

    for qrel in dataset.qrels_iter():
      query_id = qrel[0]
      doc_id = qrel[1]
      relevance = qrel[2]

      try:
        document = docs.get(doc_id)[1]
      except:
        continue

      query = queries[str(query_id)]

      cQuery, cDocument = cleanData(query, document)
      pair = {"query" : cQuery, "document": cDocument}

      data.append(pair)

    print('before merge:', len(data))
    data = mergeDuplicates(data)
    jsondata['data'] = data
    jsondata['length'] = len(data)
    print(jsondata['dataset'], jsondata['length'])

    outputFile.write(json.dumps(jsondata))
    outputFile.close()
    data = []

  # Extract query-documetn pairs from dbpedia
  docsList = [dbpedia_dev.docs_store(), dbpedia_test.docs_store()]
  datasetList = [dbpedia_dev, dbpedia_test]
  data = []

  for docs, dataset in zip(docsList, datasetList):
    queries = {}
    
    for q in dataset.queries_iter():
      queries[str(q[0])] = q[1]

    for qrel in dataset.qrels_iter():
      query_id = qrel[0]
      doc_id = qrel[1]

      try:
        document = docs.get(doc_id)[1]
      except:
        continue

      query = queries[str(query_id)]

      cQuery, cDocument = cleanData(query, document)
      pair = {"query" : cQuery, "document": cDocument}
      data.append(pair)

  print('before merge:', len(data))
  data = mergeDuplicates(data) # Merge duplicates
  jsonData2['data'] = data
  jsonData2['length'] = len(data)
  print(jsonData2['dataset'], jsonData2['length'])
  file2.write(json.dumps(jsonData2))
  file2.close()

  '''# Extract query-document pairs from fever
  docsList = [fever_train.docs_store(), fever_test.docs_store(), fever_dev.docs_store()]
  datasetList = [fever_train, fever_test, fever_dev]
  data = []

  for docs, dataset in zip(docsList, datasetList):
    queries = {}
    
    for q in dataset.queries_iter():
      queries[str(q[0])] = q[1]

    for qrel in dataset.qrels_iter():
      query_id = qrel[0]
      doc_id = qrel[1]

      try:
        document = docs.get(doc_id)[1]
      except:
        continue

      query = queries[str(query_id)]

      cQuery, cDocument = cleanData(query, document)
      pair = {"query" : cQuery, "document": cDocument}
      data.append(pair)

  print('before merge:', len(data))
  data = mergeDuplicates(data)
  jsonData1['data'] = data
  jsonData1['length'] = len(data)
  print(jsonData1['dataset'], jsonData1['length'])
  file1.write(json.dumps(jsonData1))
  file1.close()'''

  # Extract query-documents pairs from scifact
  docsList = [scifact_train.docs_store(), scifact_test.docs_store()]
  datasetList = [scifact_train, scifact_test]
  data = []

  for docs, dataset in zip(docsList, datasetList):
    queries = {}
    
    for q in dataset.queries_iter():
      queries[str(q[0])] = q[1]

    for qrel in dataset.qrels_iter():
      query_id = qrel[0]
      doc_id = qrel[1]

      try:
        document = docs.get(doc_id)[1]
      except:
        continue

      query = queries[str(query_id)]

      cQuery, cDocument = cleanData(query, document)
      pair = {"query" : cQuery, "document": cDocument}
      data.append(pair)

  print('before merge:', len(data))
  data = mergeDuplicates(data)
  jsonData['data'] = data
  jsonData['length'] = len(data)
  print(jsonData['dataset'], jsonData['length'])
  file.write(json.dumps(jsonData))
  file.close()

'''# Process all the datasets from codec
# In commandline, google collab
#!wget https://codec-public-data.s3.amazonaws.com/codec_documents.jsonl -P "/root/.ir_datasets/codec/v1"'''
def Codec():
  # Load datasets
  economics = ir_datasets.load("codec/economics")
  history = ir_datasets.load("codec/history")
  politics = ir_datasets.load("codec/politics")
  
  output_file = path + "/codec_economics_data.json"
  output_file_1 = path + "/codec_history_data.json"
  output_file_2 = path + "/codec_politics_data.json"

  file = open(output_file, "w")
  file1 = open(output_file_1, "w")
  file2 = open(output_file_2, "w")

  # Metadata
  jsonData = {"dataset": "codec_economics", "length": 0, "source": "https://ir-datasets.com/codec.html", "data": None}
  jsonData1 = {"dataset": "codec_history", "length": 0, "source": "https://ir-datasets.com/codec.html", "data": None}
  jsonData2 = {"dataset": "codec_politics", "length": 0, "source": "https://ir-datasets.com/codec.html", "data": None}

  data = []

  # Get documents
  docsList = [economics.docs_store(), history.docs_store(), politics.docs_store()]
  datasetList = [economics, history, politics]
  files = [file, file1, file2]
  jsonDataList = [jsonData, jsonData1, jsonData2]

  # Extract query-documents pairs with a relevance score of 2 or 3
  for docs, dataset, outputFile, jsondata in zip(docsList, datasetList, files, jsonDataList):
    for qrel in dataset.qrels_iter():
      query_id = qrel[0]
      doc_id = qrel[1]
      relevance = qrel[2]

      if relevance >= 2:
        document = docs.get(doc_id)[1]
        query = ""
  
        for q in dataset.queries_iter():
          if q[0] == query_id:
            query = q[1]
            break

        cQuery, cDocument = cleanData(query, document)
        pair = {"query" : cQuery, "document": cDocument}

        data.append(pair)

    print('before merge:', len(data))
    data = mergeDuplicates(data)
    jsondata['data'] = data
    jsondata['length'] = len(data)
    print(jsondata['dataset'], jsondata['length'])

    outputFile.write(json.dumps(jsondata))
    outputFile.close()
    data = []
  
# Process all the Lotte datasets
def Lotte():
  # Load datasets
  lotte_science_dev = ir_datasets.load("lotte/science/dev/search")
  lotte_science_test = ir_datasets.load("lotte/science/test/search")
  lotte_writing_dev = ir_datasets.load("lotte/writing/dev/search")
  lotte_writing_test = ir_datasets.load("lotte/writing/test/search")
  lotte_technology_dev = ir_datasets.load("lotte/technology/dev/search")
  lotte_technology_test = ir_datasets.load("lotte/technology/test/search")

  output_file = path + "/lotte_science_data.json"
  output_file_1 = path + "/lotte_writing_data.json"
  output_file_2 = path + "/lotte_technology_data.json"

  file = open(output_file, "w")
  file1 = open(output_file_1, "w")
  file2 = open(output_file_2, "w")

  # Metadata
  jsonData = {"dataset": "lotte_science", "length": 0, "source": "https://ir-datasets.com/lotte.html", "data": None}
  jsonData1 = {"dataset": "lotte_writing", "length": 0, "source": "https://ir-datasets.com/lotte.html", "data": None}
  jsonData2 = {"dataset": "lotte_technology", "length": 0, "source": "https://ir-datasets.com/lotte.html", "data": None}

  data = []

  # Load documents
  docsList = [lotte_science_dev.docs_store(), lotte_science_test.docs_store()]
  datasetList = [lotte_science_dev, lotte_science_test]

  docsList1 = [lotte_writing_dev.docs_store(), lotte_writing_test.docs_store()]
  datasetList1 = [lotte_writing_dev, lotte_writing_test]

  docsList2 = [lotte_technology_dev.docs_store(), lotte_technology_test.docs_store()]
  datasetList2 = [lotte_technology_dev, lotte_technology_test]

  # Extract query-document pairs from LoTTE science with a relecance score of 1 (all qrels)
  for docs, dataset in zip(docsList, datasetList):
    for qrel in dataset.qrels_iter():
      query_id = qrel[0]
      doc_id = qrel[1]

      document = docs.get(doc_id)[1]
      query = ""

      for q in dataset.queries_iter():
        if q[0] == query_id:
          query = q[1]
          break

      cQuery, cDocument = cleanData(query, document)
      pair = {"query" : cQuery, "document": cDocument}
      data.append(pair)

  print('before merge:', len(data))
  data = mergeDuplicates(data)
  jsonData['data'] = data
  jsonData['length'] = len(data)
  print(jsonData['dataset'], jsonData['length'])

  file.write(json.dumps(jsonData))
  file.close()
  data = []

  # Extract query-document pairs from LoTTE writing with a relecance score of 1 (all qrels)
  for docs, dataset in zip(docsList1, datasetList1):
    for qrel in dataset.qrels_iter():
      query_id = qrel[0]
      doc_id = qrel[1]

      document = docs.get(doc_id)[1]
      query = ""

      for q in dataset.queries_iter():
        if q[0] == query_id:
          query = q[1]
          break

      cQuery, cDocument = cleanData(query, document)
      pair = {"query" : cQuery, "document": cDocument}
      data.append(pair)

  print('before merge:', len(data))
  data = mergeDuplicates(data)
  jsonData1['data'] = data
  jsonData1['length'] = len(data)
  print(jsonData1['dataset'], jsonData1['length'])

  file1.write(json.dumps(jsonData1))
  file1.close()
  data = []

  # Extract query-document pairs from LoTTE technology with a relecance score of 1 (all qrels)
  for docs, dataset in zip(docsList2, datasetList2):
    for qrel in dataset.qrels_iter():
      query_id = qrel[0]
      doc_id = qrel[1]

      document = docs.get(doc_id)[1]
      query = ""

      for q in dataset.queries_iter():
        if q[0] == query_id:
          query = q[1]
          break

      cQuery, cDocument = cleanData(query, document)
      pair = {"query" : cQuery, "document": cDocument}
      data.append(pair)

  print('before merge:', len(data))
  data = mergeDuplicates(data)
  jsonData2['data'] = data
  jsonData2['length'] = len(data)
  print(jsonData2['dataset'], jsonData2['length'])

  file2.write(json.dumps(jsonData2))
  file2.close()

# Process the MSMarco v1.1 dataset (Not used)
'''def MSmarco():
  # Load dataset 
  cache_dir="/content/cache/" # Where downloaded dataset will be saved to, CHANGE TO RELEVENT PATH
  msmarco = load_dataset("ms_marco", 'v1.1', cache_dir=cache_dir)
  print(msmarco)

  output_file = path + "/msmarco_data.json"
 
  file = open(output_file, "w")
  
  # Metadata
  jsonData = {"dataset": "msmarco_v1.1", "length": 0, "source": "https://huggingface.co/datasets/ms_marco", "data": None}
  
  data = []

  # Extract query-document pairs from train set
  for question, passage in zip(msmarco['train']['query'], msmarco['train']['passages']):
    context = ""
    for text in passage['passage_text']:
      context += text

    cQuery, cDocument = cleanData(question, context)
    pair = {"query" : cQuery, "document": cDocument}
    data.append(pair)
    
  # Extract query-document pairs from validation set
  for question, passage in zip(msmarco['validation']['query'], msmarco['validation']['passages']):
    context = ""
    for text in passage['passage_text']:
      context += text

    cQuery, cDocument = cleanData(question, context)
    pair = {"query" : cQuery, "document": cDocument}
    data.append(pair)
  
  # Extract query-document pairs from test set
  for question, passage in zip(msmarco['test']['query'], msmarco['test']['passages']):
    context = ""
    for text in passage['passage_text']:
      context += text

    cQuery, cDocument = cleanData(question, context)
    pair = {"query" : cQuery, "document": cDocument}
    data.append(pair)

  jsonData['data'] = data
  jsonData['length'] = len(data)
  print(jsonData['dataset'], jsonData['length'])

  file.write(json.dumps(jsonData))
  file.close()'''

# Process the Msmarco-passage dataset (Not used)
def MSmarco_passage():
  # Load dataset
  msmarco_passage_dev = ir_datasets.load("msmarco-passage/dev/judged")
  msmarco_passage_train = ir_datasets.load("msmarco-passage/train/judged")

  output_file = path + "/msmarco_passage_data.json"

  file = open(output_file, "w")
  # Metadata
  jsonData = {"dataset": "msmarco_passage", "length": 0, "source": "https://ir-datasets.com/msmarco-passage.html", "data": None}

  data = []

  # Load documents
  docsList = [msmarco_passage_dev.docs_store(), msmarco_passage_train.docs_store()]
  datasetList = [msmarco_passage_dev, msmarco_passage_train]

  # Extract query-document pairs with relevance score 1
  for docs, dataset in zip(docsList, datasetList):
    queries = {}
    
    for q in dataset.queries_iter():
      queries[str(q[0])] = q[1]
    
    for qrel in dataset.qrels_iter():
      query_id = qrel[0]
      doc_id = qrel[1]

      try:
        document = docs.get(doc_id)[1]
      except:
        continue

      query = queries[str(query_id)]

      cQuery, cDocument = cleanData(query, document)
      pair = {"query" : cQuery, "document": cDocument}
      data.append(pair)

  print('before merge:', len(data))
  data = mergeDuplicates(data)
  jsonData['data'] = data
  jsonData['length'] = len(data)
  print(jsonData['dataset'], jsonData['length'])

  file.write(json.dumps(jsonData))
  file.close()

# Inspect the processed datasets
def Inspect():
  antique = path + "/IR_small/antique_data.json"
  dbpedia = path + "/IR_small/beir_dbpedia_data.json"
  scifact = path + "/IR_small/beir_scifact_data.json"
  b_english = path + "/IR_small/beir_english_data.json"
  b_gaming = path + "/IR_small/beir_gaming_data.json"
  b_math = path + "/IR_small/beir_mathematica_data.json"
  b_physics = path + "/IR_small/beir_physics_data.json"
  b_programming = path + "/IR_small/beir_programmers_data.json"
  b_stats = path + "/IR_small/beir_stats_data.json"
  b_unix = path + "/IR_small/beir_unix_data.json"
  b_android = path + "/IR_small/beir_android_data.json"
  economics = path + "/IR_small/codec_economics_data.json"
  history = path + "/IR_small/codec_history_data.json"
  politics = path + "/IR_small/codec_politics_data.json"
  lotte_science = path + "/IR_small/lotte_science_data.json"
  lotte_writing = path + "/IR_small/lotte_writing_data.json"
  lotte_tech = path + "/IR_small/lotte_technology_data.json"
  msmarco_passage = path + "/msmarco_passage_data.json" 

  file_list = [dbpedia, scifact, b_english, b_gaming, b_math, b_physics, b_programming, b_stats, b_unix, b_android, economics, history, politics, lotte_science, lotte_writing, lotte_tech, antique]
  #file_list = [msmarco_passage]

  for file in file_list:
    jfile = None
    try:
      jfile = json.load(open(file)) # Load the JSON file
    except:
      continue
    
    emptyQ = 0
    emptyC = 0
    avg_Q_len = 0
    avg_C_len = 0
    min_Q_len = 1000000000
    min_C_len = 1000000000
    max_Q_len = -1
    max_C_len = -1
    count = 0
    count1 = 0

    print(jfile['dataset']) # Print dataset name
    
    for data in jfile['data']:
      # Check for empty queries or documents
      if (data['query'] == '' or data['query'] == None):
        emptyQ += 1
      if data['document'] == '' or data['document'] == None or data['document'] == []:
        emptyC += 1
      
      # Sum the lengths (in words)
      avg_Q_len += len(data['query'].split())
      avg_C_len += len(data['document'].split())

      if (Tokenize == True):
          # Tokenize the context
          source = tokenizer.batch_encode_plus([data['context']], truncation_strategy='do_not_truncate', return_tensors='pt')
          source_ids = source['input_ids'].squeeze()
          source_original_length = len(source_ids)
            
            # Count the number of contexts longer than these lengths
          if source_original_length > 512:
            count += 1

          if source_original_length > 1024:
            count1 += 1

      if len(data['query'].split()) < min_Q_len:
        min_Q_len = len(data['query'].split())

      if len(data['query'].split()) > max_Q_len:
        max_Q_len = len(data['query'].split())

      if len(data['document'].split()) < min_C_len:
        min_C_len = len(data['document'].split())

      if len(data['document'].split()) > max_C_len:
        max_C_len = len(data['document'].split())

    # Calculate average
    avg_Q_len /= jfile['length']
    avg_C_len /= jfile['length']

    # Print out stats
    print(jfile['length'])
    print('empty queries:', emptyQ)
    print('empty document:', emptyC)
    print('min query length:', int(min_Q_len), 'words')
    print('min document length:', int(min_C_len), 'words')
    print('average query length:', int(avg_Q_len), 'words')
    print('average document length:', int(avg_C_len), 'words')
    print('max query length:', int(max_Q_len), 'words')
    print('max document length:', int(max_C_len), 'words')
    print('no. of docs above 512 tokens:', int(count), 'docs')
    print('no. of docs above 1024 tokens:', int(count1), 'docs')

    # Sample dataset
    for i in range(1):
      index = random.randint(0, jfile['length']-1)
      print("query:", jfile['data'][index]['query'])
      print("document:", jfile['data'][index]['document'], '\n')
    
  del jfile

# Uncomment dataset to process
if __name__ == "__main__":
	datasets = []
	# Get datasets
	for arg in sys.argv[1:]:
		datasets.append(arg.lower())

	datasets = set(datasets)

  # Run selected dataset functions
	for dataset in datasets:
		if dataset == "lotte":
			Lotte()

		if dataset == "antique":
			Antique()

		if dataset == "codec":
			Codec()

		if dataset == "beir":
			Beir()

		if dataset == "msmarco":
			MSmarco_passage()

	Inspect()
