import torch
import random

device = 'cuda' # cuda or cpu

# Set random seed for python random library and torch
random.seed(42)
torch.manual_seed(42)

# Print out random numbers
for i in range(4):
    print(random.random())

for i in range(4):
    print(torch.randn(4).to(device))
