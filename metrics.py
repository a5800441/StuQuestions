"""
Generate the automatic metrics (ROUGE, BLEU, GLEU, and METEOR) for the generated questions
Created by: Adam Vere
Date: 
"""

from rouge_score import rouge_scorer
from nltk.translate.bleu_score import sentence_bleu, corpus_bleu
from nltk.translate.meteor_score import single_meteor_score
from nltk import word_tokenize
import nltk
from nltk.translate.gleu_score import sentence_gleu, corpus_gleu

path = "" # Global path variable, no need to change this path

''' Calcuate rouges scores (ROUGE-1, ROUGE-2, ROUGE-L)
# preds (list) = generated questions
# acts (list) = gold questions
# typ (string) = type of test
# path (string) = path to model directory
# This function was adaptaed from Ros et al.'s code: https://github.com/kevinros/INLG2022StudentQuestions/blob/main/transformers_mod/src/prefix.ipynb
'''
def rougeScore(preds, acts, typ, path):
    # Initialize ROUGE scorer
    scorer = rouge_scorer.RougeScorer(["rouge1", "rouge2", "rougeL"], use_stemmer=False)

    # Dictionaries containing each ROUGE variants' scores
    rouge1 = {"precision": [], "recall": [], "fmeasure": []}
    rouge2 = {"precision": [], "recall": [], "fmeasure": []}
    rougeL = {"precision": [], "recall": [], "fmeasure": []}

    for pred, act in zip(preds, acts):
        scores = scorer.score(act, pred)  # Calculate ROUGE scores
        # Extract and scores
        r1_precision, r1_recall, r1_fmeasure = scores["rouge1"]
        r2_precision, r2_recall, r2_fmeasure = scores["rouge2"]
        rL_precision, rL_recall, rL_fmeasure = scores["rougeL"]
        rouge1["precision"].append(r1_precision)
        rouge1["recall"].append(r1_recall)
        rouge1["fmeasure"].append(r1_fmeasure)
        rouge2["precision"].append(r2_precision)
        rouge2["recall"].append(r2_recall)
        rouge2["fmeasure"].append(r2_fmeasure)
        rougeL["precision"].append(rL_precision)
        rougeL["recall"].append(rL_recall)
        rougeL["fmeasure"].append(rL_fmeasure)

    # Calcuate the final score values by averaging
    num_samples = len(preds)
    for key in rouge1:
        rouge1[key] = sum(rouge1[key]) / num_samples
    for key in rouge2:
        rouge2[key] = sum(rouge2[key]) / num_samples
    for key in rougeL:
        rougeL[key] = sum(rougeL[key]) / num_samples

    # Save the results to the text file
    file = open(path + "results.txt", "a")
    file.write("\nROUGE Scores: (" + typ + ")\n")
    file.write("Rouge_1: " + str(rouge1) + "\n")
    file.write("Rouge_2: " + str(rouge2) + "\n")
    file.write("Rouge_L: " + str(rougeL) + "\n")
    file.close()

    return rouge1, rouge2, rougeL


''' Calcuate BLEU scores (BLEU-1, BLEU-2, BLEU-3, BLEU-4)
# preds (list) = generated questions
# acts (list) = gold questions
# typ (string) = type of test
# path (string) = path to model directory'''
def bleuScore(preds, acts, typ, path):
    # Dictionary containing each BLEU scores
    bleu = {
        "bleu-1": [],
        "bleu-2": [],
        "bleu-3": [],
        "bleu-4": [],
        "bleu": [],
        "bleu-corp-1": [],
        "bleu-corp-2": [],
        "bleu-corp-3": [],
        "bleu-corp-4": [],
        "bleu-corp": [],
    }

    # Calculate and save each score to the dictionary
    for pred, act in zip(preds, acts):
        # Split the generated and gold question by words
        pred = pred.split()
        act = act.split()
        bleu["bleu-1"].append(
            sentence_bleu([act], pred, weights=(1, 0, 0, 0), smoothing_function=None)
        )
        bleu["bleu-2"].append(
            sentence_bleu([act], pred, weights=(0, 1, 0, 0), smoothing_function=None)
        )
        bleu["bleu-3"].append(
            sentence_bleu([act], pred, weights=(1, 0, 1, 0), smoothing_function=None)
        )
        bleu["bleu-4"].append(
            sentence_bleu([act], pred, weights=(0, 0, 0, 1), smoothing_function=None)
        )
        bleu["bleu"].append(sentence_bleu([act], pred, smoothing_function=None))
        bleu["bleu-corp-1"].append(
            corpus_bleu([[act]], [pred], weights=(1, 0, 0, 0), smoothing_function=None)
        )
        bleu["bleu-corp-2"].append(
            corpus_bleu([[act]], [pred], weights=(0, 1, 0, 0), smoothing_function=None)
        )
        bleu["bleu-corp-3"].append(
            corpus_bleu([[act]], [pred], weights=(1, 0, 1, 0), smoothing_function=None)
        )
        bleu["bleu-corp-4"].append(
            corpus_bleu([[act]], [pred], weights=(0, 0, 0, 1), smoothing_function=None)
        )
        bleu["bleu-corp"].append(corpus_bleu([[act]], [pred], smoothing_function=None))

    # Average the scores to get the final value for each BLEU metric
    num_samples = len(preds)
    for key in bleu:
        bleu[key] = sum(bleu[key]) / num_samples

    # Save results to the text file
    file = open(path + "results.txt", "a")
    file.write("\nBLEU Scores: (" + typ + ")\n")
    file.write(str(bleu) + "\n")
    file.close()

    return bleu


''' Calcuate METEOR score
# preds (list) = generated questions
# acts (list) = gold questions
# typ (string) = type of test
# path (string) = path to model directory'''
def meteorScore(preds, acts, typ, path):
    # Dictionary containing each METEOR score
    meteor = {"meteor": []}

    # Calculate the metric score for each generated question
    for pred, act in zip(preds, acts):
        # Tokenize the generated and gold question before calculating the score
        pred = word_tokenize(pred)
        act = word_tokenize(act)
        meteor["meteor"].append(single_meteor_score((act), (pred)))

    # Average the scores to get the final value
    num_samples = len(preds)
    for key in meteor:
        meteor[key] = sum(meteor[key]) / num_samples

    # Save results to the text file
    file = open(path + "results.txt", "a")
    file.write("\nMETEOR Scores: (" + typ + ")\n")
    file.write(str(meteor) + "\n")
    file.close()

    return meteor


''' Calcuate GLEU score (GLEU-1, GLEU-2, GLEU-3, GLEU-4)
# preds (list) = generated questions
# acts (list) = gold questions
# typ (string) = type of test
# path (string) = path to model directory'''
def gleuScore(preds, acts, typ, path):
    # Dictionary containing each GLEU scores
    gleu = {
        "gleu-1": [],
        "gleu-2": [],
        "gleu-3": [],
        "gleu-4": [],
        "gleu": [],
        "gleu-corp-1": [],
        "gleu-corp-2": [],
        "gleu-corp-3": [],
        "gleu-corp-4": [],
        "gleu-corp": [],
    }

    # Calculate and save each score to the dictionary
    for pred, act in zip(preds, acts):
        # Split the generated and gold question by words
        pred = pred.split()
        act = act.split()
        gleu["gleu-1"].append(sentence_gleu([act], pred, min_len=1, max_len=1))
        gleu["gleu-2"].append(sentence_gleu([act], pred, min_len=2, max_len=2))
        gleu["gleu-3"].append(sentence_gleu([act], pred, min_len=3, max_len=3))
        gleu["gleu-4"].append(sentence_gleu([act], pred, min_len=4, max_len=4))
        gleu["gleu"].append(sentence_gleu([act], pred))
        gleu["gleu-corp-1"].append(corpus_gleu([[act]], [pred], min_len=1, max_len=1))
        gleu["gleu-corp-2"].append(corpus_gleu([[act]], [pred], min_len=2, max_len=2))
        gleu["gleu-corp-3"].append(corpus_gleu([[act]], [pred], min_len=3, max_len=3))
        gleu["gleu-corp-4"].append(corpus_gleu([[act]], [pred], min_len=4, max_len=4))
        gleu["gleu-corp"].append(corpus_gleu([[act]], [pred]))

    # Average the score to get the final value for each GLEU metric
    num_samples = len(preds)
    for key in gleu:
        gleu[key] = sum(gleu[key]) / num_samples

    # Save results to the text file
    file = open(path + "results.txt", "a")
    file.write("\nGLEU Scores: (" + typ + ")\n")
    file.write(str(gleu) + "\n")
    file.close()

    return gleu


# Main function, pass in directory containing the nltk folder
def main(dir):
    global path

    path = dir
    nltk.data.path.append(path + "/nltk_data")  # Set path to downloaded nltk data
