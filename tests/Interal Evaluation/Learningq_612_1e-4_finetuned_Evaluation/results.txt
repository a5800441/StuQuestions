{'MODEL': 'Learningq_612_1e-4_finetuned', 'TEST_BATCH_SIZE': 1, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'QC_DATASETS': ['learning-q'], 'IR_DATASETS': ['IR-small'], 'TYPE': ''}
4 Example questions 
gamma code is more conservative than delta code?
if the binary code is different than the binary code, how do we know that it's uniform?
if we use multiple ranking methods, how do we know which one is the most relevant?
- how does the music play a lesser role in opera?

Lecture-question test set 

Gold question: for the notation "Term Lexicon" and "DocID Lexicon" at the right, is it corresponding to the "dictionary" and "posting"? Also, what is the third index of those vectors?
Generated Question: if we use integers to sort the data, why do we need to compress the data?

Gold question: Although I get why 3 is 101 in Omega code, I am not quite sure why 5 is 11001 (The 110 part), and how the 0 in the middle works. I hope the professor could explain more during the lecture.
Generated Question: if we use binary code to encode the difference between 5 and 110 followed by 01.

Gold question: I'm still very confused how integer compression actually reduces size of storage since some of the examples make it seem like you're using more bits than before on some inputs
Generated Question: gamma coding is more effective than binary coding?

Gold question: How is this block in "Local" sort being created / partitioned? How do tuples with different doc IDs get grouped together? I am very confused with this whole process.
Generated Question: if we have to write the temporary file into the disk, how do we sort?

Gold question: What's the meaning of "combine all the top-k sets"?
Generated Question: if there are multiple top-k documents, how do we determine the likelihood of finding duplicated documents?

Gold question: Can a cluster for words be compared to clusters containing larger objects, like groups of documents?
Generated Question: if we have multiple documents, how do we cluster them?

Gold question: How does unary code compress binary code? Won't unary code always be more bits than binary?
Generated Question: gamma coding is more efficient than gamma coding?

Gold question: Examples of how to carry out those tests and what does the result mean?
Generated Question: if the variance is high, how do we know that the results are random?

Gold question: Could we get more examples of Statistical Significance Testing?
Generated Question: if the variance is high, how do we know that the results are random?

Gold question: What are the most popular methods for statistical significance testing?
Generated Question: s why we use statistical significance test?

LearningQ test set 

Gold question: can the electric fields interact with one another?
Generated Question: , how do we know that the electric field is in the positive direction?

Gold question: in the seesaw example with the weights, is the distance representative of the center of masses of each  particle '' in that system?
Generated Question: , why is the moment of inertia so important?

Gold question: what allows you to combine a, b and c as a solution when you got each of them from different x values?
Generated Question: , how do we know when to use partial fraction decomposition?

Gold question: the number between these extremes would be 0, so my point is, even though this would seem to be a dumb question, why can't an integer divided by 0 be 0?
Generated Question: if 0/0 is undefined, then why doesn't it just become zero?

Gold question: also, what exactly is a triad in the context of muscle cells and how do they work?
Generated Question: , how do we know that the muscle is contracting?

Gold question: is it to nourish the cornea/lens?
Generated Question: the conjunctiva protects the cornea from friction?

Gold question: how can you find the radius if you only know the circumference?
Generated Question: , how do you find the circumference of a circle?

Gold question: as stated in the video the horizontal component of t3 is taken for the tension of t1 and if you want to take the tension of t3 which is at angle what would be the components of t1 and t2?
Generated Question: t1 and t2 are the same thing?

Gold question: so hitler basically infiltrated the nazi party at ludendorff's suggestion because they both believed in the stab in the back theory?
Generated Question: , what is the meaning of the word'stabbed in the back '?

Gold question: where did the square root symbol originate from?
Generated Question: , what is the difference between a square root and a radical?
