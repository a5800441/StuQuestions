{'MODEL': 'IR_small_512_1e-4_finetuned', 'TEST_BATCH_SIZE': 1, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'QC_DATASETS': ['learning-q'], 'IR_DATASETS': ['IR-small'], 'TYPE': ''}
4 Example questions 
Is gamma or gamma used to sort data?
if we add a parameter to the log of the value of x?
Using a variety of ranking methods is not always effective because there are many valid documents in the pool?
opera is a dramatic art form in which the emotional content is conveyed more through the lyrics than by the actors' performances?

Lecture-question test set 

Gold question: for the notation "Term Lexicon" and "DocID Lexicon" at the right, is it corresponding to the "dictionary" and "posting"? Also, what is the third index of those vectors?
Generated Question: using temporary files instead of using actual memory?

Gold question: Although I get why 3 is 101 in Omega code, I am not quite sure why 5 is 11001 (The 110 part), and how the 0 in the middle works. I hope the professor could explain more during the lecture.
Generated Question: if we have more than two different values?

Gold question: I'm still very confused how integer compression actually reduces size of storage since some of the examples make it seem like you're using more bits than before on some inputs
Generated Question: So what is the difference between binary and gamma coding methods?

Gold question: How is this block in "Local" sort being created / partitioned? How do tuples with different doc IDs get grouped together? I am very confused with this whole process.
Generated Question: if we use the same key as we do for other keys to sort?

Gold question: What's the meaning of "combine all the top-k sets"?
Generated Question: Is there only one way to evaluate a dataset?

Gold question: Can a cluster for words be compared to clusters containing larger objects, like groups of documents?
Generated Question: if the clustering algorithm is used to find common words in a book or two?

Gold question: How does unary code compress binary code? Won't unary code always be more bits than binary?
Generated Question: gamma coding is one of the more effective ways to deal with small numbers?

Gold question: Examples of how to carry out those tests and what does the result mean?
Generated Question: How do we know if a query has a huge impact on the results?

Gold question: Could we get more examples of Statistical Significance Testing?
Generated Question: How do we know if a query has a huge impact on the results?

Gold question: What are the most popular methods for statistical significance testing?
Generated Question: If there is a high variance, why don't we use the average instead of using one?

LearningQ test set 

Gold question: can the electric fields interact with one another?
Generated Question: a lot easier to deal with electric field problems?

Gold question: in the seesaw example with the weights, is the distance representative of the center of masses of each  particle '' in that system?
Generated Question: vs torque vs instantaneous force

Gold question: what allows you to combine a, b and c as a solution when you got each of them from different x values?
Generated Question: if we are dealing with a more complicated partial fraction decomposition problem?

Gold question: the number between these extremes would be 0, so my point is, even though this would seem to be a dumb question, why can't an integer divided by 0 be 0?
Generated Question: mathematicians have never defined what it means to divide by zero?

Gold question: also, what exactly is a triad in the context of muscle cells and how do they work?
Generated Question: and tendons connected to each other?

Gold question: is it to nourish the cornea/lens?
Generated Question: , how do we know the cornea is going to be protected?

Gold question: how can you find the radius if you only know the circumference?
Generated Question: take the principal root of 36 pi and calculate its circumference using pi r squared?

Gold question: as stated in the video the horizontal component of t3 is taken for the tension of t1 and if you want to take the tension of t3 which is at angle what would be the components of t1 and t2?
Generated Question: related to the force problem?

Gold question: so hitler basically infiltrated the nazi party at ludendorff's suggestion because they both believed in the stab in the back theory?
Generated Question: in the military, how did hitler get his start in the world war 1?

Gold question: where did the square root symbol originate from?
Generated Question: the square root of the squared if we don't know what times are equal to and what does radical symbol mean?
