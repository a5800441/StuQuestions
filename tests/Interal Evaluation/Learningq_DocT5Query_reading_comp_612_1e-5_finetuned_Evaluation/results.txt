{'MODEL': 'Learningq_DocT5Query_reading_comp_612_1e-5_finetuned', 'TEST_BATCH_SIZE': 1, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'QC_DATASETS': ['learning-q'], 'IR_DATASETS': ['IR-small'], 'TYPE': ''}
4 Example questions 
do people find that gamma coding works well for inverted index, compression, etc?
exactly how do we determine the log of x in unary code?
if we have multiple ranking methods, how do we know which one is best?
what is the difference between opera and symphonic orchestra?

Lecture-question test set 

Gold question: for the notation "Term Lexicon" and "DocID Lexicon" at the right, is it corresponding to the "dictionary" and "posting"? Also, what is the third index of those vectors?
Generated Question: do we use integers as indexes in the merge sort?

Gold question: Although I get why 3 is 101 in Omega code, I am not quite sure why 5 is 11001 (The 110 part), and how the 0 in the middle works. I hope the professor could explain more during the lecture.
Generated Question: if the value of log (x) is higher than the original, how do we know that the difference is 1 to the flow of log of x?

Gold question: I'm still very confused how integer compression actually reduces size of storage since some of the examples make it seem like you're using more bits than before on some inputs
Generated Question: gamma coding methods are used to find the magnitude of values higher than the original?

Gold question: How is this block in "Local" sort being created / partitioned? How do tuples with different doc IDs get grouped together? I am very confused with this whole process.
Generated Question: if we are using term IDs as a key to sort, how do we know that all entries that share the same term will be grouped together?

Gold question: What's the meaning of "combine all the top-k sets"?
Generated Question: do we combine the top-K documents to form a unit?

Gold question: Can a cluster for words be compared to clusters containing larger objects, like groups of documents?
Generated Question: can text mining be used to cluster articles?

Gold question: How does unary code compress binary code? Won't unary code always be more bits than binary?
Generated Question: is gamma coding less aggressive than unary coding?

Gold question: Examples of how to carry out those tests and what does the result mean?
Generated Question: if there is a large variance, what does it mean that the results might change if we use different queries?

Gold question: Could we get more examples of Statistical Significance Testing?
Generated Question: if there is a large variance, what does it mean that the results might change if we use different queries?

Gold question: What are the most popular methods for statistical significance testing?
Generated Question: is there a difference between the mean and standard deviation in statistical significance tests?

LearningQ test set 

Gold question: can the electric fields interact with one another?
Generated Question: 'Electronic Field' is defined as the amount of electrical force exerted on a charge?

Gold question: in the seesaw example with the weights, is the distance representative of the center of masses of each  particle '' in that system?
Generated Question: 'Measure of Moment' is the same thing as 'Torque'?

Gold question: what allows you to combine a, b and c as a solution when you got each of them from different x values?
Generated Question: if the denominator is higher than the numerator, how do we factor it?

Gold question: the number between these extremes would be 0, so my point is, even though this would seem to be a dumb question, why can't an integer divided by 0 be 0?
Generated Question: if we divide by zero, what does it mean to divide by zero?

Gold question: also, what exactly is a triad in the context of muscle cells and how do they work?
Generated Question: 'smooth' and 'cardial' muscles are examples of what?

Gold question: is it to nourish the cornea/lens?
Generated Question: the cornea protects it from friction?

Gold question: how can you find the radius if you only know the circumference?
Generated Question: if the circle has an area of 36pi, how do we find its circumference?

Gold question: as stated in the video the horizontal component of t3 is taken for the tension of t1 and if you want to take the tension of t3 which is at angle what would be the components of t1 and t2?
Generated Question: 'Tension' is the force exerted by a rope or string?

Gold question: so hitler basically infiltrated the nazi party at ludendorff's suggestion because they both believed in the stab in the back theory?
Generated Question: the stab in the back theory was believed to be true by those who took over and signed the armistice?

Gold question: where did the square root symbol originate from?
Generated Question: the square root of 9 is equal to how many times it's equal to 9?
