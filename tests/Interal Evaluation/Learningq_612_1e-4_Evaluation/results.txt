{'MODEL': 'Learningq_612_1e-4', 'TEST_BATCH_SIZE': 1, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'QC_DATASETS': ['learning-q'], 'IR_DATASETS': ['IR-small'], 'TYPE': ''}
4 Example questions 
gamma code is good for sorting distributions?
binary code '' is the same as  binary code ''?
if there are more than one duplicated documents in the pool then why do we penalize the system?
if a person is going to be in an opera and they want to make the audience feel special, how do they know what to put on their head when they are performing the opera?

Lecture-question test set 

Gold question: for the notation "Term Lexicon" and "DocID Lexicon" at the right, is it corresponding to the "dictionary" and "posting"? Also, what is the third index of those vectors?
Generated Question: if so, how do you make it easier to compress the data?

Gold question: Although I get why 3 is 101 in Omega code, I am not quite sure why 5 is 11001 (The 110 part), and how the 0 in the middle works. I hope the professor could explain more during the lecture.
Generated Question: if the binary code is 2 to the power of x, then why do we have to convert it into a binary code?

Gold question: I'm still very confused how integer compression actually reduces size of storage since some of the examples make it seem like you're using more bits than before on some inputs
Generated Question: if the binary code is 2 to the 1st power then why do we have to convert it to a fraction?

Gold question: How is this block in "Local" sort being created / partitioned? How do tuples with different doc IDs get grouped together? I am very confused with this whole process.
Generated Question: if you put the temporary file into the disk as a temporary file would it be possible to process the next batch of documents?

Gold question: What's the meaning of "combine all the top-k sets"?
Generated Question: if there are duplicated documents in the pool of all possible outcomes then why is it that we have to use as many different return ranking methods as possible?

Gold question: Can a cluster for words be compared to clusters containing larger objects, like groups of documents?
Generated Question: text mining can be done in a very sophisticated way ''?

Gold question: How does unary code compress binary code? Won't unary code always be more bits than binary?
Generated Question: gamma coding is an example of gamma coding?

Gold question: Examples of how to carry out those tests and what does the result mean?
Generated Question: if the question asks to compare two sets of data and we are asked to find the mean of each set of data then what is the probability that the difference between the two sets of data will be equal?

Gold question: Could we get more examples of Statistical Significance Testing?
Generated Question: if the question asks to compare two sets of data and we are asked to find the mean of each set of data then what is the probability that the difference between the two sets of data will be equal?

Gold question: What are the most popular methods for statistical significance testing?
Generated Question: if you have a set of data that is not consistent in any way, how do you know which one to use?

LearningQ test set 

Gold question: can the electric fields interact with one another?
Generated Question: if the electric field is in the opposite direction to the positive charge, then why do we have to take the negative charge into account?

Gold question: in the seesaw example with the weights, is the distance representative of the center of masses of each  particle '' in that system?
Generated Question: if we have to find the moment of inertia then how do we know that the force is perpendicular to the distance from the axis of rotation?

Gold question: what allows you to combine a, b and c as a solution when you got each of them from different x values?
Generated Question: if the denominator is a degree higher than the numerator, how do you factor it?

Gold question: the number between these extremes would be 0, so my point is, even though this would seem to be a dumb question, why can't an integer divided by 0 be 0?
Generated Question: 0 divided by zero is undefined?

Gold question: also, what exactly is a triad in the context of muscle cells and how do they work?
Generated Question: , what is the difference between skeletal and cardiac muscles?

Gold question: is it to nourish the cornea/lens?
Generated Question: if the conjunctiva is thicker than the cornea, then why does it have to be thicker than the cornea?

Gold question: how can you find the radius if you only know the circumference?
Generated Question: if the radius is 6 and the circumference is 12pi, how do you find the circumference?

Gold question: as stated in the video the horizontal component of t3 is taken for the tension of t1 and if you want to take the tension of t3 which is at angle what would be the components of t1 and t2?
Generated Question: t1 is the force exerted by the rope on the box, and t2 is the force that pulls the box down?

Gold question: so hitler basically infiltrated the nazi party at ludendorff's suggestion because they both believed in the stab in the back theory?
Generated Question: if so, how did hitler get the idea to create such a great product?

Gold question: where did the square root symbol originate from?
Generated Question: if you have a negative number to the power of a negative number, how do you find the square root?
