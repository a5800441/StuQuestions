{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_DocT5Query_reading_comp_612_1e-5', 'EPOCH_SETTING': '5', 'NAME': 'Learningq_DocT5Query_reading_comp_612_1e-5_10_1e-5', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 10, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 1e-05, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': ''}

1 0 tensor(1.9933, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.5736, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.4181, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(2.9769, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.9938, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.4405008170564297

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2495819941109073, 'recall': 0.16719070121303062, 'fmeasure': 0.18187308092563845}
Rouge_2: {'precision': 0.05692506645896477, 'recall': 0.03035610855184329, 'fmeasure': 0.03579997265046237}
Rouge_L: {'precision': 0.2266072052462879, 'recall': 0.15174251737626257, 'fmeasure': 0.16605567364220272}

2 0 tensor(4.1458, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.2764, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.1484, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.8618, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.4370, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.385463692374149

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2539687014460063, 'recall': 0.17336770346095914, 'fmeasure': 0.19148110472991287}
Rouge_2: {'precision': 0.06236059138601513, 'recall': 0.03358306340262233, 'fmeasure': 0.04086339615724002}
Rouge_L: {'precision': 0.22992706511623442, 'recall': 0.15582949861231718, 'fmeasure': 0.1726478027257966}

3 0 tensor(2.8194, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.1444, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.5678, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.5904, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(5.3070, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.3462886123333946

ROUGE Scores: (val)
Rouge_1: {'precision': 0.243269298553446, 'recall': 0.1645273000631096, 'fmeasure': 0.1818855142553029}
Rouge_2: {'precision': 0.07228458923374179, 'recall': 0.043487113632438526, 'fmeasure': 0.04998293410425436}
Rouge_L: {'precision': 0.2198468807142784, 'recall': 0.150840148527005, 'fmeasure': 0.16592425382328047}

4 0 tensor(3.8614, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.8099, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(4.2063, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.4963, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.6123, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.3370577127246532

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20471102429897098, 'recall': 0.1591296973760481, 'fmeasure': 0.1630657499773703}
Rouge_2: {'precision': 0.03770653988749702, 'recall': 0.028808160498740933, 'fmeasure': 0.030102178482626973}
Rouge_L: {'precision': 0.1721291523103193, 'recall': 0.13615201917798986, 'fmeasure': 0.13864957570858918}

5 0 tensor(3.0154, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.9574, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.7655, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.9693, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(2.3857, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 3.3330832335908536

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20187016234488797, 'recall': 0.16524406747328363, 'fmeasure': 0.16758030386133202}
Rouge_2: {'precision': 0.03231921092090584, 'recall': 0.026811716725724702, 'fmeasure': 0.02693114988786957}
Rouge_L: {'precision': 0.17186793048193402, 'recall': 0.13703154387192662, 'fmeasure': 0.14022289979074584}

6 0 tensor(0.9081, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.2011, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(2.8157, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.0582, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.1799, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 0 3.3273555535381125

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19611700503236415, 'recall': 0.15269109148062673, 'fmeasure': 0.1589296895470886}
Rouge_2: {'precision': 0.03737705326688377, 'recall': 0.026652231290088313, 'fmeasure': 0.028678746946027484}
Rouge_L: {'precision': 0.17583953546526354, 'recall': 0.1357167550272275, 'fmeasure': 0.14128507167670928}

7 0 tensor(1.6905, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(4.3355, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(2.5167, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(2.7725, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(2.9050, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 0 3.3519541954590104

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20232899938388735, 'recall': 0.15829686810650095, 'fmeasure': 0.16195114043179404}
Rouge_2: {'precision': 0.041159734803802604, 'recall': 0.028558937062100342, 'fmeasure': 0.029767255508755856}
Rouge_L: {'precision': 0.17580794685598689, 'recall': 0.13356662493657684, 'fmeasure': 0.13842727992176607}

8 0 tensor(3.2576, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(2.8613, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(0.9883, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(2.5763, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(2.4090, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 0 3.3589444544355747

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19216838729944144, 'recall': 0.15798357870980387, 'fmeasure': 0.16146760721447292}
Rouge_2: {'precision': 0.023765029697233084, 'recall': 0.015755284240945678, 'fmeasure': 0.017292440536171037}
Rouge_L: {'precision': 0.16762829664781698, 'recall': 0.13314863797755508, 'fmeasure': 0.13767201651968816}

9 0 tensor(1.1744, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(3.6381, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(1.9526, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(2.3638, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(2.0314, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 9 0 3.3927361359030512

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2051973962118528, 'recall': 0.14794047941230248, 'fmeasure': 0.1566159364555968}
Rouge_2: {'precision': 0.0351081122267563, 'recall': 0.02067781712478058, 'fmeasure': 0.023477745904162548}
Rouge_L: {'precision': 0.1796408767370881, 'recall': 0.1280838173847846, 'fmeasure': 0.13659207599729056}

10 0 tensor(2.3086, device='cuda:0', grad_fn=<NllLossBackward0>)

10 100 tensor(1.8784, device='cuda:0', grad_fn=<NllLossBackward0>)

10 200 tensor(2.3143, device='cuda:0', grad_fn=<NllLossBackward0>)

10 300 tensor(2.1806, device='cuda:0', grad_fn=<NllLossBackward0>)

10 400 tensor(0.8743, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 10 0 3.4077564871917336

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20920485009763223, 'recall': 0.16421765866929228, 'fmeasure': 0.16656992970858525}
Rouge_2: {'precision': 0.020309328148311195, 'recall': 0.015046847036530161, 'fmeasure': 0.01596094476058702}
Rouge_L: {'precision': 0.1716335082667008, 'recall': 0.1363193599013656, 'fmeasure': 0.13754182326821882}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.18894726558433547, 'recall': 0.1381524486250028, 'fmeasure': 0.1457269749108249}
Rouge_2: {'precision': 0.031158778328589647, 'recall': 0.02807929544304333, 'fmeasure': 0.027163594285048453}
Rouge_L: {'precision': 0.16582452878235343, 'recall': 0.12660612983079853, 'fmeasure': 0.1315047475516245}

1 0 tensor(2.9819, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.7531, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.3196, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.3120, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.4694, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.8882395494274977

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22813926468876244, 'recall': 0.1369878544391755, 'fmeasure': 0.15930409407068716}
Rouge_2: {'precision': 0.0445216080348792, 'recall': 0.02840529875425439, 'fmeasure': 0.03196659087395677}
Rouge_L: {'precision': 0.2048161586540352, 'recall': 0.1286059159479803, 'fmeasure': 0.14728966759392448}

2 0 tensor(2.3137, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.3276, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.9610, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.0508, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(1.3752, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.338994084334955

ROUGE Scores: (val)
Rouge_1: {'precision': 0.263742376579347, 'recall': 0.16457821503970277, 'fmeasure': 0.18438406260938187}
Rouge_2: {'precision': 0.07290903540903541, 'recall': 0.04555026091232258, 'fmeasure': 0.05117043856489927}
Rouge_L: {'precision': 0.22813200118238625, 'recall': 0.1448302844921809, 'fmeasure': 0.16126203497302793}

3 0 tensor(2.2621, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(1.8664, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(4.2501, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.1147, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.6504, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.3013328212063486

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2551970591118632, 'recall': 0.1634550973823239, 'fmeasure': 0.1821311495074578}
Rouge_2: {'precision': 0.07087788566447104, 'recall': 0.0498664845922626, 'fmeasure': 0.05394363201011552}
Rouge_L: {'precision': 0.2189746502836437, 'recall': 0.14498916260492356, 'fmeasure': 0.15975316337284642}

4 0 tensor(3.7956, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.8326, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(3.1024, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.5828, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.5342, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 10 3.295932528449268

ROUGE Scores: (val)
Rouge_1: {'precision': 0.25032073425705903, 'recall': 0.16473207022491784, 'fmeasure': 0.1814307627350544}
Rouge_2: {'precision': 0.06544971945796911, 'recall': 0.0430989239771502, 'fmeasure': 0.047453379521168806}
Rouge_L: {'precision': 0.20170532228978189, 'recall': 0.13523084471821964, 'fmeasure': 0.14813150324062743}

5 0 tensor(4.4835, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.5596, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.7173, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.3079, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(3.0019, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 10 3.285089752296122

ROUGE Scores: (val)
Rouge_1: {'precision': 0.25268888154280433, 'recall': 0.1705021169240054, 'fmeasure': 0.18526888962502114}
Rouge_2: {'precision': 0.0646434846040298, 'recall': 0.04144767069532261, 'fmeasure': 0.04597429835151862}
Rouge_L: {'precision': 0.20938639154913782, 'recall': 0.14394512138977852, 'fmeasure': 0.15552119657247532}

6 0 tensor(2.2581, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(2.9307, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(2.3993, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(2.9746, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.5055, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 10 3.2923532491777

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23829692938332367, 'recall': 0.16081610930400722, 'fmeasure': 0.17325372718764512}
Rouge_2: {'precision': 0.048217404108724056, 'recall': 0.03631693003251071, 'fmeasure': 0.03692398008666802}
Rouge_L: {'precision': 0.19009086985326687, 'recall': 0.13261799076849665, 'fmeasure': 0.14083166492675397}

7 0 tensor(3.3066, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(3.8595, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(2.6914, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(2.5683, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(2.9426, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 10 3.2968390358657373

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23751169024662558, 'recall': 0.149468144327735, 'fmeasure': 0.16694967064403127}
Rouge_2: {'precision': 0.04050333633368067, 'recall': 0.02349840637964322, 'fmeasure': 0.02773954558993058}
Rouge_L: {'precision': 0.20149695971969997, 'recall': 0.12913671284672337, 'fmeasure': 0.14307602232006936}

8 0 tensor(2.4777, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(2.6793, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(1.9992, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(0.6765, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(3.0575, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 10 3.307676655490224

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2367949323114316, 'recall': 0.1527704247944206, 'fmeasure': 0.1689074046854337}
Rouge_2: {'precision': 0.06419294686558112, 'recall': 0.03548151983489437, 'fmeasure': 0.04144104283583672}
Rouge_L: {'precision': 0.2050160422631728, 'recall': 0.13459093547699916, 'fmeasure': 0.14787077072219357}

9 0 tensor(3.1398, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(2.4607, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(3.2239, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(1.2974, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(2.6532, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 9 10 3.3281592268769336

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22787896631672575, 'recall': 0.13548464995824858, 'fmeasure': 0.15342490397342506}
Rouge_2: {'precision': 0.04341701658774829, 'recall': 0.022463558029894953, 'fmeasure': 0.027622879938219852}
Rouge_L: {'precision': 0.1987267619134454, 'recall': 0.11983288807585855, 'fmeasure': 0.13546903906351515}

10 0 tensor(1.7376, device='cuda:0', grad_fn=<NllLossBackward0>)

10 100 tensor(2.1358, device='cuda:0', grad_fn=<NllLossBackward0>)

10 200 tensor(2.8707, device='cuda:0', grad_fn=<NllLossBackward0>)

10 300 tensor(2.7337, device='cuda:0', grad_fn=<NllLossBackward0>)

10 400 tensor(1.3552, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 10 10 3.3542934917822116

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21300082330390793, 'recall': 0.12862971905279022, 'fmeasure': 0.14579110431261744}
Rouge_2: {'precision': 0.04373570196740928, 'recall': 0.022541631419988807, 'fmeasure': 0.027321827486470048}
Rouge_L: {'precision': 0.1871036513701506, 'recall': 0.11510649596392965, 'fmeasure': 0.1298567799138687}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.24378497029965823, 'recall': 0.13771145160603387, 'fmeasure': 0.16446137558534593}
Rouge_2: {'precision': 0.04873569582871909, 'recall': 0.02217084534348633, 'fmeasure': 0.027961831992185028}
Rouge_L: {'precision': 0.21296982291474345, 'recall': 0.12002917893060115, 'fmeasure': 0.14338421864064665}

1 0 tensor(4.5009, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(1.7897, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(2.7851, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(4.7796, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(3.9440, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.3313920750456343

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21408840688501704, 'recall': 0.11059760725726957, 'fmeasure': 0.13234322651401187}
Rouge_2: {'precision': 0.030414500753483804, 'recall': 0.012036901053992395, 'fmeasure': 0.016414083260817138}
Rouge_L: {'precision': 0.1792325847410593, 'recall': 0.09763396328920664, 'fmeasure': 0.11423431160400334}

2 0 tensor(3.6132, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.2769, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.3701, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.1653, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.5029, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.266280182337357

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22995543211867925, 'recall': 0.12798134001443207, 'fmeasure': 0.14668474698792772}
Rouge_2: {'precision': 0.031287638914757554, 'recall': 0.01160440571292683, 'fmeasure': 0.015656777813272068}
Rouge_L: {'precision': 0.1848591912597265, 'recall': 0.105213481148393, 'fmeasure': 0.11988665088917373}

3 0 tensor(2.0539, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.3043, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.5663, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.7669, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.6802, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.2337804719553156

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24947376514386013, 'recall': 0.14393419917989436, 'fmeasure': 0.1648567706309824}
Rouge_2: {'precision': 0.06304509990950667, 'recall': 0.02769481949112394, 'fmeasure': 0.035461735080054374}
Rouge_L: {'precision': 0.21065091191422716, 'recall': 0.12634880694507022, 'fmeasure': 0.14248989477398089}

4 0 tensor(2.4547, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(1.7729, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.5096, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.8069, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(1.1977, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 20 3.2080827294769936

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24578456604877344, 'recall': 0.13898148229596924, 'fmeasure': 0.15758176907195845}
Rouge_2: {'precision': 0.06026059627754543, 'recall': 0.026954755942118793, 'fmeasure': 0.03382092726021721}
Rouge_L: {'precision': 0.20788235526520668, 'recall': 0.12235400831559406, 'fmeasure': 0.13684209774595574}

5 0 tensor(2.5554, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(0.7848, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(4.1312, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.2770, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.9651, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 20 3.210775510739472

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23552925788652818, 'recall': 0.13639301334499654, 'fmeasure': 0.15865004805364968}
Rouge_2: {'precision': 0.036270053665236536, 'recall': 0.017390374468216677, 'fmeasure': 0.02222347478122802}
Rouge_L: {'precision': 0.19154194502098168, 'recall': 0.11718684859733511, 'fmeasure': 0.13359124016960805}

6 0 tensor(3.0838, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(2.1789, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(3.9090, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.8224, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.4528, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 20 3.2089583368624672

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23444131425339398, 'recall': 0.1435409714557019, 'fmeasure': 0.16001815139485925}
Rouge_2: {'precision': 0.05160136473695794, 'recall': 0.02363577690293196, 'fmeasure': 0.029973592803524064}
Rouge_L: {'precision': 0.20467151747529716, 'recall': 0.13099064711736141, 'fmeasure': 0.14384798558951817}

7 0 tensor(2.3162, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(2.6079, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(3.0536, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(1.9973, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(3.2410, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 20 3.2139736575595403

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24696142962528012, 'recall': 0.14421419535353117, 'fmeasure': 0.16531556570306727}
Rouge_2: {'precision': 0.05848672534376292, 'recall': 0.026172514348357975, 'fmeasure': 0.03249677682536999}
Rouge_L: {'precision': 0.21171969643528646, 'recall': 0.12267152479288673, 'fmeasure': 0.1410698227574353}

8 0 tensor(3.3987, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(4.2816, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(2.6976, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(2.3228, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(4.0393, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 20 3.220843629816831

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23802341892072695, 'recall': 0.13919522889459957, 'fmeasure': 0.1586245746307445}
Rouge_2: {'precision': 0.03903746912221489, 'recall': 0.014563689569975063, 'fmeasure': 0.019944977846273956}
Rouge_L: {'precision': 0.19901285321524606, 'recall': 0.12182828171413332, 'fmeasure': 0.13678636936640387}

9 0 tensor(2.5635, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(3.0036, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(3.2820, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(2.2935, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(2.9386, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 9 20 3.2313981965436773

ROUGE Scores: (val)
Rouge_1: {'precision': 0.25704525180439536, 'recall': 0.13018534485451774, 'fmeasure': 0.15618310446319872}
Rouge_2: {'precision': 0.04557071929953285, 'recall': 0.0178910091983914, 'fmeasure': 0.024576272443858325}
Rouge_L: {'precision': 0.20999555256914754, 'recall': 0.11041324022520801, 'fmeasure': 0.1309695130520954}

10 0 tensor(2.4217, device='cuda:0', grad_fn=<NllLossBackward0>)

10 100 tensor(3.3803, device='cuda:0', grad_fn=<NllLossBackward0>)

10 200 tensor(2.5447, device='cuda:0', grad_fn=<NllLossBackward0>)

10 300 tensor(2.1346, device='cuda:0', grad_fn=<NllLossBackward0>)

10 400 tensor(2.3126, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 10 20 3.2443964637942235

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22200145594961146, 'recall': 0.12861715399871262, 'fmeasure': 0.14694117497423065}
Rouge_2: {'precision': 0.027519246013762464, 'recall': 0.013162596648911899, 'fmeasure': 0.017022522289330558}
Rouge_L: {'precision': 0.1783879602174717, 'recall': 0.1101204113997744, 'fmeasure': 0.12349078541278388}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.17735381902048566, 'recall': 0.13044123050573, 'fmeasure': 0.14068429552144768}
Rouge_2: {'precision': 0.014312847646180979, 'recall': 0.010171685008286314, 'fmeasure': 0.011233432429084603}
Rouge_L: {'precision': 0.1321767738434405, 'recall': 0.10141066485235732, 'fmeasure': 0.10703835632285878}

avg_f1_1: 0.18068852001933378

avg_f1_2: 0.046462767064808085

avg_f1_L: 0.1592526077627809
