{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_DocT5Query_reading_comp_612_1e-5', 'EPOCH_SETTING': '5', 'NAME': 'Learningq_DocT5Query_reading_comp_612_1e-5_7_1e-5', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 7, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 1e-05, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': ''}

1 0 tensor(1.9933, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.5736, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.4181, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(2.9769, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.9938, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.4405008170564297

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2495819941109073, 'recall': 0.16719070121303062, 'fmeasure': 0.18187308092563845}
Rouge_2: {'precision': 0.05692506645896477, 'recall': 0.03035610855184329, 'fmeasure': 0.03579997265046237}
Rouge_L: {'precision': 0.2266072052462879, 'recall': 0.15174251737626257, 'fmeasure': 0.16605567364220272}

2 0 tensor(4.1458, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.2764, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.1484, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.8618, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.4370, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.385463692374149

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2539687014460063, 'recall': 0.17336770346095914, 'fmeasure': 0.19148110472991287}
Rouge_2: {'precision': 0.06236059138601513, 'recall': 0.03358306340262233, 'fmeasure': 0.04086339615724002}
Rouge_L: {'precision': 0.22992706511623442, 'recall': 0.15582949861231718, 'fmeasure': 0.1726478027257966}

3 0 tensor(2.8194, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.1444, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.5678, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.5904, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(5.3070, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.3462886123333946

ROUGE Scores: (val)
Rouge_1: {'precision': 0.243269298553446, 'recall': 0.1645273000631096, 'fmeasure': 0.1818855142553029}
Rouge_2: {'precision': 0.07228458923374179, 'recall': 0.043487113632438526, 'fmeasure': 0.04998293410425436}
Rouge_L: {'precision': 0.2198468807142784, 'recall': 0.150840148527005, 'fmeasure': 0.16592425382328047}

4 0 tensor(3.8614, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.8099, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(4.2063, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.4963, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.6123, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.3370577127246532

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20471102429897098, 'recall': 0.1591296973760481, 'fmeasure': 0.1630657499773703}
Rouge_2: {'precision': 0.03770653988749702, 'recall': 0.028808160498740933, 'fmeasure': 0.030102178482626973}
Rouge_L: {'precision': 0.1721291523103193, 'recall': 0.13615201917798986, 'fmeasure': 0.13864957570858918}

5 0 tensor(3.0154, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.9574, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.7655, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.9693, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(2.3857, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 3.3330832335908536

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20187016234488797, 'recall': 0.16524406747328363, 'fmeasure': 0.16758030386133202}
Rouge_2: {'precision': 0.03231921092090584, 'recall': 0.026811716725724702, 'fmeasure': 0.02693114988786957}
Rouge_L: {'precision': 0.17186793048193402, 'recall': 0.13703154387192662, 'fmeasure': 0.14022289979074584}

6 0 tensor(0.9081, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.2011, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(2.8157, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.0582, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.1799, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 0 3.3273555535381125

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19611700503236415, 'recall': 0.15269109148062673, 'fmeasure': 0.1589296895470886}
Rouge_2: {'precision': 0.03737705326688377, 'recall': 0.026652231290088313, 'fmeasure': 0.028678746946027484}
Rouge_L: {'precision': 0.17583953546526354, 'recall': 0.1357167550272275, 'fmeasure': 0.14128507167670928}

7 0 tensor(1.6905, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(4.3355, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(2.5167, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(2.7725, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(2.9050, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 0 3.3519541954590104

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20232899938388735, 'recall': 0.15829686810650095, 'fmeasure': 0.16195114043179404}
Rouge_2: {'precision': 0.041159734803802604, 'recall': 0.028558937062100342, 'fmeasure': 0.029767255508755856}
Rouge_L: {'precision': 0.17580794685598689, 'recall': 0.13356662493657684, 'fmeasure': 0.13842727992176607}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.21080738148006395, 'recall': 0.15116092549780868, 'fmeasure': 0.16327755649800915}
Rouge_2: {'precision': 0.025704012968163912, 'recall': 0.014734239123806272, 'fmeasure': 0.01753428732271474}
Rouge_L: {'precision': 0.17514544747932767, 'recall': 0.12753731425379025, 'fmeasure': 0.13646990233745865}

1 0 tensor(2.9819, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.7531, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.3196, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.3120, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.4694, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.8882395494274977

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22813926468876244, 'recall': 0.1369878544391755, 'fmeasure': 0.15930409407068716}
Rouge_2: {'precision': 0.0445216080348792, 'recall': 0.02840529875425439, 'fmeasure': 0.03196659087395677}
Rouge_L: {'precision': 0.2048161586540352, 'recall': 0.1286059159479803, 'fmeasure': 0.14728966759392448}

2 0 tensor(2.3137, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.3276, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.9610, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.0508, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(1.3752, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.338994084334955

ROUGE Scores: (val)
Rouge_1: {'precision': 0.263742376579347, 'recall': 0.16457821503970277, 'fmeasure': 0.18438406260938187}
Rouge_2: {'precision': 0.07290903540903541, 'recall': 0.04555026091232258, 'fmeasure': 0.05117043856489927}
Rouge_L: {'precision': 0.22813200118238625, 'recall': 0.1448302844921809, 'fmeasure': 0.16126203497302793}

3 0 tensor(2.2621, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(1.8664, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(4.2501, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.1147, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.6504, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.3013328212063486

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2551970591118632, 'recall': 0.1634550973823239, 'fmeasure': 0.1821311495074578}
Rouge_2: {'precision': 0.07087788566447104, 'recall': 0.0498664845922626, 'fmeasure': 0.05394363201011552}
Rouge_L: {'precision': 0.2189746502836437, 'recall': 0.14498916260492356, 'fmeasure': 0.15975316337284642}

4 0 tensor(3.7956, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.8326, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(3.1024, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.5828, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.5342, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 10 3.295932528449268

ROUGE Scores: (val)
Rouge_1: {'precision': 0.25032073425705903, 'recall': 0.16473207022491784, 'fmeasure': 0.1814307627350544}
Rouge_2: {'precision': 0.06544971945796911, 'recall': 0.0430989239771502, 'fmeasure': 0.047453379521168806}
Rouge_L: {'precision': 0.20170532228978189, 'recall': 0.13523084471821964, 'fmeasure': 0.14813150324062743}

5 0 tensor(4.4835, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.5596, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.7173, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.3079, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(3.0019, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 10 3.285089752296122

ROUGE Scores: (val)
Rouge_1: {'precision': 0.25268888154280433, 'recall': 0.1705021169240054, 'fmeasure': 0.18526888962502114}
Rouge_2: {'precision': 0.0646434846040298, 'recall': 0.04144767069532261, 'fmeasure': 0.04597429835151862}
Rouge_L: {'precision': 0.20938639154913782, 'recall': 0.14394512138977852, 'fmeasure': 0.15552119657247532}

6 0 tensor(2.2581, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(2.9307, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(2.3993, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(2.9746, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.5055, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 10 3.2923532491777

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23829692938332367, 'recall': 0.16081610930400722, 'fmeasure': 0.17325372718764512}
Rouge_2: {'precision': 0.048217404108724056, 'recall': 0.03631693003251071, 'fmeasure': 0.03692398008666802}
Rouge_L: {'precision': 0.19009086985326687, 'recall': 0.13261799076849665, 'fmeasure': 0.14083166492675397}

7 0 tensor(3.3066, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(3.8595, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(2.6914, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(2.5683, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(2.9426, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 10 3.2968390358657373

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23751169024662558, 'recall': 0.149468144327735, 'fmeasure': 0.16694967064403127}
Rouge_2: {'precision': 0.04050333633368067, 'recall': 0.02349840637964322, 'fmeasure': 0.02773954558993058}
Rouge_L: {'precision': 0.20149695971969997, 'recall': 0.12913671284672337, 'fmeasure': 0.14307602232006936}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.2589450804873082, 'recall': 0.14776640634122118, 'fmeasure': 0.17407304296003412}
Rouge_2: {'precision': 0.06281349749202007, 'recall': 0.026396132434489843, 'fmeasure': 0.03350833466737998}
Rouge_L: {'precision': 0.22410950262847446, 'recall': 0.12641248212990985, 'fmeasure': 0.15016171793359795}

1 0 tensor(4.5009, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(1.7897, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(2.7851, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(4.7796, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(3.9440, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.3313920750456343

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21408840688501704, 'recall': 0.11059760725726957, 'fmeasure': 0.13234322651401187}
Rouge_2: {'precision': 0.030414500753483804, 'recall': 0.012036901053992395, 'fmeasure': 0.016414083260817138}
Rouge_L: {'precision': 0.1792325847410593, 'recall': 0.09763396328920664, 'fmeasure': 0.11423431160400334}

2 0 tensor(3.6132, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.2769, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.3701, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.1653, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.5029, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.266280182337357

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22995543211867925, 'recall': 0.12798134001443207, 'fmeasure': 0.14668474698792772}
Rouge_2: {'precision': 0.031287638914757554, 'recall': 0.01160440571292683, 'fmeasure': 0.015656777813272068}
Rouge_L: {'precision': 0.1848591912597265, 'recall': 0.105213481148393, 'fmeasure': 0.11988665088917373}

3 0 tensor(2.0539, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.3043, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.5663, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.7669, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.6802, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.2337804719553156

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24947376514386013, 'recall': 0.14393419917989436, 'fmeasure': 0.1648567706309824}
Rouge_2: {'precision': 0.06304509990950667, 'recall': 0.02769481949112394, 'fmeasure': 0.035461735080054374}
Rouge_L: {'precision': 0.21065091191422716, 'recall': 0.12634880694507022, 'fmeasure': 0.14248989477398089}

4 0 tensor(2.4547, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(1.7729, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.5096, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.8069, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(1.1977, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 20 3.2080827294769936

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24578456604877344, 'recall': 0.13898148229596924, 'fmeasure': 0.15758176907195845}
Rouge_2: {'precision': 0.06026059627754543, 'recall': 0.026954755942118793, 'fmeasure': 0.03382092726021721}
Rouge_L: {'precision': 0.20788235526520668, 'recall': 0.12235400831559406, 'fmeasure': 0.13684209774595574}

5 0 tensor(2.5554, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(0.7848, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(4.1312, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.2770, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.9651, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 20 3.210775510739472

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23552925788652818, 'recall': 0.13639301334499654, 'fmeasure': 0.15865004805364968}
Rouge_2: {'precision': 0.036270053665236536, 'recall': 0.017390374468216677, 'fmeasure': 0.02222347478122802}
Rouge_L: {'precision': 0.19154194502098168, 'recall': 0.11718684859733511, 'fmeasure': 0.13359124016960805}

6 0 tensor(3.0838, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(2.1789, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(3.9090, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.8224, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.4528, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 20 3.2089583368624672

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23444131425339398, 'recall': 0.1435409714557019, 'fmeasure': 0.16001815139485925}
Rouge_2: {'precision': 0.05160136473695794, 'recall': 0.02363577690293196, 'fmeasure': 0.029973592803524064}
Rouge_L: {'precision': 0.20467151747529716, 'recall': 0.13099064711736141, 'fmeasure': 0.14384798558951817}

7 0 tensor(2.3162, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(2.6079, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(3.0536, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(1.9973, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(3.2410, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 20 3.2139736575595403

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24696142962528012, 'recall': 0.14421419535353117, 'fmeasure': 0.16531556570306727}
Rouge_2: {'precision': 0.05848672534376292, 'recall': 0.026172514348357975, 'fmeasure': 0.03249677682536999}
Rouge_L: {'precision': 0.21171969643528646, 'recall': 0.12267152479288673, 'fmeasure': 0.1410698227574353}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.21444228015865446, 'recall': 0.15096419498294283, 'fmeasure': 0.16504767730300984}
Rouge_2: {'precision': 0.017770295548073325, 'recall': 0.01161201422639331, 'fmeasure': 0.013519413132939704}
Rouge_L: {'precision': 0.16488433323228643, 'recall': 0.11855836067652374, 'fmeasure': 0.12895802234056908}

avg_f1_1: 0.18068852001933378

avg_f1_2: 0.046462767064808085

avg_f1_L: 0.1592526077627809
