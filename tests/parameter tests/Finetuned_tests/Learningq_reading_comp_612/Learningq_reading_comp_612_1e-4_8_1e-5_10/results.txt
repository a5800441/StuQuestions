{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_reading_comp_612_1e-4', 'EPOCH_SETTING': '10', 'NAME': 'Learningq_reading_comp_612_1e-4_8_1e-5_10', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 8, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 1e-05, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': ''}

1 0 tensor(2.8512, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(5.0574, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.7178, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.1359, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(5.2010, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.918766286413548

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2085967068270158, 'recall': 0.13088760886702017, 'fmeasure': 0.1477256653725555}
Rouge_2: {'precision': 0.021271398836073232, 'recall': 0.013983905664466247, 'fmeasure': 0.014928566411617262}
Rouge_L: {'precision': 0.1924882860575781, 'recall': 0.12145713211613622, 'fmeasure': 0.1365543310955426}

2 0 tensor(4.7149, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.9116, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.0116, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.6743, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.6518, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.7359915102942516

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20385636021229234, 'recall': 0.12268215749297676, 'fmeasure': 0.13904723569276753}
Rouge_2: {'precision': 0.029066663834728064, 'recall': 0.01747740585752294, 'fmeasure': 0.018596179071201376}
Rouge_L: {'precision': 0.1808745397728448, 'recall': 0.1088898349607555, 'fmeasure': 0.12311642897212961}

3 0 tensor(3.6473, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.4206, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(4.0238, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.2093, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(6.0588, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.658564628180811

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2155519621621316, 'recall': 0.12040655805524529, 'fmeasure': 0.14369904050960472}
Rouge_2: {'precision': 0.04149748556528218, 'recall': 0.017563028905563088, 'fmeasure': 0.022986643721067377}
Rouge_L: {'precision': 0.19421755269212893, 'recall': 0.1067844309485718, 'fmeasure': 0.12853487956698081}

4 0 tensor(4.5807, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(4.3752, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(5.4765, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.4410, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.6026, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.6290620444184643

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2331732768173446, 'recall': 0.12952303009544613, 'fmeasure': 0.15440325743313363}
Rouge_2: {'precision': 0.04720223279545313, 'recall': 0.01898970647519338, 'fmeasure': 0.025156624926385956}
Rouge_L: {'precision': 0.2083077939010142, 'recall': 0.11193368376731617, 'fmeasure': 0.13564432883886549}

5 0 tensor(3.7675, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.3491, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(4.3747, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.3415, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(2.8345, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 3.59229624170368

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21092855504095465, 'recall': 0.12912476618417437, 'fmeasure': 0.14921184144867264}
Rouge_2: {'precision': 0.039156888309430676, 'recall': 0.018526300706322427, 'fmeasure': 0.023761764986304296}
Rouge_L: {'precision': 0.1912552434184905, 'recall': 0.114311645513626, 'fmeasure': 0.1337509486864941}

6 0 tensor(1.5713, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.6462, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(3.2410, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.2861, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.7215, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 0 3.5656023884223678

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19248849079357555, 'recall': 0.11951178204739321, 'fmeasure': 0.13609399005110193}
Rouge_2: {'precision': 0.023917137476459516, 'recall': 0.012222405374773215, 'fmeasure': 0.01487591451948509}
Rouge_L: {'precision': 0.1706908439959288, 'recall': 0.10408908659242616, 'fmeasure': 0.11948718954599631}

7 0 tensor(2.2825, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(5.1290, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(2.9917, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(3.1918, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(3.5896, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 0 3.5514806013996316

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21084424050525744, 'recall': 0.13135017039113392, 'fmeasure': 0.14910125619757225}
Rouge_2: {'precision': 0.03896959913909067, 'recall': 0.02267676417881979, 'fmeasure': 0.025603552477667213}
Rouge_L: {'precision': 0.18826070351494079, 'recall': 0.11788045059565455, 'fmeasure': 0.1332589063248496}

8 0 tensor(4.2633, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(3.6572, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(1.1336, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(2.7008, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(2.6252, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 0 3.5348340434543157

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2296826290470359, 'recall': 0.13373351817354764, 'fmeasure': 0.1564306092981499}
Rouge_2: {'precision': 0.04551282051282052, 'recall': 0.021967346329517252, 'fmeasure': 0.028142816430476714}
Rouge_L: {'precision': 0.20582901091375674, 'recall': 0.11611726842868376, 'fmeasure': 0.138152712746307}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.19708187514791284, 'recall': 0.1311009235694879, 'fmeasure': 0.14326791678962184}
Rouge_2: {'precision': 0.02470935772822565, 'recall': 0.017936764396107818, 'fmeasure': 0.018633588554106975}
Rouge_L: {'precision': 0.16905795985984665, 'recall': 0.11310551926959435, 'fmeasure': 0.12334191124329077}

1 0 tensor(4.5934, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(5.4872, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.9805, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.0075, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.9154, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.7652176400510275

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2221255596255596, 'recall': 0.10489564484652925, 'fmeasure': 0.12886696729562763}
Rouge_2: {'precision': 0.023563157709499172, 'recall': 0.010412812175156019, 'fmeasure': 0.013323242067160101}
Rouge_L: {'precision': 0.19309469888738168, 'recall': 0.09362411070600049, 'fmeasure': 0.11374188679225807}

2 0 tensor(1.9582, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.3647, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.7287, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.3412, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(1.5440, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.656622183032152

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22873926547707027, 'recall': 0.11437869670479814, 'fmeasure': 0.13900781358731537}
Rouge_2: {'precision': 0.034006000774293464, 'recall': 0.014286882351709052, 'fmeasure': 0.019439730059579743}
Rouge_L: {'precision': 0.1999255487060364, 'recall': 0.1007206629442052, 'fmeasure': 0.12225044100894311}

3 0 tensor(2.5785, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.2107, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(4.3354, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.9520, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.8880, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.5973326382113666

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23633338206508925, 'recall': 0.11279924608985348, 'fmeasure': 0.14068893687032458}
Rouge_2: {'precision': 0.03932486889803963, 'recall': 0.01617556562678514, 'fmeasure': 0.02143211030063452}
Rouge_L: {'precision': 0.208293265000582, 'recall': 0.10114281144503123, 'fmeasure': 0.12502540770360607}

4 0 tensor(4.0199, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.5445, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(3.0423, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(4.1657, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.9478, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 10 3.563214678589891

ROUGE Scores: (val)
Rouge_1: {'precision': 0.221795041002358, 'recall': 0.10961941949335928, 'fmeasure': 0.13314114844774394}
Rouge_2: {'precision': 0.046393951576878395, 'recall': 0.017496385416508928, 'fmeasure': 0.023562658616032087}
Rouge_L: {'precision': 0.19582585436243974, 'recall': 0.09798696154934303, 'fmeasure': 0.1187767820799907}

5 0 tensor(4.5477, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(3.2335, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.7201, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.1018, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(4.1945, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 10 3.5329602578791177

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24566639322736883, 'recall': 0.11331139354666772, 'fmeasure': 0.14385147077399693}
Rouge_2: {'precision': 0.04026809910956252, 'recall': 0.016009980063532554, 'fmeasure': 0.021814244409083312}
Rouge_L: {'precision': 0.21414327542376316, 'recall': 0.10112815823311881, 'fmeasure': 0.1277459614122552}

6 0 tensor(2.6693, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(3.4630, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(2.7728, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(3.6654, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.7517, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 10 3.5176715385623094

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2328329191134068, 'recall': 0.1128147102455839, 'fmeasure': 0.14072527121133746}
Rouge_2: {'precision': 0.0383637133797596, 'recall': 0.015948095245550176, 'fmeasure': 0.020970226226286654}
Rouge_L: {'precision': 0.20182114633334136, 'recall': 0.09824875515426663, 'fmeasure': 0.12234834348557017}

7 0 tensor(3.9937, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(4.9323, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(2.6042, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(2.8141, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(3.0650, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 10 3.5142065163065745

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2414342432635115, 'recall': 0.12015244756341319, 'fmeasure': 0.14763868579530573}
Rouge_2: {'precision': 0.03797172731704183, 'recall': 0.015939589749239803, 'fmeasure': 0.02098673199224496}
Rouge_L: {'precision': 0.21426727066970963, 'recall': 0.10753054614678093, 'fmeasure': 0.13129617751191758}

8 0 tensor(3.1490, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(3.6738, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(2.2760, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(1.1472, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(3.6456, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 10 3.5029938388161543

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23732097847951497, 'recall': 0.1153027192883107, 'fmeasure': 0.14354641382397504}
Rouge_2: {'precision': 0.04641152600074295, 'recall': 0.01748149324358757, 'fmeasure': 0.023226586141078316}
Rouge_L: {'precision': 0.20940316458609137, 'recall': 0.10438178312920382, 'fmeasure': 0.12880031241713108}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.25580349882675474, 'recall': 0.13520387882610155, 'fmeasure': 0.15407846725932772}
Rouge_2: {'precision': 0.08156004202515832, 'recall': 0.02461652688396874, 'fmeasure': 0.030626240749831488}
Rouge_L: {'precision': 0.23407558204069834, 'recall': 0.11660451636559001, 'fmeasure': 0.13751284637333797}

1 0 tensor(5.3627, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(2.2462, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(2.8343, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(5.8523, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.4907, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.664279089135639

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2088416291806123, 'recall': 0.08498677472058985, 'fmeasure': 0.10572917153882723}
Rouge_2: {'precision': 0.024092009685230027, 'recall': 0.004958665299738415, 'fmeasure': 0.007789243157258842}
Rouge_L: {'precision': 0.18280798768086906, 'recall': 0.07601220125727844, 'fmeasure': 0.09350647437459689}

2 0 tensor(4.4005, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.8480, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.5526, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.7935, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(4.5954, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.5723464408163297

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1982136922565636, 'recall': 0.08987239650197604, 'fmeasure': 0.11229504685321398}
Rouge_2: {'precision': 0.02318045862412762, 'recall': 0.0051999365922039496, 'fmeasure': 0.008285236473602652}
Rouge_L: {'precision': 0.16591689577731447, 'recall': 0.0770697216231274, 'fmeasure': 0.09530414360412823}

3 0 tensor(2.5991, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.4200, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.5992, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(4.2997, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.1312, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.501940659547256

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2104029977357363, 'recall': 0.0984212473719089, 'fmeasure': 0.12053842429297884}
Rouge_2: {'precision': 0.03011226062073519, 'recall': 0.007118579705555619, 'fmeasure': 0.011185212638908685}
Rouge_L: {'precision': 0.18489859085309573, 'recall': 0.08719507380896539, 'fmeasure': 0.10613962861804771}

4 0 tensor(3.0026, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(1.9403, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.9113, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(5.0320, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(1.9344, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 20 3.460131006725764

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2440137546069749, 'recall': 0.1124444683030246, 'fmeasure': 0.13795281638672421}
Rouge_2: {'precision': 0.03775221953188055, 'recall': 0.017525954073679316, 'fmeasure': 0.021547002646913817}
Rouge_L: {'precision': 0.21999328355260558, 'recall': 0.10428002053577888, 'fmeasure': 0.12653058900256028}

5 0 tensor(3.1723, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(0.9249, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(4.4713, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.6090, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(2.1796, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 20 3.435882207700762

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24950466892141857, 'recall': 0.11525796354429939, 'fmeasure': 0.1395404553893764}
Rouge_2: {'precision': 0.05220608017218186, 'recall': 0.01996135220107718, 'fmeasure': 0.025430250299160764}
Rouge_L: {'precision': 0.22328484354905087, 'recall': 0.10747884213285569, 'fmeasure': 0.12860603139915597}

6 0 tensor(3.4484, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(2.6981, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(4.3984, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(2.0791, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.8836, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 20 3.41744345123485

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23427250715386305, 'recall': 0.10277470311090718, 'fmeasure': 0.12913910922307736}
Rouge_2: {'precision': 0.04795534032822168, 'recall': 0.020807527292808287, 'fmeasure': 0.02663802572057434}
Rouge_L: {'precision': 0.21729583975346684, 'recall': 0.09789723311717637, 'fmeasure': 0.12186440809244402}

7 0 tensor(2.4125, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(3.1090, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(3.7077, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(2.6130, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(4.1084, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 20 3.391053477586326

ROUGE Scores: (val)
Rouge_1: {'precision': 0.26738285255234406, 'recall': 0.12706163201117002, 'fmeasure': 0.1543394412626601}
Rouge_2: {'precision': 0.05417675544794189, 'recall': 0.022628424440357058, 'fmeasure': 0.029028414860298904}
Rouge_L: {'precision': 0.24447398176211735, 'recall': 0.12011259892892696, 'fmeasure': 0.14439576293662884}

8 0 tensor(4.2207, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(4.5664, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(3.5628, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(2.8301, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(4.4108, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 20 3.3836354830507505

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24805406457948823, 'recall': 0.11871362600085505, 'fmeasure': 0.14449248387352467}
Rouge_2: {'precision': 0.045244821092278724, 'recall': 0.018922007459415826, 'fmeasure': 0.023965988299639085}
Rouge_L: {'precision': 0.2219787745211474, 'recall': 0.11007428341234819, 'fmeasure': 0.13204775182395373}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.19047917103472656, 'recall': 0.11423992735482243, 'fmeasure': 0.13509517210166883}
Rouge_2: {'precision': 0.03136390358612581, 'recall': 0.021847010409101912, 'fmeasure': 0.023525733525733526}
Rouge_L: {'precision': 0.17837090892646448, 'recall': 0.10722199672578069, 'fmeasure': 0.12632488746931522}

avg_f1_1: 0.15280291211870523

avg_f1_2: 0.02691129663560257

avg_f1_L: 0.1379482177316178
