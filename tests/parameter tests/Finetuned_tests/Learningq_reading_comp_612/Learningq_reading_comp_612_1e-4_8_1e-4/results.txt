{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_reading_comp_612_1e-4', 'EPOCH_SETTING': '5', 'NAME': 'Learningq_reading_comp_612_1e-4_8_1e-4', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 8, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': ''}

1 0 tensor(2.2931, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.1229, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.3052, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.0091, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.6398, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.4315178424625072

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2269198909921479, 'recall': 0.1175495034017864, 'fmeasure': 0.1435815787147382}
Rouge_2: {'precision': 0.04007447354904982, 'recall': 0.017557488168467014, 'fmeasure': 0.022975177049071517}
Rouge_L: {'precision': 0.20296166317575773, 'recall': 0.10407031386472926, 'fmeasure': 0.12718754676998026}

2 0 tensor(3.0767, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.1440, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.6693, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.5619, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.3766, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.424374610690747

ROUGE Scores: (val)
Rouge_1: {'precision': 0.16341936699613666, 'recall': 0.10901150668701563, 'fmeasure': 0.11979105979716491}
Rouge_2: {'precision': 0.021188275001834324, 'recall': 0.012795130689801938, 'fmeasure': 0.014806253129954792}
Rouge_L: {'precision': 0.14534412081770007, 'recall': 0.09415727259746688, 'fmeasure': 0.10472994458367428}

3 0 tensor(2.1295, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.3797, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.3085, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(1.8160, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.6627, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.497617470005811

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20112247893902926, 'recall': 0.11451368231164143, 'fmeasure': 0.13636716937026513}
Rouge_2: {'precision': 0.029365690806368774, 'recall': 0.009269177516261209, 'fmeasure': 0.013857689761604947}
Rouge_L: {'precision': 0.17838943348664182, 'recall': 0.0998859089911419, 'fmeasure': 0.11974526361506957}

4 0 tensor(2.4200, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.7890, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.7950, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(1.3082, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.5206, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.6708965887457636

ROUGE Scores: (val)
Rouge_1: {'precision': 0.16119283259113765, 'recall': 0.10767226555805899, 'fmeasure': 0.11609319711562367}
Rouge_2: {'precision': 0.03170567662093086, 'recall': 0.010993023691102225, 'fmeasure': 0.013537606532740859}
Rouge_L: {'precision': 0.1511515556007081, 'recall': 0.10000985645865652, 'fmeasure': 0.10801404396480677}

5 0 tensor(2.4019, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.1225, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.6223, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.4519, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.4086, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 3.8968422776561673

ROUGE Scores: (val)
Rouge_1: {'precision': 0.15796988121462063, 'recall': 0.08527708211698289, 'fmeasure': 0.099885015278961}
Rouge_2: {'precision': 0.013293650793650795, 'recall': 0.0052149676743636795, 'fmeasure': 0.006921043406324405}
Rouge_L: {'precision': 0.1355090409348687, 'recall': 0.07098854808008527, 'fmeasure': 0.08461199585531491}

6 0 tensor(0.6933, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.2352, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.6202, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.3182, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.5059, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 0 4.0972169172965875

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1745818281087473, 'recall': 0.09788685342959877, 'fmeasure': 0.1133021052824965}
Rouge_2: {'precision': 0.03573446327683616, 'recall': 0.014485028783233305, 'fmeasure': 0.019515955898188508}
Rouge_L: {'precision': 0.15375185770250577, 'recall': 0.08492190914665199, 'fmeasure': 0.09891394719381305}

7 0 tensor(0.5385, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.1574, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(0.9001, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(1.3518, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.5516, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 0 4.445455977472208

ROUGE Scores: (val)
Rouge_1: {'precision': 0.16857390348889534, 'recall': 0.12745090375238263, 'fmeasure': 0.1325272087774269}
Rouge_2: {'precision': 0.013161461890275448, 'recall': 0.010392331489385157, 'fmeasure': 0.010477067761518757}
Rouge_L: {'precision': 0.15107856141728207, 'recall': 0.10942514420119935, 'fmeasure': 0.116778451929607}

8 0 tensor(0.5303, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(1.0495, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(0.0879, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(0.7441, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.2694, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 0 4.697374612598096

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1837636531866827, 'recall': 0.12962972166164077, 'fmeasure': 0.13683295606291368}
Rouge_2: {'precision': 0.019688307271210733, 'recall': 0.00987488444549755, 'fmeasure': 0.012835577651474786}
Rouge_L: {'precision': 0.1426074645201361, 'recall': 0.10171761843075709, 'fmeasure': 0.10633068035861701}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.17496591839792378, 'recall': 0.11444555174184837, 'fmeasure': 0.13029637195192295}
Rouge_2: {'precision': 0.014855427591276648, 'recall': 0.006863394149450577, 'fmeasure': 0.008824912554399829}
Rouge_L: {'precision': 0.14528969839151507, 'recall': 0.0980467556260711, 'fmeasure': 0.11042623372500876}

1 0 tensor(3.4597, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.4052, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.2613, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(2.7498, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.3145, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.392252198079737

ROUGE Scores: (val)
Rouge_1: {'precision': 0.27478905499888273, 'recall': 0.1257810046078557, 'fmeasure': 0.15360501153468353}
Rouge_2: {'precision': 0.06365077812351844, 'recall': 0.015452459001770034, 'fmeasure': 0.02255917392045092}
Rouge_L: {'precision': 0.23184674503720404, 'recall': 0.10929055152343095, 'fmeasure': 0.1315190154022667}

2 0 tensor(1.4001, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.5527, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.3784, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.5191, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(1.3736, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.3779659373004263

ROUGE Scores: (val)
Rouge_1: {'precision': 0.195130044355045, 'recall': 0.12020910528077648, 'fmeasure': 0.13302160264369722}
Rouge_2: {'precision': 0.0420871691675135, 'recall': 0.019846085077472455, 'fmeasure': 0.023297644060070705}
Rouge_L: {'precision': 0.1672318601745016, 'recall': 0.10008048934552909, 'fmeasure': 0.11158301816279795}

3 0 tensor(1.7310, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(1.3293, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.7915, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.3712, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.2913, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.4565993387524676

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2237995357273467, 'recall': 0.1328883586579657, 'fmeasure': 0.1477638684561289}
Rouge_2: {'precision': 0.03620544873754116, 'recall': 0.020309255056135275, 'fmeasure': 0.022843700457637093}
Rouge_L: {'precision': 0.18686118155800255, 'recall': 0.1137791057883598, 'fmeasure': 0.12450498028224408}

4 0 tensor(2.3982, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(1.7146, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.9648, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.5692, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.5547, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 10 3.563240491762394

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20783531598668548, 'recall': 0.14762292736173754, 'fmeasure': 0.15389076202387586}
Rouge_2: {'precision': 0.02636148597222372, 'recall': 0.017069728268031555, 'fmeasure': 0.018860963834409664}
Rouge_L: {'precision': 0.17042099786963757, 'recall': 0.12174918454313419, 'fmeasure': 0.12635932163422517}

5 0 tensor(3.2738, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.1618, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.3818, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.1045, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.4884, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 10 3.8327982876358964

ROUGE Scores: (val)
Rouge_1: {'precision': 0.230987670666652, 'recall': 0.12937894046834877, 'fmeasure': 0.1524563300966431}
Rouge_2: {'precision': 0.0326647607135412, 'recall': 0.021614221518781434, 'fmeasure': 0.024965428363460173}
Rouge_L: {'precision': 0.20692064690450623, 'recall': 0.11824569615429886, 'fmeasure': 0.13795204951415035}

6 0 tensor(0.6060, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(2.3404, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(1.5548, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.3312, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.0887, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 10 4.002833561199467

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24648781914176832, 'recall': 0.14752602875105778, 'fmeasure': 0.16688284328323888}
Rouge_2: {'precision': 0.04604976795267052, 'recall': 0.028784979584212313, 'fmeasure': 0.03210079612651267}
Rouge_L: {'precision': 0.21423802910154183, 'recall': 0.12813827069322115, 'fmeasure': 0.14546556848630876}

7 0 tensor(0.6231, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(2.1628, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(0.6695, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.5991, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.4879, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 10 4.315568415130057

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22194924376595823, 'recall': 0.12630679142530493, 'fmeasure': 0.1483060291963432}
Rouge_2: {'precision': 0.03783209729936302, 'recall': 0.01976408835555095, 'fmeasure': 0.02460367152878505}
Rouge_L: {'precision': 0.18777726079914023, 'recall': 0.10931702647141224, 'fmeasure': 0.12736124908443008}

8 0 tensor(0.2625, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(1.1150, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(0.1510, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(0.2439, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(1.8437, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 10 4.518732288988625

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20418387090673346, 'recall': 0.12849887613616842, 'fmeasure': 0.14230454687940583}
Rouge_2: {'precision': 0.02874881323212517, 'recall': 0.016640688220218608, 'fmeasure': 0.01892798233687686}
Rouge_L: {'precision': 0.16924106500408492, 'recall': 0.11000708662796678, 'fmeasure': 0.12022134830678062}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.18414913659711696, 'recall': 0.1391908146107047, 'fmeasure': 0.14014315993642154}
Rouge_2: {'precision': 0.04224184388343075, 'recall': 0.027875315536524316, 'fmeasure': 0.02811907557963105}
Rouge_L: {'precision': 0.15999691423048035, 'recall': 0.11084891381792178, 'fmeasure': 0.1175972082226635}

1 0 tensor(4.7214, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(1.7403, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(2.7372, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(4.8088, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.2418, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.2705586936514255

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23229093383629767, 'recall': 0.10634950000175053, 'fmeasure': 0.13016726740523937}
Rouge_2: {'precision': 0.04580490131337589, 'recall': 0.01606820090059661, 'fmeasure': 0.02093581927775001}
Rouge_L: {'precision': 0.20131103802589345, 'recall': 0.09778456793482769, 'fmeasure': 0.11743117709668392}

2 0 tensor(3.5751, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.2913, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.9444, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.0946, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.2132, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.2478795455673994

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2804678337527159, 'recall': 0.1221588974025657, 'fmeasure': 0.15200626577815943}
Rouge_2: {'precision': 0.08009208305818473, 'recall': 0.022903313940388297, 'fmeasure': 0.031821049074969875}
Rouge_L: {'precision': 0.2500824940453423, 'recall': 0.11122452624918094, 'fmeasure': 0.13706610257523275}

3 0 tensor(1.4040, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.2911, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.2469, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.5117, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(0.8339, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.285052394462844

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21822586095753438, 'recall': 0.10877835276616302, 'fmeasure': 0.12818364852246586}
Rouge_2: {'precision': 0.04744111820383007, 'recall': 0.01694184243875239, 'fmeasure': 0.02145338452863247}
Rouge_L: {'precision': 0.19997910549741899, 'recall': 0.09994536805607605, 'fmeasure': 0.11683706883889403}

4 0 tensor(1.1036, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(0.4740, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.6085, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.3831, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(0.3405, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 20 3.45700801681664

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22796590489498608, 'recall': 0.10272943752465685, 'fmeasure': 0.1281167184402106}
Rouge_2: {'precision': 0.05874361043852569, 'recall': 0.022432712822769426, 'fmeasure': 0.030418879337124602}
Rouge_L: {'precision': 0.21142072690913014, 'recall': 0.09293123605364768, 'fmeasure': 0.11677334313381797}

5 0 tensor(1.5604, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(0.1844, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.5980, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.3304, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.9105, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 20 3.728823660793951

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21076113152384338, 'recall': 0.0986369554159124, 'fmeasure': 0.12197468558803769}
Rouge_2: {'precision': 0.048997847726661284, 'recall': 0.018666674893840083, 'fmeasure': 0.02550200174631695}
Rouge_L: {'precision': 0.18839094050958458, 'recall': 0.09005087925483318, 'fmeasure': 0.11011323100305384}

6 0 tensor(2.3632, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.9857, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(1.7990, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.7297, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.9398, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 20 4.016080238051334

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22440028537984671, 'recall': 0.1276538450920927, 'fmeasure': 0.14788472311389506}
Rouge_2: {'precision': 0.045597493991784796, 'recall': 0.023758979104635498, 'fmeasure': 0.028256103358412596}
Rouge_L: {'precision': 0.19382523702065077, 'recall': 0.11077824546130124, 'fmeasure': 0.1274594327769064}

7 0 tensor(0.7404, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(0.8261, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.4229, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.6517, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.7123, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 20 4.186713803622682

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20661939554210107, 'recall': 0.10288374693437044, 'fmeasure': 0.12231781336296053}
Rouge_2: {'precision': 0.03756547816866859, 'recall': 0.01276481582989085, 'fmeasure': 0.017594688149875325}
Rouge_L: {'precision': 0.18915785519920478, 'recall': 0.09533845542718108, 'fmeasure': 0.11245841517960938}

8 0 tensor(0.7453, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(2.4496, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(1.0190, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(0.5102, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(2.7213, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 20 4.363594805790206

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19041091838698448, 'recall': 0.124852593508029, 'fmeasure': 0.1361806925237542}
Rouge_2: {'precision': 0.027353052587218506, 'recall': 0.015994269997586137, 'fmeasure': 0.018315047863263875}
Rouge_L: {'precision': 0.15995527731818282, 'recall': 0.10310380780597313, 'fmeasure': 0.11313960330491851}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.14857427765064504, 'recall': 0.11537647873647187, 'fmeasure': 0.12447225593584174}
Rouge_2: {'precision': 0.014666239110683555, 'recall': 0.01210826210826211, 'fmeasure': 0.012772030086558236}
Rouge_L: {'precision': 0.11773153365000628, 'recall': 0.09263349766015744, 'fmeasure': 0.09957297359554305}

avg_f1_1: 0.15415689592537885

avg_f1_2: 0.028965674083518023

avg_f1_L: 0.13657307261050725
