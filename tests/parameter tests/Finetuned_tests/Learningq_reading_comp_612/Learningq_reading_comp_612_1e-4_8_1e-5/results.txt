{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_reading_comp_612_1e-4', 'EPOCH_SETTING': '5', 'NAME': 'Learningq_reading_comp_612_1e-4_8_1e-5', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 8, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 1e-05, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': ''}

1 0 tensor(2.2931, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.4363, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.2677, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.2118, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.6340, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.6244776248931885

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2506117234930794, 'recall': 0.13830344852086546, 'fmeasure': 0.16604599149631}
Rouge_2: {'precision': 0.03917706587198113, 'recall': 0.017475414884054387, 'fmeasure': 0.02191409780409593}
Rouge_L: {'precision': 0.21886825509706864, 'recall': 0.12055596564810629, 'fmeasure': 0.14529412600854352}

2 0 tensor(4.3463, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.6958, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.9241, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.2589, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.6025, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.553753637661368

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21460074765159512, 'recall': 0.12777134763103346, 'fmeasure': 0.15029849339686022}
Rouge_2: {'precision': 0.03402786700393878, 'recall': 0.018109118034730216, 'fmeasure': 0.021874052334986113}
Rouge_L: {'precision': 0.19112751655124535, 'recall': 0.11329522817610048, 'fmeasure': 0.13391313767950053}

3 0 tensor(3.3492, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.2761, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.9140, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.8136, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(5.8117, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.518537386999292

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23771073559209152, 'recall': 0.14191281848985227, 'fmeasure': 0.16506899576245085}
Rouge_2: {'precision': 0.042695299051231254, 'recall': 0.022262239165722078, 'fmeasure': 0.027164731070962143}
Rouge_L: {'precision': 0.2149706225977412, 'recall': 0.129608597287326, 'fmeasure': 0.15062919684984943}

4 0 tensor(4.4425, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(4.1972, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(5.2369, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.8701, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.2821, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.496370317572254

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2142747083425049, 'recall': 0.13405600473199164, 'fmeasure': 0.15390465642704768}
Rouge_2: {'precision': 0.041799114256741376, 'recall': 0.021182924610716174, 'fmeasure': 0.025729543325725274}
Rouge_L: {'precision': 0.19823354611490202, 'recall': 0.12541991873424066, 'fmeasure': 0.1431340162072549}

5 0 tensor(3.5778, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.9102, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(4.3550, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.1911, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(2.5674, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 3.4767165982117088

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22758103478442465, 'recall': 0.14273807961382248, 'fmeasure': 0.1643821313253654}
Rouge_2: {'precision': 0.04676382939094804, 'recall': 0.023418987175660516, 'fmeasure': 0.02930606233795601}
Rouge_L: {'precision': 0.2046567368601267, 'recall': 0.12795865000388446, 'fmeasure': 0.14766512469678908}

6 0 tensor(1.2631, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.2361, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(3.0119, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.1960, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.5869, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 0 3.454406025046009

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2355609827195071, 'recall': 0.1525178243511944, 'fmeasure': 0.17222904225433705}
Rouge_2: {'precision': 0.043065832472612135, 'recall': 0.022494182225431838, 'fmeasure': 0.027488695976142032}
Rouge_L: {'precision': 0.20763772894879573, 'recall': 0.13254644478360345, 'fmeasure': 0.1505499882849173}

7 0 tensor(2.0581, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(4.6355, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(2.7114, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(3.0642, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(3.5000, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 0 3.451204492884167

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2077046305859865, 'recall': 0.1263285039871613, 'fmeasure': 0.14910197717227167}
Rouge_2: {'precision': 0.037919942580959536, 'recall': 0.021959396015249048, 'fmeasure': 0.026618208233391876}
Rouge_L: {'precision': 0.18541152820813836, 'recall': 0.11562494201241293, 'fmeasure': 0.1352183100241238}

8 0 tensor(3.8924, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(3.4836, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(1.0267, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(2.7137, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(2.4864, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 0 3.4495792338403604

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21330627940797428, 'recall': 0.13987175908699664, 'fmeasure': 0.15646849400019316}
Rouge_2: {'precision': 0.03875126756482689, 'recall': 0.02244728628759032, 'fmeasure': 0.02518902588952022}
Rouge_L: {'precision': 0.19701626057558264, 'recall': 0.12705900686746477, 'fmeasure': 0.1435002948214829}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.26128190467813106, 'recall': 0.15193138914051954, 'fmeasure': 0.18080244196648576}
Rouge_2: {'precision': 0.049445941898772085, 'recall': 0.027866061062363414, 'fmeasure': 0.03352597001334611}
Rouge_L: {'precision': 0.22931670007141702, 'recall': 0.13360744807341, 'fmeasure': 0.15948717379210853}

1 0 tensor(3.4597, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.7526, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.4982, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(2.7087, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.6258, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.57639427010606

ROUGE Scores: (val)
Rouge_1: {'precision': 0.25070844324695407, 'recall': 0.13339273341558833, 'fmeasure': 0.15678241355321998}
Rouge_2: {'precision': 0.03819992053303991, 'recall': 0.015460364966750783, 'fmeasure': 0.020004992176224916}
Rouge_L: {'precision': 0.20704825590897474, 'recall': 0.10984951428381466, 'fmeasure': 0.12910503483361954}

2 0 tensor(1.7610, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.1759, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.9197, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.1564, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(1.6049, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.516034864797825

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2600898215048142, 'recall': 0.13707254237133037, 'fmeasure': 0.16276734708967053}
Rouge_2: {'precision': 0.03881673881673881, 'recall': 0.017470902541975523, 'fmeasure': 0.022418127808561682}
Rouge_L: {'precision': 0.22541185898431232, 'recall': 0.12068851284733001, 'fmeasure': 0.1422667306521934}

3 0 tensor(2.3717, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.0932, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(4.3120, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.0408, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.8474, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.4658169172159057

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2474805174500296, 'recall': 0.11603544961469762, 'fmeasure': 0.14599875258447687}
Rouge_2: {'precision': 0.05205187766163376, 'recall': 0.018761342533993707, 'fmeasure': 0.02566081460748185}
Rouge_L: {'precision': 0.21597513259098625, 'recall': 0.10220985134283027, 'fmeasure': 0.12826129655276872}

4 0 tensor(4.0655, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.0925, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.9675, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.8537, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.6784, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 10 3.4413235863534415

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23839082665302166, 'recall': 0.12313566710313907, 'fmeasure': 0.15011333399224405}
Rouge_2: {'precision': 0.05468931475029037, 'recall': 0.020370303514640795, 'fmeasure': 0.027809127384292903}
Rouge_L: {'precision': 0.20338237304700707, 'recall': 0.10718080793032732, 'fmeasure': 0.12952000311440004}

5 0 tensor(4.6150, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(3.0014, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.5918, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.6158, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(3.9359, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 10 3.421292590658839

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2347789654550774, 'recall': 0.12191691339001294, 'fmeasure': 0.14411770873958227}
Rouge_2: {'precision': 0.04994192799070848, 'recall': 0.018660132664077324, 'fmeasure': 0.025518278109387577}
Rouge_L: {'precision': 0.20459559868634467, 'recall': 0.11009434824274071, 'fmeasure': 0.12877851227040482}

6 0 tensor(2.5409, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(3.2222, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(2.6417, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(3.5100, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.5222, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 10 3.4084398259476916

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2325233520265802, 'recall': 0.1246582083076392, 'fmeasure': 0.14989279645648954}
Rouge_2: {'precision': 0.054106530326042526, 'recall': 0.021291416360306074, 'fmeasure': 0.028467162133784377}
Rouge_L: {'precision': 0.20816275327573747, 'recall': 0.11309137422656199, 'fmeasure': 0.13535531421277652}

7 0 tensor(3.8158, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(4.7851, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(2.5098, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(2.6803, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(2.9292, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 10 3.3970883365084483

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24819645423124617, 'recall': 0.12742702142390006, 'fmeasure': 0.15205684938228659}
Rouge_2: {'precision': 0.04941752014922746, 'recall': 0.01658818765656997, 'fmeasure': 0.022669200123976718}
Rouge_L: {'precision': 0.21564242289672697, 'recall': 0.1132409193279685, 'fmeasure': 0.1340830333700261}

8 0 tensor(3.1090, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(3.2621, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(2.2791, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(0.9998, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(3.5100, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 10 3.388188971251976

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23052013147063363, 'recall': 0.12081528367099745, 'fmeasure': 0.14230577192071164}
Rouge_2: {'precision': 0.03914801323337908, 'recall': 0.018180094476612077, 'fmeasure': 0.0227228921031535}
Rouge_L: {'precision': 0.19947776597704858, 'recall': 0.10848414657749557, 'fmeasure': 0.12661776857335852}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.25107770706265914, 'recall': 0.12892977398278355, 'fmeasure': 0.15508503548031083}
Rouge_2: {'precision': 0.06343669250645995, 'recall': 0.01858159594843766, 'fmeasure': 0.025346278254126657}
Rouge_L: {'precision': 0.22359062695930002, 'recall': 0.11323504176958347, 'fmeasure': 0.1362610512544054}

1 0 tensor(4.7214, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(1.7724, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(2.4972, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(5.2403, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.3718, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.5071129808991643

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2193868513783767, 'recall': 0.10350963670433416, 'fmeasure': 0.12588832654918686}
Rouge_2: {'precision': 0.022619047619047622, 'recall': 0.006878714505833149, 'fmeasure': 0.009948200908471571}
Rouge_L: {'precision': 0.18862952019731674, 'recall': 0.08934429629083603, 'fmeasure': 0.10829521047713984}

2 0 tensor(4.0950, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.5708, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.4166, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.3789, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(4.2233, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.436029351363748

ROUGE Scores: (val)
Rouge_1: {'precision': 0.232100667693888, 'recall': 0.09590346724048475, 'fmeasure': 0.12062420305731437}
Rouge_2: {'precision': 0.031485965384270474, 'recall': 0.010709331548521297, 'fmeasure': 0.014043080987898011}
Rouge_L: {'precision': 0.21187357839900212, 'recall': 0.09003410103082238, 'fmeasure': 0.11205162150600707}

3 0 tensor(2.2914, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.1912, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.0382, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(4.0458, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.7385, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.3889582490516923

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21622600563278524, 'recall': 0.11144595986688867, 'fmeasure': 0.13348429682934146}
Rouge_2: {'precision': 0.041518274569122035, 'recall': 0.01984758214587285, 'fmeasure': 0.02461261157763971}
Rouge_L: {'precision': 0.18741122342817254, 'recall': 0.09946177453856647, 'fmeasure': 0.11790609652808401}

4 0 tensor(2.5257, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(1.9002, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.4710, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(4.5588, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(1.4571, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 20 3.3640108856104187

ROUGE Scores: (val)
Rouge_1: {'precision': 0.25423123459015773, 'recall': 0.117107705849609, 'fmeasure': 0.1471389414719304}
Rouge_2: {'precision': 0.05530058453787267, 'recall': 0.026783015259293736, 'fmeasure': 0.032939971425227255}
Rouge_L: {'precision': 0.21971231659666351, 'recall': 0.10604367154373069, 'fmeasure': 0.13084661664492142}

5 0 tensor(2.8716, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(0.9003, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(4.2621, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.4251, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.9716, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 20 3.348114929967007

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2575588287774098, 'recall': 0.11823633326170623, 'fmeasure': 0.1435462573379353}
Rouge_2: {'precision': 0.06350781709704939, 'recall': 0.027547463867168588, 'fmeasure': 0.03451206060274852}
Rouge_L: {'precision': 0.2362927125960394, 'recall': 0.11239406243941556, 'fmeasure': 0.13479745481822744}

6 0 tensor(3.3815, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(2.5209, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(4.0364, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.9270, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.6287, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 20 3.329366175805108

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24281851138780344, 'recall': 0.11219369357759779, 'fmeasure': 0.13645154375218935}
Rouge_2: {'precision': 0.05752256218357913, 'recall': 0.02203886887918568, 'fmeasure': 0.02981724714322338}
Rouge_L: {'precision': 0.21949202619691144, 'recall': 0.10573167239060838, 'fmeasure': 0.12677214167680373}

7 0 tensor(2.3113, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(2.9908, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(3.5128, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(2.4205, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(3.7232, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 20 3.3222626740649597

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2551156774985289, 'recall': 0.11826321642674772, 'fmeasure': 0.14460145124382365}
Rouge_2: {'precision': 0.052563773326485194, 'recall': 0.019189958826700774, 'fmeasure': 0.026005744854611913}
Rouge_L: {'precision': 0.22761511779457935, 'recall': 0.10849481927625035, 'fmeasure': 0.13119622623264945}

8 0 tensor(3.6591, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(4.3457, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(3.3538, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(2.6657, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(4.1865, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 20 3.324993013325384

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2826413837630189, 'recall': 0.1276399452264894, 'fmeasure': 0.1599374541524219}
Rouge_2: {'precision': 0.06445691784674835, 'recall': 0.02702611871924551, 'fmeasure': 0.03500279448948492}
Rouge_L: {'precision': 0.24874411342507055, 'recall': 0.11554090467553747, 'fmeasure': 0.1431707643542272}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.15204558815669925, 'recall': 0.09499336433890956, 'fmeasure': 0.11174752842934257}
Rouge_2: {'precision': 0.0074822297044519264, 'recall': 0.0051440329218107, 'fmeasure': 0.005964096141229796}
Rouge_L: {'precision': 0.12412083800972692, 'recall': 0.07955953648678106, 'fmeasure': 0.09221542879388374}

avg_f1_1: 0.16497794783214315

avg_f1_2: 0.03092533965374177

avg_f1_L: 0.14535556395209
