{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_DocT5Query_reading_comp_612_1e-5', 'EPOCH_SETTING': '5', 'NAME': 'Learningq_DocT5Query_reading_comp_612_1e-5_fine7_e4', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 7, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(2.8806, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(2.5948, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(2.7598, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.4567, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(1.7732, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.1053, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.4771, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.9907, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.5727, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.8842, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(2.2246, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.6197, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(1.5780, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.4346, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.1652, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(1.9675, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.0724, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.6251, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.3055, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.2729, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(0.9712, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.7851, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.7195, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.8408, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.0513, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(1.3148, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.6105, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.2304, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.3434, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.9206, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(0.9901, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.7955, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.9635, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.5928, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.2151, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.17003805942485187, 'recall': 0.1185725198001762, 'fmeasure': 0.12673089835910512}
Rouge_2: {'precision': 0.024272822996463397, 'recall': 0.0213435147864729, 'fmeasure': 0.02040753563724481}
Rouge_L: {'precision': 0.14102045752989148, 'recall': 0.10121554140546198, 'fmeasure': 0.10681582040411929}
