{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_DocT5Query_reading_comp_612_1e-5', 'EPOCH_SETTING': '5', 'NAME': 'Learningq_DocT5Query_reading_comp_612_1e-5_fine10', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 10, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 1e-05, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(2.8806, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(2.5265, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.0125, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.5489, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(1.7887, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.9665, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.0260, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.6081, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.9989, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(4.2631, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(3.0572, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.5197, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.4033, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.7435, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.9954, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(3.2321, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.5855, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(3.3413, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.6260, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.1740, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(2.8759, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(3.2475, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.4319, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.1769, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(3.9309, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(2.6551, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.6806, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(2.8939, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.6421, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(3.5074, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(2.5076, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(3.1492, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(3.6467, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(3.1682, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(2.1556, device='cuda:0', grad_fn=<NllLossBackward0>)

8 0 tensor(4.1945, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(3.9338, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(2.8608, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(3.0481, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.9065, device='cuda:0', grad_fn=<NllLossBackward0>)

9 0 tensor(1.5520, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(2.6656, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(1.6704, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(0.7809, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(2.5873, device='cuda:0', grad_fn=<NllLossBackward0>)

10 0 tensor(2.2403, device='cuda:0', grad_fn=<NllLossBackward0>)

10 100 tensor(2.5597, device='cuda:0', grad_fn=<NllLossBackward0>)

10 200 tensor(3.1722, device='cuda:0', grad_fn=<NllLossBackward0>)

10 300 tensor(3.7374, device='cuda:0', grad_fn=<NllLossBackward0>)

10 400 tensor(2.3172, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.19482186037764382, 'recall': 0.164465232168019, 'fmeasure': 0.16280111414863674}
Rouge_2: {'precision': 0.020875977421336383, 'recall': 0.01948307786265611, 'fmeasure': 0.019300663854411086}
Rouge_L: {'precision': 0.15712467848439957, 'recall': 0.13924893010994155, 'fmeasure': 0.1348171283320132}
