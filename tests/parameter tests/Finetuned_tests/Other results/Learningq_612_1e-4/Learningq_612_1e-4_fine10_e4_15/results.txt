{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_612_1e-4', 'EPOCH_SETTING': '15', 'NAME': 'Learningq_612_1e-4_fine10_e4_15', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 10, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(3.5911, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.4713, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.0001, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.6023, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.0370, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.7569, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.8540, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.6426, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.9135, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.8847, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(2.6438, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.9081, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.3396, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.2246, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.4551, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(2.0424, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.0255, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.8589, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.7345, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(1.9261, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(1.3977, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.6739, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.1055, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.5667, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.7056, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(1.5764, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.6543, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.8816, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.3994, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.3655, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(0.9835, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(2.0413, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.7685, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.3493, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(0.9995, device='cuda:0', grad_fn=<NllLossBackward0>)

8 0 tensor(1.7663, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(2.4601, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(1.5009, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(1.4556, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.2958, device='cuda:0', grad_fn=<NllLossBackward0>)

9 0 tensor(0.4743, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(0.6336, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(0.2522, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(0.1708, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(1.3698, device='cuda:0', grad_fn=<NllLossBackward0>)

10 0 tensor(0.4924, device='cuda:0', grad_fn=<NllLossBackward0>)

10 100 tensor(0.4691, device='cuda:0', grad_fn=<NllLossBackward0>)

10 200 tensor(0.9372, device='cuda:0', grad_fn=<NllLossBackward0>)

10 300 tensor(0.7441, device='cuda:0', grad_fn=<NllLossBackward0>)

10 400 tensor(0.6470, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.18252458168745617, 'recall': 0.16056217300963507, 'fmeasure': 0.15101912199215245}
Rouge_2: {'precision': 0.018026967791118738, 'recall': 0.01488534506818285, 'fmeasure': 0.014867377631938745}
Rouge_L: {'precision': 0.14431773516415686, 'recall': 0.13040411072768995, 'fmeasure': 0.12158529266054907}

BLEU Scores: (test)
{'bleu-1': 0.06949280258755758, 'bleu-2': 1.1695904782589897e-308, 'bleu-3': 1.5462661839198e-309, 'bleu-4': 1.1695904782589897e-308, 'bleu': 5.650740759202491e-232, 'bleu-corp-1': 0.06949280258755758, 'bleu-corp-2': 1.1695904782589897e-308, 'bleu-corp-3': 1.5462661839198e-309, 'bleu-corp-4': 1.1695904782589897e-308, 'bleu-corp': 5.650740759202491e-232}

METEOR Scores: (test)
{'meteor': 0.11538778997230227}

GLEU Scores: (test)
{'gleu-1': 0.08177354018812288, 'gleu-2': 0.0, 'gleu-3': 0.0, 'gleu-4': 0.0, 'gleu': 0.022622687998000917, 'gleu-corp-1': 0.08177354018812288, 'gleu-corp-2': 0.0, 'gleu-corp-3': 0.0, 'gleu-corp-4': 0.0, 'gleu-corp': 0.022622687998000917}
