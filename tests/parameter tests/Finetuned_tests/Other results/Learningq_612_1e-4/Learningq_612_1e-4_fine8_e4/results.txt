{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_612_1e-4', 'EPOCH_SETTING': '5', 'NAME': 'Learningq_612_1e-4_fine8_e4', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 8, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(3.2310, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.0176, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(2.8407, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.6098, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(1.9951, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.5830, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.7607, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.2185, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.7126, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.7643, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(2.3633, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.7652, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(1.9367, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.0293, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.5652, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(2.0596, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.7947, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.5087, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.3788, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(1.9450, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(1.3481, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.8824, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.7798, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.4632, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.5188, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(1.4674, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.8461, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.3735, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.3024, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.1042, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(1.2917, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.9501, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.8024, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.7233, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(0.8694, device='cuda:0', grad_fn=<NllLossBackward0>)

8 0 tensor(1.7528, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(2.5554, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(1.6374, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(1.6075, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.3395, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.14235209924137432, 'recall': 0.11342917344505364, 'fmeasure': 0.11504441945590918}
Rouge_2: {'precision': 0.017323577889615624, 'recall': 0.013118140337896165, 'fmeasure': 0.012519843137649278}
Rouge_L: {'precision': 0.11520555605709527, 'recall': 0.09298781363316684, 'fmeasure': 0.09417356030707687}

BLEU Scores: (test)
{'bleu-1': 0.06888636923306549, 'bleu-2': 0.004056289313663382, 'bleu-3': 1.532772593879737e-309, 'bleu-4': 1.093153076943157e-308, 'bleu': 2.6783961554078494e-156, 'bleu-corp-1': 0.06888636923306549, 'bleu-corp-2': 0.004056289313663382, 'bleu-corp-3': 1.532772593879737e-309, 'bleu-corp-4': 1.093153076943157e-308, 'bleu-corp': 2.6783961554078494e-156}

METEOR Scores: (test)
{'meteor': 0.10187083624235507}

GLEU Scores: (test)
{'gleu-1': 0.07704559025316632, 'gleu-2': 0.004552870979064098, 'gleu-3': 0.0, 'gleu-4': 0.0, 'gleu': 0.022824147087806636, 'gleu-corp-1': 0.07704559025316632, 'gleu-corp-2': 0.004552870979064098, 'gleu-corp-3': 0.0, 'gleu-corp-4': 0.0, 'gleu-corp': 0.022824147087806636}
