{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_612_1e-4', 'EPOCH_SETTING': '10', 'NAME': 'Learningq_612_1e-4_fine7_10', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 7, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 1e-05, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(3.5107, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.7172, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.3980, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.8592, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.5383, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(4.0574, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.9037, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(5.1474, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.3093, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(4.4503, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(3.3242, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.8266, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(4.4390, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(4.2850, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.9909, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(3.2448, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(4.4270, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(4.1486, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(4.2922, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.3194, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(2.8828, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(3.6815, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.7196, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.2301, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(4.8257, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(2.9577, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.7542, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(2.8932, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(2.2506, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(4.0472, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(2.7669, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(3.6321, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(4.1284, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(3.2348, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(2.4963, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.14295786107601482, 'recall': 0.10178713574795714, 'fmeasure': 0.10676782596992454}
Rouge_2: {'precision': 0.017817612157234798, 'recall': 0.014332840054260698, 'fmeasure': 0.014647223845337053}
Rouge_L: {'precision': 0.1325538088343908, 'recall': 0.0956134710589002, 'fmeasure': 0.09961113554255745}

BLEU Scores: (test)
{'bleu-1': 0.05481353197117239, 'bleu-2': 0.00531807343427798, 'bleu-3': 1.21964157081508e-309, 'bleu-4': 9.36102321128571e-309, 'bleu': 3.114659593256741e-156, 'bleu-corp-1': 0.05481353197117239, 'bleu-corp-2': 0.00531807343427798, 'bleu-corp-3': 1.21964157081508e-309, 'bleu-corp-4': 9.36102321128571e-309, 'bleu-corp': 3.114659593256741e-156}

METEOR Scores: (test)
{'meteor': 0.09925551071416218}

GLEU Scores: (test)
{'gleu-1': 0.06549127650332612, 'gleu-2': 0.005888204338339109, 'gleu-3': 0.0, 'gleu-4': 0.0, 'gleu': 0.019722303492422315, 'gleu-corp-1': 0.06549127650332612, 'gleu-corp-2': 0.005888204338339109, 'gleu-corp-3': 0.0, 'gleu-corp-4': 0.0, 'gleu-corp': 0.019722303492422315}
