{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_612_1e-4', 'EPOCH_SETTING': '10', 'NAME': 'Learningq_612_1e-4_fine7_e4_10', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 7, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(3.5107, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.3518, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(2.9087, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.5203, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.0277, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.7485, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.9341, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.4838, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.9280, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.6815, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(2.5162, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.1239, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.0748, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.0719, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.4814, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(1.8762, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.1368, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.8329, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.6355, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(1.8744, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(1.0955, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.0481, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.8406, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.6224, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.7341, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(1.3765, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.8083, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.2327, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.2340, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.8203, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(1.0185, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(2.1249, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(2.0580, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.6134, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(0.5812, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.1413672881369593, 'recall': 0.10686742415855408, 'fmeasure': 0.11199400672391122}
Rouge_2: {'precision': 0.010921170851657445, 'recall': 0.010849056603773584, 'fmeasure': 0.010462110057185905}
Rouge_L: {'precision': 0.10933002277560959, 'recall': 0.08424454777835827, 'fmeasure': 0.08734909584115692}

BLEU Scores: (test)
{'bleu-1': 0.05832739537507828, 'bleu-2': 0.004192872117400419, 'bleu-3': 1.297827626839034e-309, 'bleu-4': 8.293804909407504e-309, 'bleu': 1.5367676702975962e-156, 'bleu-corp-1': 0.05832739537507828, 'bleu-corp-2': 0.004192872117400419, 'bleu-corp-3': 1.297827626839034e-309, 'bleu-corp-4': 8.293804909407504e-309, 'bleu-corp': 1.5367676702975962e-156}

METEOR Scores: (test)
{'meteor': 0.09764784438184299}

GLEU Scores: (test)
{'gleu-1': 0.06837076973911226, 'gleu-2': 0.004192872117400419, 'gleu-3': 0.0, 'gleu-4': 0.0, 'gleu': 0.020104549312547466, 'gleu-corp-1': 0.06837076973911226, 'gleu-corp-2': 0.004192872117400419, 'gleu-corp-3': 0.0, 'gleu-corp-4': 0.0, 'gleu-corp': 0.020104549312547466}
