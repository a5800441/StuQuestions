{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_612_1e-4', 'EPOCH_SETTING': '5', 'NAME': 'Learningq_612_1e-4_fine10_e4', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 10, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(3.2310, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.0176, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(2.8407, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.6098, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(1.9951, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.5830, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.7607, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.2185, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.7126, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.7643, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(2.3633, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.7652, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(1.9367, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.0293, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.5652, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(2.0596, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.7947, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.5087, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.3788, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(1.9450, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(1.3481, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.8824, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.7798, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.4632, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.5188, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(1.4674, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.8461, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.3735, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.3024, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.1042, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(1.2917, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.9501, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.8024, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.7233, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(0.8694, device='cuda:0', grad_fn=<NllLossBackward0>)

8 0 tensor(1.7528, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(2.5554, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(1.6374, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(1.6075, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.3395, device='cuda:0', grad_fn=<NllLossBackward0>)

9 0 tensor(0.2746, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(0.5836, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(0.3123, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(0.1950, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(0.8248, device='cuda:0', grad_fn=<NllLossBackward0>)

10 0 tensor(0.4115, device='cuda:0', grad_fn=<NllLossBackward0>)

10 100 tensor(0.5728, device='cuda:0', grad_fn=<NllLossBackward0>)

10 200 tensor(1.0876, device='cuda:0', grad_fn=<NllLossBackward0>)

10 300 tensor(0.7208, device='cuda:0', grad_fn=<NllLossBackward0>)

10 400 tensor(0.3671, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.14816950565709255, 'recall': 0.12108965279594153, 'fmeasure': 0.12274456711145915}
Rouge_2: {'precision': 0.025825641863377713, 'recall': 0.018146828410278378, 'fmeasure': 0.019208859672937256}
Rouge_L: {'precision': 0.1257473716287023, 'recall': 0.10188936287346553, 'fmeasure': 0.10311498932520662}

BLEU Scores: (test)
{'bleu-1': 0.07278277718838744, 'bleu-2': 0.004093963716841595, 'bleu-3': 1.6194705487144e-309, 'bleu-4': 1.0928860158283857e-308, 'bleu': 2.2877121449547682e-156, 'bleu-corp-1': 0.07278277718838744, 'bleu-corp-2': 0.004093963716841595, 'bleu-corp-3': 1.6194705487144e-309, 'bleu-corp-4': 1.0928860158283857e-308, 'bleu-corp': 2.2877121449547682e-156}

METEOR Scores: (test)
{'meteor': 0.1036846811796061}

GLEU Scores: (test)
{'gleu-1': 0.08008705240235262, 'gleu-2': 0.004073756432246998, 'gleu-3': 0.0, 'gleu-4': 0.0, 'gleu': 0.023658261379923103, 'gleu-corp-1': 0.08008705240235262, 'gleu-corp-2': 0.004073756432246998, 'gleu-corp-3': 0.0, 'gleu-corp-4': 0.0, 'gleu-corp': 0.023658261379923103}
