{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_512_1e-5', 'EPOCH_SETTING': '15', 'NAME': 'IR_small_512_1e-5_fine10_15_e5', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 10, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 1e-05, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(2.5166, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.1610, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.7292, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.1588, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.8553, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.5873699673151567

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23022955083024874, 'recall': 0.11617890939358462, 'fmeasure': 0.13561963757410261}
Rouge_2: {'precision': 0.04061407048695184, 'recall': 0.01317013934043281, 'fmeasure': 0.016481641549851555}
Rouge_L: {'precision': 0.21034350467301613, 'recall': 0.10348283868091154, 'fmeasure': 0.12144622645527986}

2 0 tensor(4.0162, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.5914, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.9616, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.9946, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.6763, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.555402921418012

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2012254706835438, 'recall': 0.13926798031927723, 'fmeasure': 0.1537100129576228}
Rouge_2: {'precision': 0.034318019653318106, 'recall': 0.023209934955738733, 'fmeasure': 0.026298760720416405}
Rouge_L: {'precision': 0.17514246086949029, 'recall': 0.12163932108294101, 'fmeasure': 0.13356144784905036}

3 0 tensor(3.2573, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.3427, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.9863, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.7621, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(5.8787, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.530859102637081

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1866318240433552, 'recall': 0.1648064169069346, 'fmeasure': 0.1606798307020395}
Rouge_2: {'precision': 0.01581542280694823, 'recall': 0.014601585741968941, 'fmeasure': 0.013553349109475648}
Rouge_L: {'precision': 0.15505289825141627, 'recall': 0.13725841172381412, 'fmeasure': 0.13316737959512095}

4 0 tensor(3.6626, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.7685, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(4.5765, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.5095, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.8035, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.5131160061238176

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19786903583878218, 'recall': 0.2018495950094347, 'fmeasure': 0.1870057530786789}
Rouge_2: {'precision': 0.02539698817116564, 'recall': 0.025459728924962517, 'fmeasure': 0.023627544251495478}
Rouge_L: {'precision': 0.15937719477019768, 'recall': 0.16379523501025692, 'fmeasure': 0.15148708691395057}

5 0 tensor(3.2757, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.8977, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.6462, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.4700, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(2.6258, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 3.519009386078786

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1745860172460634, 'recall': 0.1829011533150917, 'fmeasure': 0.16544618707401024}
Rouge_2: {'precision': 0.021918921540166503, 'recall': 0.026111289344587164, 'fmeasure': 0.020963788715026573}
Rouge_L: {'precision': 0.14124085440950637, 'recall': 0.14563822189398265, 'fmeasure': 0.13262931956361615}

6 0 tensor(1.1348, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.4791, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(3.2004, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.2021, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.2217, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 0 3.529680235911224

ROUGE Scores: (val)
Rouge_1: {'precision': 0.17111661790924915, 'recall': 0.16882454875060293, 'fmeasure': 0.1572194149835591}
Rouge_2: {'precision': 0.02008337383029787, 'recall': 0.01779059246456825, 'fmeasure': 0.017857959200826237}
Rouge_L: {'precision': 0.13782739797535798, 'recall': 0.13779386535321533, 'fmeasure': 0.12736036583211233}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.16982331482291546, 'recall': 0.15504326047844696, 'fmeasure': 0.1525525693054748}
Rouge_2: {'precision': 0.014893692580320965, 'recall': 0.011594762587811247, 'fmeasure': 0.012450542173455413}
Rouge_L: {'precision': 0.13870615518911097, 'recall': 0.12600655077279907, 'fmeasure': 0.12385583621569665}

1 0 tensor(3.0765, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.7081, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.5268, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(2.7999, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.4082, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.460089994639885

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2153771249979345, 'recall': 0.1736913957504175, 'fmeasure': 0.16912457740662684}
Rouge_2: {'precision': 0.04069178908131562, 'recall': 0.03228192918055503, 'fmeasure': 0.03186291893769193}
Rouge_L: {'precision': 0.17968487307868278, 'recall': 0.14334359352458986, 'fmeasure': 0.13936359219532554}

2 0 tensor(1.4985, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.0796, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.0099, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.8997, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(1.2663, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.422590527592636

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2206091953345218, 'recall': 0.1421792299321247, 'fmeasure': 0.15810163582744713}
Rouge_2: {'precision': 0.03323844177502714, 'recall': 0.018742755793921456, 'fmeasure': 0.021660743920200756}
Rouge_L: {'precision': 0.18150673705211945, 'recall': 0.11885480655594662, 'fmeasure': 0.13122276060087884}

3 0 tensor(2.4604, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(1.9264, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.5725, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.5086, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.9915, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.3971369150208264

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2367377781576974, 'recall': 0.15253884525016556, 'fmeasure': 0.16387989667279276}
Rouge_2: {'precision': 0.04860994357945578, 'recall': 0.027289602588989628, 'fmeasure': 0.030000723784115568}
Rouge_L: {'precision': 0.20278827327895235, 'recall': 0.13255944497298028, 'fmeasure': 0.14124801206289567}

4 0 tensor(3.6686, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.0122, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.8559, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.7380, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.4345, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 10 3.3793069589428786

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23919842512023276, 'recall': 0.14371675019769567, 'fmeasure': 0.1630718730916611}
Rouge_2: {'precision': 0.042794366879732745, 'recall': 0.024402770831532212, 'fmeasure': 0.028134349589874492}
Rouge_L: {'precision': 0.19655380686585838, 'recall': 0.124061648369143, 'fmeasure': 0.1385170386729645}

5 0 tensor(4.5521, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.7007, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.6417, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.0507, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(3.0992, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 10 3.3780473078169475

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20319946403178638, 'recall': 0.1367556290752305, 'fmeasure': 0.1492557822264136}
Rouge_2: {'precision': 0.02357230268317648, 'recall': 0.015038367914358187, 'fmeasure': 0.01685166842523282}
Rouge_L: {'precision': 0.15931146795772974, 'recall': 0.10942314106998463, 'fmeasure': 0.11834619860654874}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.20922690708645034, 'recall': 0.14460386661652053, 'fmeasure': 0.15271835695022898}
Rouge_2: {'precision': 0.044045197254023755, 'recall': 0.016300971281564255, 'fmeasure': 0.019509620437575675}
Rouge_L: {'precision': 0.1780867971055623, 'recall': 0.11723447050567436, 'fmeasure': 0.12508839339843586}

1 0 tensor(4.6107, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(1.8662, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.0847, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(5.1168, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.5174, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.3951196771557046

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2098783025496306, 'recall': 0.1284585976279316, 'fmeasure': 0.14064511381604805}
Rouge_2: {'precision': 0.036184060760331954, 'recall': 0.013596825025025775, 'fmeasure': 0.01813404866496288}
Rouge_L: {'precision': 0.17108715194654736, 'recall': 0.1049896746049759, 'fmeasure': 0.11426815491698523}

2 0 tensor(3.8442, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.4637, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.4724, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.7538, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.6765, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.3620764825303677

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23765311398530164, 'recall': 0.13278604735295257, 'fmeasure': 0.15063506838057214}
Rouge_2: {'precision': 0.0428947464964414, 'recall': 0.017672348374434783, 'fmeasure': 0.02362996909430409}
Rouge_L: {'precision': 0.20175457526562107, 'recall': 0.11548244864984254, 'fmeasure': 0.12971184021469606}

3 0 tensor(2.3530, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.5527, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.9369, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.9698, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.5389, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.3223524730084306

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21810120025211582, 'recall': 0.1307960831625304, 'fmeasure': 0.1463584935846585}
Rouge_2: {'precision': 0.04050321839454441, 'recall': 0.01938992931356801, 'fmeasure': 0.024302291382626195}
Rouge_L: {'precision': 0.18444105154959425, 'recall': 0.116809502691586, 'fmeasure': 0.12746283550536652}

4 0 tensor(2.5817, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(1.9890, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.2939, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.9282, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(1.3997, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 20 3.316266043711517

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24248176493939205, 'recall': 0.11346968063102049, 'fmeasure': 0.14160185989040502}
Rouge_2: {'precision': 0.036231141739616314, 'recall': 0.011650631057242476, 'fmeasure': 0.017079654589329765}
Rouge_L: {'precision': 0.20476566747753186, 'recall': 0.10039238732232474, 'fmeasure': 0.12299150401412368}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.16256691746887827, 'recall': 0.13494346699111062, 'fmeasure': 0.13532367074216814}
Rouge_2: {'precision': 0.02855940355940356, 'recall': 0.02446311858076564, 'fmeasure': 0.024376128434099448}
Rouge_L: {'precision': 0.12895659774744744, 'recall': 0.10889078121620263, 'fmeasure': 0.10833952022901693}

avg_f1_1: 0.1689217996219593

avg_f1_2: 0.027487990346911507

avg_f1_L: 0.14081564639718078
