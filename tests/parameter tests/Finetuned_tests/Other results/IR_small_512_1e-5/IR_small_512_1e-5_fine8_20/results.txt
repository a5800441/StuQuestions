{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_512_1e-5', 'EPOCH_SETTING': '20', 'NAME': 'IR_small_512_1e-5_fine8_20', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 8, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(2.5945, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.9458, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.2779, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.5663, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.8092, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.4587498579995106

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18185501383866812, 'recall': 0.12776542330897286, 'fmeasure': 0.1391343899003765}
Rouge_2: {'precision': 0.020578212549666606, 'recall': 0.01311876047785596, 'fmeasure': 0.014770906925952825}
Rouge_L: {'precision': 0.1583225051143664, 'recall': 0.11077634346110095, 'fmeasure': 0.11997044956802652}

2 0 tensor(2.6286, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.9959, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.9255, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.2374, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.4102, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.516471816321551

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18038446710391406, 'recall': 0.12219881252164264, 'fmeasure': 0.13574761299892488}
Rouge_2: {'precision': 0.025105120867832734, 'recall': 0.010798286554017074, 'fmeasure': 0.013581029381737414}
Rouge_L: {'precision': 0.14801377134115762, 'recall': 0.09614994896465133, 'fmeasure': 0.10882400627510601}

3 0 tensor(2.1043, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.7406, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.8083, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.2186, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.5918, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.632676074060343

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1755279098983398, 'recall': 0.13371367861501013, 'fmeasure': 0.13764047793477802}
Rouge_2: {'precision': 0.011366270264575349, 'recall': 0.008897401996907826, 'fmeasure': 0.00919332228877278}
Rouge_L: {'precision': 0.15439510359604197, 'recall': 0.11773560108159857, 'fmeasure': 0.12059828886914634}

4 0 tensor(2.0610, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.1243, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.3213, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(1.8783, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.6492, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.7733862662719466

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21967099565934647, 'recall': 0.17572730634188707, 'fmeasure': 0.18300176764333256}
Rouge_2: {'precision': 0.030536600875583932, 'recall': 0.022477363171584854, 'fmeasure': 0.024841360958011007}
Rouge_L: {'precision': 0.19057641302800563, 'recall': 0.1551202188692777, 'fmeasure': 0.1599105353016619}

5 0 tensor(2.3258, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.0678, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.8856, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.0276, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.6957, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 3.9540038957434187

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19234422938311263, 'recall': 0.15253675607133252, 'fmeasure': 0.15843502778600824}
Rouge_2: {'precision': 0.028633144024470047, 'recall': 0.023424894744608377, 'fmeasure': 0.023942390001712034}
Rouge_L: {'precision': 0.1662788026197797, 'recall': 0.13095395675732469, 'fmeasure': 0.13622289391517772}

6 0 tensor(0.2819, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.4878, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(1.0532, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.5051, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.4163, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 0 4.248381232811233

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18722432509098827, 'recall': 0.15231191986366105, 'fmeasure': 0.1525234571707165}
Rouge_2: {'precision': 0.019119823070471127, 'recall': 0.01702073386465861, 'fmeasure': 0.017052652459845514}
Rouge_L: {'precision': 0.15936101863189017, 'recall': 0.12153736734185035, 'fmeasure': 0.12478451854671897}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.15472267101436446, 'recall': 0.11870247558045045, 'fmeasure': 0.12566106733021748}
Rouge_2: {'precision': 0.007787372326772993, 'recall': 0.007759896411394747, 'fmeasure': 0.007158619532034854}
Rouge_L: {'precision': 0.1283644620594793, 'recall': 0.09714975466203936, 'fmeasure': 0.10289820642980105}

1 0 tensor(3.1410, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.7335, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.3468, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(2.6288, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.6332, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.3988945484161377

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2023185478043705, 'recall': 0.1630368777309016, 'fmeasure': 0.163806223728379}
Rouge_2: {'precision': 0.031127465576119447, 'recall': 0.030661717786929457, 'fmeasure': 0.02773941950044305}
Rouge_L: {'precision': 0.1621689582340492, 'recall': 0.13564108759837032, 'fmeasure': 0.13419898790481152}

2 0 tensor(1.1356, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.5288, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.0776, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.7100, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(4.1422, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.821224176302189

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2060362391439679, 'recall': 0.12856097473074884, 'fmeasure': 0.1436050913533173}
Rouge_2: {'precision': 0.007520325203252033, 'recall': 0.003897051458027067, 'fmeasure': 0.00472789512584121}
Rouge_L: {'precision': 0.1634865707400951, 'recall': 0.10667556662654117, 'fmeasure': 0.11712542950169483}

3 0 tensor(2.9511, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(1.7876, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.5476, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.4936, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.0680, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.4609207554561334

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22163399904329065, 'recall': 0.139491801332766, 'fmeasure': 0.15149715464637753}
Rouge_2: {'precision': 0.018707642423815184, 'recall': 0.011076730657208116, 'fmeasure': 0.01138435268237931}
Rouge_L: {'precision': 0.17847767775795784, 'recall': 0.11130566461684317, 'fmeasure': 0.12106903153882338}

4 0 tensor(1.8900, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(1.3162, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.3512, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.5534, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.3432, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 10 3.6265536706622052

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2122855539700579, 'recall': 0.1543643380704066, 'fmeasure': 0.16133454713420967}
Rouge_2: {'precision': 0.025242546987525465, 'recall': 0.01732031844170661, 'fmeasure': 0.019071826730126105}
Rouge_L: {'precision': 0.16962492896959727, 'recall': 0.12632684199322852, 'fmeasure': 0.1304035488410404}

5 0 tensor(2.7561, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.1265, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.7945, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.0922, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.1687, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 10 3.8720790714752384

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1732633668724433, 'recall': 0.1347615416741385, 'fmeasure': 0.13442848116522418}
Rouge_2: {'precision': 0.013737303864635285, 'recall': 0.007771808972763375, 'fmeasure': 0.008789524056721728}
Rouge_L: {'precision': 0.14146124031270896, 'recall': 0.11334144786478706, 'fmeasure': 0.11106348383045561}

6 0 tensor(0.4310, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.8343, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(1.1305, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.2534, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.9622, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 10 4.08621608338705

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20477421849383257, 'recall': 0.17527952029632304, 'fmeasure': 0.1704973183389895}
Rouge_2: {'precision': 0.022315138120842692, 'recall': 0.015257522554617986, 'fmeasure': 0.016805145804902322}
Rouge_L: {'precision': 0.1601363024052966, 'recall': 0.1432464147731154, 'fmeasure': 0.13621916738575887}

7 0 tensor(0.5108, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.6196, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(0.7518, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.3841, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.6235, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 10 4.331180161092339

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19315453779651848, 'recall': 0.13143159484937902, 'fmeasure': 0.14454206244602028}
Rouge_2: {'precision': 0.0258692277626809, 'recall': 0.015073037798382441, 'fmeasure': 0.017737883473918897}
Rouge_L: {'precision': 0.1635249777008631, 'recall': 0.11136899365544561, 'fmeasure': 0.122197116638331}

8 0 tensor(0.5246, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(0.7753, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(0.1047, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(0.1511, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(1.8155, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 10 4.503632088986839

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18098689762351905, 'recall': 0.1477997250131762, 'fmeasure': 0.14732125622893316}
Rouge_2: {'precision': 0.014521820649586263, 'recall': 0.007729911276399331, 'fmeasure': 0.009614977958668728}
Rouge_L: {'precision': 0.13505367832614368, 'recall': 0.116308185244556, 'fmeasure': 0.1136336967323087}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.18854626824773824, 'recall': 0.20167211722786282, 'fmeasure': 0.17507366726596976}
Rouge_2: {'precision': 0.024211003810123536, 'recall': 0.027254407765024545, 'fmeasure': 0.02365281808388126}
Rouge_L: {'precision': 0.14040627902026034, 'recall': 0.1544305432335528, 'fmeasure': 0.13188993347535907}

1 0 tensor(4.4424, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(1.7699, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.2508, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(4.9959, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.3635, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.46540018663568

ROUGE Scores: (val)
Rouge_1: {'precision': 0.16210954711397316, 'recall': 0.09826088389762865, 'fmeasure': 0.11080176083371325}
Rouge_2: {'precision': 0.012538682653338686, 'recall': 0.007120849042452652, 'fmeasure': 0.007421471279410763}
Rouge_L: {'precision': 0.13697143176571025, 'recall': 0.08400653298988758, 'fmeasure': 0.09363646445600757}

2 0 tensor(3.5215, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.9957, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.9626, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.2165, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.4820, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.259000938827709

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24792447896288544, 'recall': 0.13356221478472816, 'fmeasure': 0.154278485932739}
Rouge_2: {'precision': 0.05189626757423368, 'recall': 0.02485191484403482, 'fmeasure': 0.032124560678342205}
Rouge_L: {'precision': 0.20772940448370675, 'recall': 0.11789922882381892, 'fmeasure': 0.1339587322763946}

3 0 tensor(1.6871, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.0589, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.1143, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.4973, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.1471, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.3123274778915666

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2372434010690294, 'recall': 0.11231772513250854, 'fmeasure': 0.12820703602730385}
Rouge_2: {'precision': 0.028531073446327684, 'recall': 0.005014186045362761, 'fmeasure': 0.007916732178233389}
Rouge_L: {'precision': 0.1936190491596641, 'recall': 0.09212352081266384, 'fmeasure': 0.104850321977757}

4 0 tensor(1.6015, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(0.5053, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.7806, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.8162, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(0.4861, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 20 3.5250306735604497

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2373947292205675, 'recall': 0.14906096699891347, 'fmeasure': 0.1680673141849449}
Rouge_2: {'precision': 0.042583217159488346, 'recall': 0.02928319742019343, 'fmeasure': 0.033688340766951284}
Rouge_L: {'precision': 0.20978409872648213, 'recall': 0.13415519278606378, 'fmeasure': 0.15024779516701334}

5 0 tensor(1.7373, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(0.2749, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.7391, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.0667, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.2557, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 20 3.679142495838262

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21391207795136744, 'recall': 0.14500478551272897, 'fmeasure': 0.15740582155526137}
Rouge_2: {'precision': 0.03392066004229514, 'recall': 0.02585307430762415, 'fmeasure': 0.026847927549407506}
Rouge_L: {'precision': 0.17615309453633154, 'recall': 0.12212034140187945, 'fmeasure': 0.1310769502787035}

6 0 tensor(2.0752, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.9485, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(1.8399, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.8569, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.8394, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 20 3.892979333713903

ROUGE Scores: (val)
Rouge_1: {'precision': 0.17403492404002885, 'recall': 0.12108099581795985, 'fmeasure': 0.12647214969416673}
Rouge_2: {'precision': 0.02050881986076403, 'recall': 0.01916706481923873, 'fmeasure': 0.01680098022930922}
Rouge_L: {'precision': 0.14239573237630548, 'recall': 0.10244807718652821, 'fmeasure': 0.10565034417209974}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.11697777531110869, 'recall': 0.0929930729767331, 'fmeasure': 0.09355536438110817}
Rouge_2: {'precision': 0.006172839506172839, 'recall': 0.009259259259259259, 'fmeasure': 0.007201646090534979}
Rouge_L: {'precision': 0.10399502760613874, 'recall': 0.08154618652984666, 'fmeasure': 0.08253152002393048}

avg_f1_1: 0.17385546672242233

avg_f1_2: 0.028756373741801778

avg_f1_L: 0.1487924992848114
