{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_1024_1e-4', 'EPOCH_SETTING': '25', 'NAME': 'IR_small_1024_1e-4_finetest8_25', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 8, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(6.2038, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.5551, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.6930, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.9992, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.4021, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.4077, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.0662, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.3074, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.0686, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.9348, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(2.6463, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.2574, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(1.8411, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.4087, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.9566, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(1.8289, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.0873, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.8474, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.5194, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.3747, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(1.0707, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.6649, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.8000, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.2331, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(2.5462, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(1.9377, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.2102, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.6549, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.6067, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.8058, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(1.0082, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(2.1990, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.5367, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(1.1914, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.5128, device='cuda:0', grad_fn=<NllLossBackward0>)

8 0 tensor(1.7851, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(2.9705, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(1.3760, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(1.5170, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.2141, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.16787574426731403, 'recall': 0.1198320277251204, 'fmeasure': 0.12403380004408168}
Rouge_2: {'precision': 0.011425305487458652, 'recall': 0.007419550077561644, 'fmeasure': 0.006978774764290379}
Rouge_L: {'precision': 0.1348216221038689, 'recall': 0.10075865775552403, 'fmeasure': 0.10151424155978261}

BLEU Scores: (test)
{'bleu-1': 0.06836828510292799, 'bleu-2': 0.0016145592109759184, 'bleu-3': 1.52124483933498e-309, 'bleu-4': 1.268056332336371e-308, 'bleu': 1.567790836028433e-156, 'bleu-corp-1': 0.06836828510292799, 'bleu-corp-2': 0.0016145592109759184, 'bleu-corp-3': 1.52124483933498e-309, 'bleu-corp-4': 1.268056332336371e-308, 'bleu-corp': 1.567790836028433e-156}

METEOR Scores: (test)
{'meteor': 0.09687304082877113}

GLEU Scores: (test)
{'gleu-1': 0.07920704978254706, 'gleu-2': 0.0016199733180865257, 'gleu-3': 0.0, 'gleu-4': 0.0, 'gleu': 0.022556312420304522, 'gleu-corp-1': 0.07920704978254706, 'gleu-corp-2': 0.0016199733180865257, 'gleu-corp-3': 0.0, 'gleu-corp-4': 0.0, 'gleu-corp': 0.022556312420304522}
