{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_1024_1e-4', 'EPOCH_SETTING': '20', 'NAME': 'IR_small_1024_1e-4_fine10_20', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 10, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(5.6812, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.7787, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.5637, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.2312, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(5.0183, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.606827699531943

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20019514107250894, 'recall': 0.1380667150753488, 'fmeasure': 0.1472590425816631}
Rouge_2: {'precision': 0.031609036716084, 'recall': 0.011188875529526344, 'fmeasure': 0.013978043154235507}
Rouge_L: {'precision': 0.16049075412235927, 'recall': 0.110404674003537, 'fmeasure': 0.11650440789530528}

2 0 tensor(2.8426, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.7727, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.7672, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.4783, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.6461, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.5673944141905185

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20425425163168454, 'recall': 0.12219142599205217, 'fmeasure': 0.13568216166107724}
Rouge_2: {'precision': 0.036251625022811464, 'recall': 0.013933450879306676, 'fmeasure': 0.016064254618738675}
Rouge_L: {'precision': 0.1759410723800164, 'recall': 0.10487091176488944, 'fmeasure': 0.1162824048245802}

3 0 tensor(2.3640, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.8212, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.1918, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.3116, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.3361, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.6104232901233737

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21335546669002822, 'recall': 0.1484026601580405, 'fmeasure': 0.16219969474170517}
Rouge_2: {'precision': 0.023005164129160555, 'recall': 0.01625097919220985, 'fmeasure': 0.01729787909000272}
Rouge_L: {'precision': 0.1720815498265756, 'recall': 0.11926089754071768, 'fmeasure': 0.13067293335770758}

4 0 tensor(2.1279, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.5672, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.9987, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(1.8136, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.5092, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.847338462280015

ROUGE Scores: (val)
Rouge_1: {'precision': 0.210130053562257, 'recall': 0.12617605109818117, 'fmeasure': 0.14433334268278547}
Rouge_2: {'precision': 0.02927885046529114, 'recall': 0.02011221010797832, 'fmeasure': 0.020963589738354795}
Rouge_L: {'precision': 0.17872513550479652, 'recall': 0.10920718616334568, 'fmeasure': 0.12426902471449569}

5 0 tensor(2.6599, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.4351, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(0.9416, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.2474, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.6895, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 4.145633248959557

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1901529592974994, 'recall': 0.09954567721574194, 'fmeasure': 0.12328320434151234}
Rouge_2: {'precision': 0.013921483413008837, 'recall': 0.005436950019506697, 'fmeasure': 0.0072712062350044964}
Rouge_L: {'precision': 0.15092622732663133, 'recall': 0.0773054862921989, 'fmeasure': 0.09681296183332072}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.18659876780021056, 'recall': 0.1296800627538908, 'fmeasure': 0.1391335180715957}
Rouge_2: {'precision': 0.02234378027940736, 'recall': 0.010949474129274352, 'fmeasure': 0.01354391246847904}
Rouge_L: {'precision': 0.15952335369649467, 'recall': 0.110537547895268, 'fmeasure': 0.11788629750844512}

1 0 tensor(6.3717, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(5.1165, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.7994, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.1411, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.5564, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.5121685164730723

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22412971529069367, 'recall': 0.14819654793182138, 'fmeasure': 0.154698602840129}
Rouge_2: {'precision': 0.043938305248997694, 'recall': 0.021755415030436615, 'fmeasure': 0.02402470503556546}
Rouge_L: {'precision': 0.18223326403769963, 'recall': 0.1199795959740065, 'fmeasure': 0.12328136037666806}

2 0 tensor(1.6720, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.7379, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.5437, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.4372, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(1.3526, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.501910472788462

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21920513330125957, 'recall': 0.11321508545885403, 'fmeasure': 0.13164912775245202}
Rouge_2: {'precision': 0.05233260655417643, 'recall': 0.017702037508622936, 'fmeasure': 0.022308437263650366}
Rouge_L: {'precision': 0.19430421125184394, 'recall': 0.09571569639877466, 'fmeasure': 0.11209420194254079}

3 0 tensor(1.8500, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(1.0249, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.5485, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.5645, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.5559, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.5366494204939865

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22073883158088023, 'recall': 0.14219061811539518, 'fmeasure': 0.14718590438149354}
Rouge_2: {'precision': 0.02502095439895045, 'recall': 0.014240293985903806, 'fmeasure': 0.014921057149577777}
Rouge_L: {'precision': 0.18992510050002687, 'recall': 0.125166108136058, 'fmeasure': 0.12820650150769178}

4 0 tensor(2.1812, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(1.7105, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.5227, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.1412, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.5257, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 10 3.86237135020698

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19899678374391427, 'recall': 0.09888196450889661, 'fmeasure': 0.11494415764411565}
Rouge_2: {'precision': 0.02546405650781548, 'recall': 0.008037420471905959, 'fmeasure': 0.011019215904067571}
Rouge_L: {'precision': 0.1640739829674549, 'recall': 0.07981229382643903, 'fmeasure': 0.092859112584338}

5 0 tensor(2.8366, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.0608, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.3694, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.2580, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.6494, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 10 4.0958118002589154

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1973407037993859, 'recall': 0.12080686458188725, 'fmeasure': 0.13064596438503062}
Rouge_2: {'precision': 0.037520857311029485, 'recall': 0.018498008754636856, 'fmeasure': 0.02095429645020462}
Rouge_L: {'precision': 0.16707656434406998, 'recall': 0.10162480451000512, 'fmeasure': 0.110014378249123}

6 0 tensor(0.8128, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.9073, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.9984, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.1759, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.1259, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 10 4.473212076396477

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1870851763318292, 'recall': 0.13502325920248376, 'fmeasure': 0.13822480220752661}
Rouge_2: {'precision': 0.025624606280896398, 'recall': 0.021074022439969666, 'fmeasure': 0.020517058460510006}
Rouge_L: {'precision': 0.14973107673343888, 'recall': 0.11040493686374556, 'fmeasure': 0.11152803755207627}

7 0 tensor(0.4419, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.2792, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(0.5973, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.0818, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.1455, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 10 4.897850199443538

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18088103484833998, 'recall': 0.12294187072376113, 'fmeasure': 0.13067044183305754}
Rouge_2: {'precision': 0.028787432614454437, 'recall': 0.019116750498604403, 'fmeasure': 0.01949693886334602}
Rouge_L: {'precision': 0.1538216914453827, 'recall': 0.10379835369078484, 'fmeasure': 0.10942316275476179}

8 0 tensor(0.1582, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(0.6239, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(0.0943, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(0.1962, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(2.1841, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 10 4.8394926815498165

ROUGE Scores: (val)
Rouge_1: {'precision': 0.15948596213440813, 'recall': 0.0993628121510215, 'fmeasure': 0.10948465643295822}
Rouge_2: {'precision': 0.01614602698684855, 'recall': 0.009386395850680775, 'fmeasure': 0.010745130547182133}
Rouge_L: {'precision': 0.1293011688729976, 'recall': 0.0830885573351055, 'fmeasure': 0.08997586237474028}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.13954259981079184, 'recall': 0.10056920121386609, 'fmeasure': 0.107369475738317}
Rouge_2: {'precision': 0.02177925433739387, 'recall': 0.015085915376613052, 'fmeasure': 0.016897157993333575}
Rouge_L: {'precision': 0.11319969103164901, 'recall': 0.07813192705463566, 'fmeasure': 0.08515278899471597}

1 0 tensor(6.6022, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.0010, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.3380, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(5.2511, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.5299, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.4576379141565097

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18975266488921175, 'recall': 0.09235843694631775, 'fmeasure': 0.10664298077785521}
Rouge_2: {'precision': 0.03789261585871755, 'recall': 0.0056913764421358285, 'fmeasure': 0.009048043838409583}
Rouge_L: {'precision': 0.16161482277265363, 'recall': 0.07534823216951081, 'fmeasure': 0.08729981117484296}

2 0 tensor(3.6646, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.2797, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.3133, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.3054, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.1754, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.413338219715377

ROUGE Scores: (val)
Rouge_1: {'precision': 0.17707492106466233, 'recall': 0.09345776515302842, 'fmeasure': 0.10477832636845565}
Rouge_2: {'precision': 0.03941643472524514, 'recall': 0.009576435020482962, 'fmeasure': 0.01262978476938515}
Rouge_L: {'precision': 0.15443756238805298, 'recall': 0.08073227455389696, 'fmeasure': 0.09004913583816848}

3 0 tensor(1.9055, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.2843, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.4564, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.8552, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.3075, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.478856352426238

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19880049712235073, 'recall': 0.10252367422272682, 'fmeasure': 0.11830609409347197}
Rouge_2: {'precision': 0.02227236040795363, 'recall': 0.007165680341820926, 'fmeasure': 0.010202096240548391}
Rouge_L: {'precision': 0.16660807120269688, 'recall': 0.08441840357675567, 'fmeasure': 0.09828072233838017}

4 0 tensor(1.4586, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(0.7707, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.8466, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.5923, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(0.5213, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 20 3.739656534235356

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1398219261629032, 'recall': 0.06240512618360762, 'fmeasure': 0.07734956569991056}
Rouge_2: {'precision': 0.02154963680387409, 'recall': 0.0051414351579877, 'fmeasure': 0.008247900341327313}
Rouge_L: {'precision': 0.12330604340823682, 'recall': 0.05671772373518715, 'fmeasure': 0.06986426147137574}

5 0 tensor(1.6265, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(0.2324, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.7276, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.3550, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.8566, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 20 3.9871691778554754

ROUGE Scores: (val)
Rouge_1: {'precision': 0.16749390800879355, 'recall': 0.08647177150440377, 'fmeasure': 0.09912506558882285}
Rouge_2: {'precision': 0.0281275221953188, 'recall': 0.011301947130958952, 'fmeasure': 0.013674593916596598}
Rouge_L: {'precision': 0.1402803014550552, 'recall': 0.07474562809914187, 'fmeasure': 0.084653373643477}

6 0 tensor(2.0477, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.7171, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(2.3283, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.7259, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.6877, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 20 4.302558032132811

ROUGE Scores: (val)
Rouge_1: {'precision': 0.15504924918591814, 'recall': 0.08922565486261616, 'fmeasure': 0.09575457540222095}
Rouge_2: {'precision': 0.024085722473710682, 'recall': 0.007860978550995901, 'fmeasure': 0.010670550350498513}
Rouge_L: {'precision': 0.13524659596116284, 'recall': 0.07756959294516776, 'fmeasure': 0.08235258983795403}

7 0 tensor(0.6273, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.3411, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.4561, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.2550, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.9715, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 20 4.684532940387726

ROUGE Scores: (val)
Rouge_1: {'precision': 0.14666188426092922, 'recall': 0.08774456937267332, 'fmeasure': 0.10101546052199951}
Rouge_2: {'precision': 0.014569423043999314, 'recall': 0.00471419044829249, 'fmeasure': 0.006791721169278153}
Rouge_L: {'precision': 0.12090443649975582, 'recall': 0.0716109554201738, 'fmeasure': 0.08223384513807605}

8 0 tensor(1.1326, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(1.7726, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(0.9021, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(0.7792, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(2.9512, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 20 5.061381633503962

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1692041047768221, 'recall': 0.10639181437516688, 'fmeasure': 0.11805996589772458}
Rouge_2: {'precision': 0.033583054537559436, 'recall': 0.017243536412071995, 'fmeasure': 0.01991930004369086}
Rouge_L: {'precision': 0.14334270082629844, 'recall': 0.09001159987518126, 'fmeasure': 0.09974995727675638}

9 0 tensor(0.2827, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(0.6078, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(0.2357, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(0.8433, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(0.9112, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 9 20 5.158409631858437

ROUGE Scores: (val)
Rouge_1: {'precision': 0.15576815799240634, 'recall': 0.10717342078390357, 'fmeasure': 0.10940932319654742}
Rouge_2: {'precision': 0.01883345689427444, 'recall': 0.0067646683004410684, 'fmeasure': 0.009072810108209234}
Rouge_L: {'precision': 0.13142885680667074, 'recall': 0.09098035949783227, 'fmeasure': 0.09174708793823393}

10 0 tensor(1.4057, device='cuda:0', grad_fn=<NllLossBackward0>)

10 100 tensor(0.0618, device='cuda:0', grad_fn=<NllLossBackward0>)

10 200 tensor(0.0591, device='cuda:0', grad_fn=<NllLossBackward0>)

10 300 tensor(0.0492, device='cuda:0', grad_fn=<NllLossBackward0>)

10 400 tensor(0.0710, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 10 20 5.512615722114757

ROUGE Scores: (val)
Rouge_1: {'precision': 0.15755870648915205, 'recall': 0.10280467257475888, 'fmeasure': 0.10710516268816654}
Rouge_2: {'precision': 0.01366851825475953, 'recall': 0.004149773165280641, 'fmeasure': 0.006157796010226877}
Rouge_L: {'precision': 0.13370282602310202, 'recall': 0.08936421899574293, 'fmeasure': 0.09147852720187226}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.1049789964332448, 'recall': 0.09974800254728014, 'fmeasure': 0.08910241981924276}
Rouge_2: {'precision': 0.011083052749719416, 'recall': 0.01210826210826211, 'fmeasure': 0.010863175114382843}
Rouge_L: {'precision': 0.09218025740901557, 'recall': 0.08501632409011149, 'fmeasure': 0.0770378745847629}

avg_f1_1: 0.1450681305584354

avg_f1_2: 0.021635864939203706

avg_f1_L: 0.11954313071405191
