{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_1024_1e-4', 'EPOCH_SETTING': '20', 'NAME': 'IR_small_1024_1e-4_finetest9_20', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 9, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(5.0384, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.2832, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.5903, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.8947, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.2819, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.3603, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.0084, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.1927, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.8832, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.8830, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(2.6033, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.1170, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(1.3583, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.1848, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.8582, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(1.5131, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.0774, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.9578, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.6597, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.1276, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(1.2294, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.5054, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.4182, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.8293, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.5514, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(1.3336, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.6543, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.4849, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.1761, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.5479, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(1.0152, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.9607, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.5495, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.7458, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(0.8402, device='cuda:0', grad_fn=<NllLossBackward0>)

8 0 tensor(1.7941, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(2.8298, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(1.2915, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(0.7909, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.2345, device='cuda:0', grad_fn=<NllLossBackward0>)

9 0 tensor(0.1146, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(0.2104, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(0.0660, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(0.0720, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(0.5525, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.16040793239446782, 'recall': 0.10819931552153361, 'fmeasure': 0.11511462610144989}
Rouge_2: {'precision': 0.024192665147160147, 'recall': 0.013054749650053115, 'fmeasure': 0.014887755409105958}
Rouge_L: {'precision': 0.14034204440405154, 'recall': 0.09787810239247538, 'fmeasure': 0.10269743843500012}

BLEU Scores: (test)
{'bleu-1': 0.06102868045267936, 'bleu-2': 0.0060116246193403855, 'bleu-3': 1.357933214944493e-309, 'bleu-4': 9.31267190855142e-309, 'bleu': 4.4127862681246484e-156, 'bleu-corp-1': 0.06102868045267936, 'bleu-corp-2': 0.0060116246193403855, 'bleu-corp-3': 1.357933214944493e-309, 'bleu-corp-4': 9.31267190855142e-309, 'bleu-corp': 4.4127862681246484e-156}

METEOR Scores: (test)
{'meteor': 0.09165435409078733}

GLEU Scores: (test)
{'gleu-1': 0.07093576772533508, 'gleu-2': 0.0064154897712849195, 'gleu-3': 0.0, 'gleu-4': 0.0, 'gleu': 0.021620856935326522, 'gleu-corp-1': 0.07093576772533508, 'gleu-corp-2': 0.0064154897712849195, 'gleu-corp-3': 0.0, 'gleu-corp-4': 0.0, 'gleu-corp': 0.021620856935326522}
