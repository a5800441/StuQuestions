{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_1024_1e-4', 'EPOCH_SETTING': '15', 'NAME': 'IR_small_1024_1e-4_fine10_15', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 10, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(4.9064, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.5640, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.5159, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.1861, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(5.2324, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.568491883197073

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1729922619753128, 'recall': 0.10634186882106454, 'fmeasure': 0.12379798639762118}
Rouge_2: {'precision': 0.01012241054613936, 'recall': 0.006120527306967985, 'fmeasure': 0.007595731324544884}
Rouge_L: {'precision': 0.15154374815391763, 'recall': 0.09194671622800565, 'fmeasure': 0.10742481029164945}

2 0 tensor(2.9919, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.1931, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.7236, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.4319, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.6058, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.525605605820478

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2269069442798256, 'recall': 0.08421786035600771, 'fmeasure': 0.10850450079768738}
Rouge_2: {'precision': 0.0449623352165725, 'recall': 0.006867395838560884, 'fmeasure': 0.010481649906506853}
Rouge_L: {'precision': 0.20674368004876484, 'recall': 0.07285796111599879, 'fmeasure': 0.0951284045614035}

3 0 tensor(1.9422, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.8932, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.2217, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.2775, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.4371, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.600431567531521

ROUGE Scores: (val)
Rouge_1: {'precision': 0.189507979737634, 'recall': 0.1269971572986698, 'fmeasure': 0.14045011313461833}
Rouge_2: {'precision': 0.01689534570890503, 'recall': 0.007836966682797611, 'fmeasure': 0.01018300324007989}
Rouge_L: {'precision': 0.15967968188001447, 'recall': 0.10176504929251552, 'fmeasure': 0.11522517871146185}

4 0 tensor(2.1376, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.3307, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.7356, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(1.7168, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.5595, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.9729175446397167

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1599511903848893, 'recall': 0.09214379684508202, 'fmeasure': 0.10937973687515687}
Rouge_2: {'precision': 0.0054143126177024475, 'recall': 0.002966101694915254, 'fmeasure': 0.0038054419118124555}
Rouge_L: {'precision': 0.13296894475857585, 'recall': 0.07659300743724536, 'fmeasure': 0.09151120350073337}

5 0 tensor(2.5088, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.3446, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.6368, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.4671, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.5752, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 4.102938203488366

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1821286355630691, 'recall': 0.12338728788371565, 'fmeasure': 0.13631449089099806}
Rouge_2: {'precision': 0.014850686037126715, 'recall': 0.010060097485321813, 'fmeasure': 0.01162251271600643}
Rouge_L: {'precision': 0.1632859669322649, 'recall': 0.10902227355419265, 'fmeasure': 0.12105204399208126}

6 0 tensor(0.4211, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.1851, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.7904, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.1728, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.4041, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 0 4.376086414870569

ROUGE Scores: (val)
Rouge_1: {'precision': 0.17939157254797292, 'recall': 0.12853418936954938, 'fmeasure': 0.13667459099888718}
Rouge_2: {'precision': 0.026104850892986482, 'recall': 0.018176858313947046, 'fmeasure': 0.01909294006636163}
Rouge_L: {'precision': 0.15036292767555495, 'recall': 0.10620099904587404, 'fmeasure': 0.11353866204465836}

7 0 tensor(0.7691, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.4769, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(0.6080, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(1.4815, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.7383, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 0 4.836665317163629

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19814171604983377, 'recall': 0.14398362922833846, 'fmeasure': 0.1558420272053427}
Rouge_2: {'precision': 0.015599090175361362, 'recall': 0.01189245868552094, 'fmeasure': 0.013042330901254132}
Rouge_L: {'precision': 0.16558679451186142, 'recall': 0.11859109844572795, 'fmeasure': 0.1285356338444102}

8 0 tensor(0.5600, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(0.8040, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(0.0474, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(0.5088, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.4039, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 0 5.271595073958575

ROUGE Scores: (val)
Rouge_1: {'precision': 0.17082571040588299, 'recall': 0.11154677454624011, 'fmeasure': 0.12240136107351264}
Rouge_2: {'precision': 0.006904700760632964, 'recall': 0.0067715899919289745, 'fmeasure': 0.006609445168767203}
Rouge_L: {'precision': 0.15345751837038848, 'recall': 0.09838243149917679, 'fmeasure': 0.10798844683390572}

9 0 tensor(0.2341, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(0.7807, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(0.0466, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(0.0442, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(0.3258, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 9 0 5.317424913584175

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18563766108303553, 'recall': 0.13349102286438733, 'fmeasure': 0.1440510892800215}
Rouge_2: {'precision': 0.02493135912614319, 'recall': 0.017152475322570297, 'fmeasure': 0.019276424595519388}
Rouge_L: {'precision': 0.1532815547987139, 'recall': 0.1075525989437373, 'fmeasure': 0.11703391707901788}

10 0 tensor(0.1658, device='cuda:0', grad_fn=<NllLossBackward0>)

10 100 tensor(0.3428, device='cuda:0', grad_fn=<NllLossBackward0>)

10 200 tensor(0.6614, device='cuda:0', grad_fn=<NllLossBackward0>)

10 300 tensor(0.0567, device='cuda:0', grad_fn=<NllLossBackward0>)

10 400 tensor(0.1417, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 10 0 5.578251897278479

ROUGE Scores: (val)
Rouge_1: {'precision': 0.16819358773496354, 'recall': 0.13376755722151457, 'fmeasure': 0.1404056064773566}
Rouge_2: {'precision': 0.022040324627300534, 'recall': 0.014810103658250662, 'fmeasure': 0.01722220452821242}
Rouge_L: {'precision': 0.14838278205676012, 'recall': 0.11717329598880023, 'fmeasure': 0.12301445606458139}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.1674731651098281, 'recall': 0.1395501260727562, 'fmeasure': 0.13360986147582085}
Rouge_2: {'precision': 0.02535382992725076, 'recall': 0.01780615509221152, 'fmeasure': 0.016460777514139276}
Rouge_L: {'precision': 0.13920194220026702, 'recall': 0.11652724318282194, 'fmeasure': 0.11139051550812551}

1 0 tensor(5.2369, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(5.0089, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.7318, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.4703, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.5054, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.5894352517476897

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2096999509023785, 'recall': 0.12376994084580183, 'fmeasure': 0.1340384645429716}
Rouge_2: {'precision': 0.04458976852977458, 'recall': 0.01580429961734129, 'fmeasure': 0.019733974453060278}
Rouge_L: {'precision': 0.1842454727955558, 'recall': 0.10447402022158193, 'fmeasure': 0.1157536666162533}

2 0 tensor(1.7145, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.7220, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.7532, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.5766, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.8770, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.8015724406009768

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20077349548938303, 'recall': 0.10258594744158948, 'fmeasure': 0.11512960914633423}
Rouge_2: {'precision': 0.01690379403794038, 'recall': 0.008032182273947407, 'fmeasure': 0.009456628228458542}
Rouge_L: {'precision': 0.1746213759748268, 'recall': 0.08412010745394359, 'fmeasure': 0.09587926776378565}

3 0 tensor(2.3178, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.2306, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.0295, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.6760, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.4813, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.808878680554832

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18995524331994126, 'recall': 0.10565712991589365, 'fmeasure': 0.1198480759311251}
Rouge_2: {'precision': 0.025437237121239988, 'recall': 0.012552525560759607, 'fmeasure': 0.015489983499122176}
Rouge_L: {'precision': 0.17084381845170146, 'recall': 0.09404832687750962, 'fmeasure': 0.10674383397290797}

4 0 tensor(2.3563, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.2810, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.2390, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.7596, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.6115, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 10 3.7606186983062

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21497919796690845, 'recall': 0.10494445689552213, 'fmeasure': 0.12692028925739676}
Rouge_2: {'precision': 0.023278076936613525, 'recall': 0.008053726459906591, 'fmeasure': 0.011332344464752552}
Rouge_L: {'precision': 0.18655697655688222, 'recall': 0.09135628605588031, 'fmeasure': 0.1097464405386749}

5 0 tensor(3.1011, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.3048, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.5398, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.3590, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.1479, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 10 4.140553705575989

ROUGE Scores: (val)
Rouge_1: {'precision': 0.17159300694942162, 'recall': 0.09449046469916655, 'fmeasure': 0.11024797872470897}
Rouge_2: {'precision': 0.025332418259247525, 'recall': 0.008737062482685699, 'fmeasure': 0.012058062465826719}
Rouge_L: {'precision': 0.1420635853109613, 'recall': 0.08077041837573856, 'fmeasure': 0.09244207333678837}

6 0 tensor(0.7124, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(2.4021, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(1.3037, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.1473, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.9712, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 10 4.2406702972039945

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19367746878431158, 'recall': 0.1173048073063431, 'fmeasure': 0.12690796691682085}
Rouge_2: {'precision': 0.028599121602972698, 'recall': 0.01166414452078084, 'fmeasure': 0.014673529698523593}
Rouge_L: {'precision': 0.16533711012175595, 'recall': 0.09888890027203855, 'fmeasure': 0.1069499344480135}

7 0 tensor(0.7323, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.4851, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(0.8115, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.5236, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.4777, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 10 4.768503346094271

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19258509464566176, 'recall': 0.11951735286335412, 'fmeasure': 0.13499270147763207}
Rouge_2: {'precision': 0.035495426866052525, 'recall': 0.016476566104753596, 'fmeasure': 0.021398363179493913}
Rouge_L: {'precision': 0.16169775988559631, 'recall': 0.09943426881250529, 'fmeasure': 0.11159359789618163}

8 0 tensor(0.4006, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(0.8792, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(0.0170, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(0.2123, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(1.7428, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 10 5.006851149768364

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18421866133797618, 'recall': 0.11431141947134155, 'fmeasure': 0.12460624706220098}
Rouge_2: {'precision': 0.03780817759476296, 'recall': 0.01801057653714841, 'fmeasure': 0.0214508839498976}
Rouge_L: {'precision': 0.16195433352486793, 'recall': 0.101079234153544, 'fmeasure': 0.10889445306817892}

9 0 tensor(1.4994, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(1.5374, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(0.0683, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(0.0530, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(0.2081, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 9 10 5.158455835609901

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1738436235184517, 'recall': 0.09589169236993789, 'fmeasure': 0.11334659808631402}
Rouge_2: {'precision': 0.03205484609528256, 'recall': 0.013651797897945983, 'fmeasure': 0.017184353397386335}
Rouge_L: {'precision': 0.1443812003437843, 'recall': 0.08147259623094066, 'fmeasure': 0.0947146285341555}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.17448283730413472, 'recall': 0.09771950613482053, 'fmeasure': 0.10718940027702026}
Rouge_2: {'precision': 0.050174334709218435, 'recall': 0.01715567378358076, 'fmeasure': 0.02039349747526792}
Rouge_L: {'precision': 0.14535756980677422, 'recall': 0.07211661073712348, 'fmeasure': 0.08203430728488495}

1 0 tensor(5.5227, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(2.3887, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.3379, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(5.0655, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.3742, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.4195102978560885

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20068315560588743, 'recall': 0.10122518959770284, 'fmeasure': 0.12113881370841886}
Rouge_2: {'precision': 0.03258676351896691, 'recall': 0.00513917340213303, 'fmeasure': 0.007724234983147901}
Rouge_L: {'precision': 0.1758264846375514, 'recall': 0.08930733385154938, 'fmeasure': 0.10563014932436121}

2 0 tensor(3.6360, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.1990, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.4290, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.1734, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.0778, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.360375356876244

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2132673773114294, 'recall': 0.11519341648401522, 'fmeasure': 0.1315604566654417}
Rouge_2: {'precision': 0.04444238012021503, 'recall': 0.010082258629628185, 'fmeasure': 0.013520475781062441}
Rouge_L: {'precision': 0.17586339441694426, 'recall': 0.09498484762012878, 'fmeasure': 0.10769493083879915}

3 0 tensor(1.6282, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.0595, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.4226, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.8796, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.3117, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.423070330235918

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22800275351568844, 'recall': 0.13362998797185544, 'fmeasure': 0.14189549460349948}
Rouge_2: {'precision': 0.03802856029885468, 'recall': 0.02257008015120406, 'fmeasure': 0.025469208266321694}
Rouge_L: {'precision': 0.19428050515249448, 'recall': 0.11412595762089665, 'fmeasure': 0.12095040201600969}

4 0 tensor(1.2820, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(0.5861, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.6851, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.0417, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(0.5335, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 20 3.7024849885601108

ROUGE Scores: (val)
Rouge_1: {'precision': 0.16841249616673346, 'recall': 0.09143512893977142, 'fmeasure': 0.10505050768533118}
Rouge_2: {'precision': 0.024305223150791706, 'recall': 0.013911319969381483, 'fmeasure': 0.01666230341223741}
Rouge_L: {'precision': 0.15012268804641693, 'recall': 0.08200233615230168, 'fmeasure': 0.093269752034042}

5 0 tensor(1.8271, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(0.3247, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.9443, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.1885, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.8556, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 20 3.9572418478585907

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19254266214486818, 'recall': 0.11793831272461408, 'fmeasure': 0.13184446259211952}
Rouge_2: {'precision': 0.03849512069851053, 'recall': 0.015820021962635312, 'fmeasure': 0.020848799959514445}
Rouge_L: {'precision': 0.15888198398599118, 'recall': 0.09723252911078728, 'fmeasure': 0.10850507495107649}

6 0 tensor(1.9426, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.7431, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(2.0860, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.5616, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.8679, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 20 4.210285356489279

ROUGE Scores: (val)
Rouge_1: {'precision': 0.17429820395331752, 'recall': 0.10323212188709968, 'fmeasure': 0.11212804906652497}
Rouge_2: {'precision': 0.03456963543939458, 'recall': 0.014169235565724038, 'fmeasure': 0.018669583074001232}
Rouge_L: {'precision': 0.14796830604342276, 'recall': 0.08959703100473904, 'fmeasure': 0.09598411680474099}

7 0 tensor(0.6015, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(0.9734, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.1195, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.2132, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.5089, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 20 4.572622558828128

ROUGE Scores: (val)
Rouge_1: {'precision': 0.16098452419817272, 'recall': 0.08571296290575699, 'fmeasure': 0.09632645925004822}
Rouge_2: {'precision': 0.02642767944363159, 'recall': 0.01154629724307419, 'fmeasure': 0.014195885106372885}
Rouge_L: {'precision': 0.13877544177499573, 'recall': 0.0748429413835856, 'fmeasure': 0.08318961828154645}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.12443516869165888, 'recall': 0.10671969103341651, 'fmeasure': 0.10692877075154168}
Rouge_2: {'precision': 0.011788520121853455, 'recall': 0.0097918179617526, 'fmeasure': 0.009941556999278138}
Rouge_L: {'precision': 0.112963975276021, 'recall': 0.09487760852139936, 'fmeasure': 0.09530596153227452}

avg_f1_1: 0.14424340776215808

avg_f1_2: 0.022065505603912895

avg_f1_L: 0.1217465674922244
