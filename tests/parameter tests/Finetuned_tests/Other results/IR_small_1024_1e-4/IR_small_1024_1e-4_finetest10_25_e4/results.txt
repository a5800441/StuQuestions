{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_1024_1e-4', 'EPOCH_SETTING': '25', 'NAME': 'IR_small_1024_1e-4_finetest10_25_e4', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 10, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(6.2038, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.5551, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.6930, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.9992, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.4021, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.4077, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.0662, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.3074, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.0686, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.9348, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(2.6463, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.2574, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(1.8411, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.4087, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.9566, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(1.8289, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.0873, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.8474, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.5194, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.3747, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(1.0707, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.6649, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.8000, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.2331, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(2.5462, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(1.9377, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.2102, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.6549, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.6067, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.8058, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(1.0082, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(2.1990, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.5367, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(1.1914, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.5128, device='cuda:0', grad_fn=<NllLossBackward0>)

8 0 tensor(1.7851, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(2.9705, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(1.3760, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(1.5170, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.2141, device='cuda:0', grad_fn=<NllLossBackward0>)

9 0 tensor(0.3359, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(0.3877, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(0.4074, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(0.0559, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(0.8268, device='cuda:0', grad_fn=<NllLossBackward0>)

10 0 tensor(0.4120, device='cuda:0', grad_fn=<NllLossBackward0>)

10 100 tensor(0.4645, device='cuda:0', grad_fn=<NllLossBackward0>)

10 200 tensor(1.2271, device='cuda:0', grad_fn=<NllLossBackward0>)

10 300 tensor(0.6247, device='cuda:0', grad_fn=<NllLossBackward0>)

10 400 tensor(0.1252, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.17029290055443932, 'recall': 0.11936131340413432, 'fmeasure': 0.12669508310772612}
Rouge_2: {'precision': 0.02280799112097669, 'recall': 0.011260392935724438, 'fmeasure': 0.013502370385061118}
Rouge_L: {'precision': 0.14430002642353146, 'recall': 0.1046522133648456, 'fmeasure': 0.10956906728436919}

BLEU Scores: (test)
{'bleu-1': 0.059559858958050216, 'bleu-2': 0.003552694733730949, 'bleu-3': 0.0017475264790636884, 'bleu-4': 0.001698984076867475, 'bleu': 0.0021864527966160945, 'bleu-corp-1': 0.059559858958050216, 'bleu-corp-2': 0.003552694733730949, 'bleu-corp-3': 0.0017475264790636884, 'bleu-corp-4': 0.001698984076867475, 'bleu-corp': 0.0021864527966160945}

METEOR Scores: (test)
{'meteor': 0.1007250577785115}

GLEU Scores: (test)
{'gleu-1': 0.07197260002049637, 'gleu-2': 0.005826859045504995, 'gleu-3': 0.0033296337402885685, 'gleu-4': 0.0023584905660377358, 'gleu': 0.022915233626541544, 'gleu-corp-1': 0.07197260002049637, 'gleu-corp-2': 0.005826859045504995, 'gleu-corp-3': 0.0033296337402885685, 'gleu-corp-4': 0.0023584905660377358, 'gleu-corp': 0.022915233626541544}
