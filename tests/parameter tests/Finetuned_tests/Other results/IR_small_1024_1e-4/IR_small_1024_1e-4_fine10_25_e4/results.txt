{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_1024_1e-4', 'EPOCH_SETTING': '25', 'NAME': 'IR_small_1024_1e-4_fine10_25_e4', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 10, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(5.2150, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.7488, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.5454, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.3616, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(5.1906, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.619131326675415

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18725795501717735, 'recall': 0.13784569158644303, 'fmeasure': 0.1415845323918471}
Rouge_2: {'precision': 0.01982342045901368, 'recall': 0.015795658138366853, 'fmeasure': 0.014709049545843511}
Rouge_L: {'precision': 0.15693756227156028, 'recall': 0.1205373338867593, 'fmeasure': 0.12117062075734746}

2 0 tensor(2.8932, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.5514, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.0299, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.5977, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.9652, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.623170569791632

ROUGE Scores: (val)
Rouge_1: {'precision': 0.17953149929720216, 'recall': 0.08519206077380238, 'fmeasure': 0.10656766688029101}
Rouge_2: {'precision': 0.0323788734805684, 'recall': 0.008024909824302835, 'fmeasure': 0.011294173650519249}
Rouge_L: {'precision': 0.15980456897705153, 'recall': 0.07541133800703775, 'fmeasure': 0.09427213249358832}

3 0 tensor(2.4872, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.9355, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.4930, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.2299, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.4903, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.6335546889547574

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19357177414715318, 'recall': 0.11824678205960848, 'fmeasure': 0.13455206693870983}
Rouge_2: {'precision': 0.01711118448406584, 'recall': 0.01263719767772826, 'fmeasure': 0.013223816182996714}
Rouge_L: {'precision': 0.1676696247057531, 'recall': 0.10049191728330799, 'fmeasure': 0.11557696426619023}

4 0 tensor(2.5878, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.5462, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.3447, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(1.8454, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.5614, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.911340846853741

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1982961437092465, 'recall': 0.14207892510292233, 'fmeasure': 0.15675119392308876}
Rouge_2: {'precision': 0.0216694322626526, 'recall': 0.014992829195872241, 'fmeasure': 0.01696619137297103}
Rouge_L: {'precision': 0.16796066173132343, 'recall': 0.11835083491605633, 'fmeasure': 0.13146144009618774}

5 0 tensor(2.6829, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.4962, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(0.8882, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.4090, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.7458, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 4.086367035316209

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19458872726044893, 'recall': 0.1373371204816885, 'fmeasure': 0.14941155562998873}
Rouge_2: {'precision': 0.023559397288210847, 'recall': 0.01677024482109228, 'fmeasure': 0.01834138182661578}
Rouge_L: {'precision': 0.1566651630656983, 'recall': 0.1116041785224255, 'fmeasure': 0.12023406285332465}

6 0 tensor(0.8693, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.2760, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(1.1384, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.5180, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.3381, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 0 4.384617268028906

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1688169867704889, 'recall': 0.1473776325810783, 'fmeasure': 0.14104567291895298}
Rouge_2: {'precision': 0.009546330603810339, 'recall': 0.005599515627686633, 'fmeasure': 0.006721048852843125}
Rouge_L: {'precision': 0.14324226801780407, 'recall': 0.12260472666672831, 'fmeasure': 0.11818136654777799}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.14816281733520514, 'recall': 0.15616604037643828, 'fmeasure': 0.13808454932036898}
Rouge_2: {'precision': 0.012344809278771542, 'recall': 0.013493237966045957, 'fmeasure': 0.011075526443755293}
Rouge_L: {'precision': 0.11868904170024114, 'recall': 0.12746718861030745, 'fmeasure': 0.11155363866660237}

1 0 tensor(6.6104, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(5.1041, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.9065, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.1953, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.7363, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.5511044903499323

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21428519073784488, 'recall': 0.16660249957357606, 'fmeasure': 0.16649907730842547}
Rouge_2: {'precision': 0.052342656745454436, 'recall': 0.02924363298417531, 'fmeasure': 0.0304775459424517}
Rouge_L: {'precision': 0.1719461416112476, 'recall': 0.1300557094048317, 'fmeasure': 0.1301886702746645}

2 0 tensor(1.5581, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.7070, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.9253, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.5733, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(1.2382, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.55670292784528

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19929974267596026, 'recall': 0.13257148281282416, 'fmeasure': 0.14314010772971475}
Rouge_2: {'precision': 0.044761071464801745, 'recall': 0.024662748468321054, 'fmeasure': 0.028646613000101722}
Rouge_L: {'precision': 0.16679188062560402, 'recall': 0.1111954529089065, 'fmeasure': 0.12006575708806289}

3 0 tensor(2.3888, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(1.3528, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.3497, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.7598, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.4521, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.6028820087270037

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1822000914398848, 'recall': 0.13120042268584264, 'fmeasure': 0.1358919258975204}
Rouge_2: {'precision': 0.02152674110689666, 'recall': 0.011178713776158361, 'fmeasure': 0.012926091247869698}
Rouge_L: {'precision': 0.15262243190848024, 'recall': 0.10905499865894067, 'fmeasure': 0.11244876906933003}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.2047109012738288, 'recall': 0.14821559874398454, 'fmeasure': 0.15114037998459215}
Rouge_2: {'precision': 0.02003306770748631, 'recall': 0.01936660407337859, 'fmeasure': 0.019288786064003104}
Rouge_L: {'precision': 0.15934454662224845, 'recall': 0.11689628880121355, 'fmeasure': 0.11766621461135801}

1 0 tensor(6.7572, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(2.8343, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.1213, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(5.4523, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.4299, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.7857057785583756

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19122212465169858, 'recall': 0.13245757229673655, 'fmeasure': 0.14274856528415328}
Rouge_2: {'precision': 0.009888390384402347, 'recall': 0.009829168620620351, 'fmeasure': 0.009387463180802146}
Rouge_L: {'precision': 0.14597007586905741, 'recall': 0.10359184247210665, 'fmeasure': 0.1096895202545735}

2 0 tensor(4.6341, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.5347, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.3951, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.4427, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.4008, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.4131408040806397

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19917412352737954, 'recall': 0.10658712492517455, 'fmeasure': 0.11584378579637684}
Rouge_2: {'precision': 0.05778853914447134, 'recall': 0.010406168043635727, 'fmeasure': 0.014571273045849315}
Rouge_L: {'precision': 0.17871303416575487, 'recall': 0.09216243322844889, 'fmeasure': 0.10059943155379045}

3 0 tensor(2.2385, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.2613, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.6717, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(4.0687, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.9240, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.429380407777883

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19872850285861254, 'recall': 0.09894761065469598, 'fmeasure': 0.11762289472929531}
Rouge_2: {'precision': 0.023520312079634113, 'recall': 0.008234873178714424, 'fmeasure': 0.011870112312343842}
Rouge_L: {'precision': 0.1721726293535865, 'recall': 0.08474288635197662, 'fmeasure': 0.10038970910288508}

4 0 tensor(1.7153, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(1.0743, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.1531, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.0166, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.3276, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 20 3.7636255207708325

ROUGE Scores: (val)
Rouge_1: {'precision': 0.17038665777908577, 'recall': 0.11853293690392588, 'fmeasure': 0.126004210010041}
Rouge_2: {'precision': 0.007776669990029911, 'recall': 0.001461953550288825, 'fmeasure': 0.0024213075060532684}
Rouge_L: {'precision': 0.1381787851476155, 'recall': 0.09466689604671082, 'fmeasure': 0.10053004176174304}

5 0 tensor(3.3491, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(0.7531, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.7364, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.3211, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.3083, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 20 3.6917247297400135

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1537837063090251, 'recall': 0.09910314966371875, 'fmeasure': 0.10584124436290147}
Rouge_2: {'precision': 0.016025309021320988, 'recall': 0.01101196191837312, 'fmeasure': 0.011590445362831036}
Rouge_L: {'precision': 0.13261699192984286, 'recall': 0.08653896458370425, 'fmeasure': 0.09133222466810588}

6 0 tensor(2.0612, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.2067, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(2.5065, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.2903, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.1342, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 20 4.2072363798901184

ROUGE Scores: (val)
Rouge_1: {'precision': 0.15763382292176914, 'recall': 0.09335361105382638, 'fmeasure': 0.10603688944439563}
Rouge_2: {'precision': 0.011811822828771981, 'recall': 0.00406730791483215, 'fmeasure': 0.0057051391797154506}
Rouge_L: {'precision': 0.1347847755968075, 'recall': 0.08258984646496753, 'fmeasure': 0.09241660677496885}

7 0 tensor(1.4391, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.5800, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(2.3021, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.6606, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(2.5469, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 20 4.269688122353311

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1526844879858208, 'recall': 0.09569561659260935, 'fmeasure': 0.1064790491467876}
Rouge_2: {'precision': 0.02464177978883861, 'recall': 0.01313485091143465, 'fmeasure': 0.015496498600351084}
Rouge_L: {'precision': 0.12362427822770482, 'recall': 0.07829289053621408, 'fmeasure': 0.08646769511936711}

8 0 tensor(1.1046, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(2.4814, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(0.8523, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(1.2044, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(3.1902, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 8 20 4.648586073164212

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1568599483548059, 'recall': 0.10022741281799585, 'fmeasure': 0.11329005057849718}
Rouge_2: {'precision': 0.02615213882163035, 'recall': 0.014271730329597915, 'fmeasure': 0.017765256524992196}
Rouge_L: {'precision': 0.12754830129902284, 'recall': 0.08424251951015191, 'fmeasure': 0.09349835054678113}

9 0 tensor(0.5705, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(0.8328, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(0.2147, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(0.9604, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(1.4041, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 9 20 4.8657796630414865

ROUGE Scores: (val)
Rouge_1: {'precision': 0.15647663135425777, 'recall': 0.12097390702321816, 'fmeasure': 0.11934628585634982}
Rouge_2: {'precision': 0.020945601854049284, 'recall': 0.015919231406180172, 'fmeasure': 0.014863342208977273}
Rouge_L: {'precision': 0.13188281621792106, 'recall': 0.10561415338236639, 'fmeasure': 0.10310179015634627}

10 0 tensor(1.4868, device='cuda:0', grad_fn=<NllLossBackward0>)

10 100 tensor(0.1677, device='cuda:0', grad_fn=<NllLossBackward0>)

10 200 tensor(0.2375, device='cuda:0', grad_fn=<NllLossBackward0>)

10 300 tensor(0.4260, device='cuda:0', grad_fn=<NllLossBackward0>)

10 400 tensor(0.2314, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 10 20 5.306759682752318

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1379247284475022, 'recall': 0.0787380270758096, 'fmeasure': 0.08380203856270003}
Rouge_2: {'precision': 0.030532015065913368, 'recall': 0.008552554811788777, 'fmeasure': 0.009912341928013206}
Rouge_L: {'precision': 0.10959683075296761, 'recall': 0.06470059421448876, 'fmeasure': 0.0664237354579159}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.11980812593239866, 'recall': 0.08907648897758978, 'fmeasure': 0.0879394884401003}
Rouge_2: {'precision': 0.043049543049543046, 'recall': 0.011437908496732025, 'fmeasure': 0.016192108758728674}
Rouge_L: {'precision': 0.10856506971544137, 'recall': 0.07334116300834569, 'fmeasure': 0.07555570776733737}

avg_f1_1: 0.1553329455052225

avg_f1_2: 0.022194728098019895

avg_f1_L: 0.12377987687514191
