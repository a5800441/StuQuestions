{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_1024_1e-4', 'EPOCH_SETTING': '25', 'NAME': 'IR_small_1024_1e-4_fine10_25', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 10, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(5.2150, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.7488, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.5454, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.3616, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(5.1906, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.619131326675415

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18725795501717735, 'recall': 0.13784569158644303, 'fmeasure': 0.1415845323918471}
Rouge_2: {'precision': 0.01982342045901368, 'recall': 0.015795658138366853, 'fmeasure': 0.014709049545843511}
Rouge_L: {'precision': 0.15693756227156028, 'recall': 0.1205373338867593, 'fmeasure': 0.12117062075734746}

2 0 tensor(2.8932, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.5514, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.0299, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.5977, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.9652, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.623170569791632

ROUGE Scores: (val)
Rouge_1: {'precision': 0.17953149929720216, 'recall': 0.08519206077380238, 'fmeasure': 0.10656766688029101}
Rouge_2: {'precision': 0.0323788734805684, 'recall': 0.008024909824302835, 'fmeasure': 0.011294173650519249}
Rouge_L: {'precision': 0.15980456897705153, 'recall': 0.07541133800703775, 'fmeasure': 0.09427213249358832}

3 0 tensor(2.4872, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.9355, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.4930, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.2299, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.4903, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.6335546889547574

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19357177414715318, 'recall': 0.11824678205960848, 'fmeasure': 0.13455206693870983}
Rouge_2: {'precision': 0.01711118448406584, 'recall': 0.01263719767772826, 'fmeasure': 0.013223816182996714}
Rouge_L: {'precision': 0.1676696247057531, 'recall': 0.10049191728330799, 'fmeasure': 0.11557696426619023}

4 0 tensor(2.5878, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.5462, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.3447, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(1.8454, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.5614, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.911340846853741

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1982961437092465, 'recall': 0.14207892510292233, 'fmeasure': 0.15675119392308876}
Rouge_2: {'precision': 0.0216694322626526, 'recall': 0.014992829195872241, 'fmeasure': 0.01696619137297103}
Rouge_L: {'precision': 0.16796066173132343, 'recall': 0.11835083491605633, 'fmeasure': 0.13146144009618774}

5 0 tensor(2.6829, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.4962, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(0.8882, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.4090, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.7458, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 4.086367035316209

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19458872726044893, 'recall': 0.1373371204816885, 'fmeasure': 0.14941155562998873}
Rouge_2: {'precision': 0.023559397288210847, 'recall': 0.01677024482109228, 'fmeasure': 0.01834138182661578}
Rouge_L: {'precision': 0.1566651630656983, 'recall': 0.1116041785224255, 'fmeasure': 0.12023406285332465}

6 0 tensor(0.8693, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.2760, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(1.1384, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.5180, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.3381, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 0 4.384617268028906

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1688169867704889, 'recall': 0.1473776325810783, 'fmeasure': 0.14104567291895298}
Rouge_2: {'precision': 0.009546330603810339, 'recall': 0.005599515627686633, 'fmeasure': 0.006721048852843125}
Rouge_L: {'precision': 0.14324226801780407, 'recall': 0.12260472666672831, 'fmeasure': 0.11818136654777799}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.14816281733520514, 'recall': 0.15616604037643828, 'fmeasure': 0.13808454932036898}
Rouge_2: {'precision': 0.012344809278771542, 'recall': 0.013493237966045957, 'fmeasure': 0.011075526443755293}
Rouge_L: {'precision': 0.11868904170024114, 'recall': 0.12746718861030745, 'fmeasure': 0.11155363866660237}

1 0 tensor(6.6104, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(5.1041, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.9065, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.1953, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.7363, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.5511044903499323

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21428519073784488, 'recall': 0.16660249957357606, 'fmeasure': 0.16649907730842547}
Rouge_2: {'precision': 0.052342656745454436, 'recall': 0.02924363298417531, 'fmeasure': 0.0304775459424517}
Rouge_L: {'precision': 0.1719461416112476, 'recall': 0.1300557094048317, 'fmeasure': 0.1301886702746645}

2 0 tensor(1.5581, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.7070, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.9253, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.5733, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(1.2382, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.55670292784528

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19929974267596026, 'recall': 0.13257148281282416, 'fmeasure': 0.14314010772971475}
Rouge_2: {'precision': 0.044761071464801745, 'recall': 0.024662748468321054, 'fmeasure': 0.028646613000101722}
Rouge_L: {'precision': 0.16679188062560402, 'recall': 0.1111954529089065, 'fmeasure': 0.12006575708806289}

3 0 tensor(2.3888, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(1.3528, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.3497, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.7598, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.4521, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.6028820087270037

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1822000914398848, 'recall': 0.13120042268584264, 'fmeasure': 0.1358919258975204}
Rouge_2: {'precision': 0.02152674110689666, 'recall': 0.011178713776158361, 'fmeasure': 0.012926091247869698}
Rouge_L: {'precision': 0.15262243190848024, 'recall': 0.10905499865894067, 'fmeasure': 0.11244876906933003}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.2047109012738288, 'recall': 0.14821559874398454, 'fmeasure': 0.15114037998459215}
Rouge_2: {'precision': 0.02003306770748631, 'recall': 0.01936660407337859, 'fmeasure': 0.019288786064003104}
Rouge_L: {'precision': 0.15934454662224845, 'recall': 0.11689628880121355, 'fmeasure': 0.11766621461135801}

1 0 tensor(6.7572, device='cuda:0', grad_fn=<NllLossBackward0>)
