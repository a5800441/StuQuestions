{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_612_1e-5_DocT5Query', 'EPOCH_SETTING': '5', 'NAME': 'Learningq_612_1e-5_DocT5Query_tune7_1', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 7, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 1e-05, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(3.9349, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.1851, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.7689, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.4267, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(5.5103, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.5853630243721657

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24315143425312918, 'recall': 0.18261735326701461, 'fmeasure': 0.19587908641361476}
Rouge_2: {'precision': 0.06507875363807568, 'recall': 0.04826273672675129, 'fmeasure': 0.0514475879447391}
Rouge_L: {'precision': 0.21924022305378235, 'recall': 0.16690956097096654, 'fmeasure': 0.17807723878146187}

2 0 tensor(3.8987, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.6808, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(2.2197, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.9736, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.3614, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.4708977250729576

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2716884007761375, 'recall': 0.1832860103284499, 'fmeasure': 0.2058417538118788}
Rouge_2: {'precision': 0.07086115391200137, 'recall': 0.04211580001943466, 'fmeasure': 0.04976522425723401}
Rouge_L: {'precision': 0.23898446878257443, 'recall': 0.1650763255564954, 'fmeasure': 0.18399628091072975}

3 0 tensor(2.9002, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.1567, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.4538, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.6888, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(5.5444, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.4070737483137745

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2557718646701697, 'recall': 0.1806304671016, 'fmeasure': 0.1991352340141096}
Rouge_2: {'precision': 0.0764592281541434, 'recall': 0.051408572031761246, 'fmeasure': 0.05783458828927685}
Rouge_L: {'precision': 0.2270400503451351, 'recall': 0.1642569727028564, 'fmeasure': 0.17933846233978093}

4 0 tensor(3.9128, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.9702, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(4.1981, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.5426, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.7630, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.3834423513735756

ROUGE Scores: (val)
Rouge_1: {'precision': 0.25166144091866915, 'recall': 0.1700601292613556, 'fmeasure': 0.19026863918412118}
Rouge_2: {'precision': 0.07417939875566995, 'recall': 0.0486840068442401, 'fmeasure': 0.055850247589503514}
Rouge_L: {'precision': 0.229764145791065, 'recall': 0.1581729355576549, 'fmeasure': 0.17601153241317666}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.26668986563475144, 'recall': 0.18067189459770244, 'fmeasure': 0.19656106203878915}
Rouge_2: {'precision': 0.06511884342073021, 'recall': 0.05321401899911159, 'fmeasure': 0.05201030740720939}
Rouge_L: {'precision': 0.22681819036089151, 'recall': 0.15884607967494546, 'fmeasure': 0.17055658806702453}

1 0 tensor(3.5749, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.6175, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.9790, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(2.9306, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.9396, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.59478992514494

ROUGE Scores: (val)
Rouge_1: {'precision': 0.26370320059344443, 'recall': 0.15245268653477237, 'fmeasure': 0.17540197265571364}
Rouge_2: {'precision': 0.0607021027752735, 'recall': 0.039065780696355, 'fmeasure': 0.04349816629494699}
Rouge_L: {'precision': 0.22920578743749465, 'recall': 0.13890428967838922, 'fmeasure': 0.15667612361249636}

2 0 tensor(1.4898, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.1925, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.3998, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.1194, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(1.3327, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.4431509971618652

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2779677470226249, 'recall': 0.16731635291266292, 'fmeasure': 0.189780881763057}
Rouge_2: {'precision': 0.07466415698123015, 'recall': 0.043708672907669076, 'fmeasure': 0.051198710129793816}
Rouge_L: {'precision': 0.2400768100463221, 'recall': 0.15108402183456018, 'fmeasure': 0.16865469199880417}

3 0 tensor(2.3986, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.0964, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.9494, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.2353, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.6935, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.376980541682825

ROUGE Scores: (val)
Rouge_1: {'precision': 0.28040188131651544, 'recall': 0.17125533548810687, 'fmeasure': 0.19554567950133162}
Rouge_2: {'precision': 0.08657362285411065, 'recall': 0.051457067393728906, 'fmeasure': 0.06056609847736909}
Rouge_L: {'precision': 0.24435094038752572, 'recall': 0.15511338436238664, 'fmeasure': 0.1747658432908067}

4 0 tensor(3.7397, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.7431, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(3.1506, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.7922, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.6737, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 10 3.3407277334027174

ROUGE Scores: (val)
Rouge_1: {'precision': 0.28226394558230233, 'recall': 0.17143354516392156, 'fmeasure': 0.1956746410876198}
Rouge_2: {'precision': 0.07954558991144357, 'recall': 0.049722727408756766, 'fmeasure': 0.05752778664389277}
Rouge_L: {'precision': 0.24026216081735974, 'recall': 0.15285142911749672, 'fmeasure': 0.17134078117835586}

5 0 tensor(4.5440, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.7055, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.9043, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.2294, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(2.6771, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 10 3.3268854632610227

ROUGE Scores: (val)
Rouge_1: {'precision': 0.27413600965430224, 'recall': 0.16994278488748307, 'fmeasure': 0.1921954564739594}
Rouge_2: {'precision': 0.077632733120538, 'recall': 0.046968900444548926, 'fmeasure': 0.05449342189589725}
Rouge_L: {'precision': 0.23505618703789427, 'recall': 0.15183404095087094, 'fmeasure': 0.16897659520692546}

6 0 tensor(2.2399, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(2.9997, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(2.4683, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(2.9619, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.6708, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 10 3.333145212836382

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2811396468588001, 'recall': 0.17595700637440106, 'fmeasure': 0.19979020383181223}
Rouge_2: {'precision': 0.08054601225332932, 'recall': 0.04639050035276768, 'fmeasure': 0.0555117664036202}
Rouge_L: {'precision': 0.24030003453425244, 'recall': 0.15654502241196408, 'fmeasure': 0.1748561852693844}

7 0 tensor(3.4052, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(4.3013, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(2.5344, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(2.7113, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(2.8174, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 10 3.3438280791771122

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2671665732641341, 'recall': 0.159439233846656, 'fmeasure': 0.18321084778939728}
Rouge_2: {'precision': 0.06899056228324521, 'recall': 0.03876355315251023, 'fmeasure': 0.04674428503257476}
Rouge_L: {'precision': 0.2253156058034106, 'recall': 0.1415798532500444, 'fmeasure': 0.15957672463736117}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.2644245934943609, 'recall': 0.17729566493122553, 'fmeasure': 0.19931269370915022}
Rouge_2: {'precision': 0.0797375750864123, 'recall': 0.05109117866415887, 'fmeasure': 0.05835706495789995}
Rouge_L: {'precision': 0.23889311592799964, 'recall': 0.16263480319273044, 'fmeasure': 0.1817940894530569}

1 0 tensor(5.7015, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(2.1714, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.3320, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(4.7335, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.6309, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.459055819753873

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2644779420203149, 'recall': 0.15976548678008584, 'fmeasure': 0.18067657736453124}
Rouge_2: {'precision': 0.06983938848345628, 'recall': 0.040035802163218545, 'fmeasure': 0.04778491847861217}
Rouge_L: {'precision': 0.2305001966018915, 'recall': 0.14524439820465207, 'fmeasure': 0.1615576546822783}

2 0 tensor(3.6940, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.7812, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.4432, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.4838, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.5918, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.3533626885737404

ROUGE Scores: (val)
Rouge_1: {'precision': 0.27207860407012946, 'recall': 0.17211387711209566, 'fmeasure': 0.18913436690627855}
Rouge_2: {'precision': 0.08301293809768387, 'recall': 0.04639329365963217, 'fmeasure': 0.05517805209806684}
Rouge_L: {'precision': 0.23524451066823954, 'recall': 0.15693191849826896, 'fmeasure': 0.16866304322305142}

3 0 tensor(2.2950, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.6669, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.4540, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.7610, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.8331, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.291533366098242

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2652445012614503, 'recall': 0.16132158854511203, 'fmeasure': 0.18040108797284907}
Rouge_2: {'precision': 0.0699154423730695, 'recall': 0.0428381022286117, 'fmeasure': 0.04959347060207709}
Rouge_L: {'precision': 0.22845229158788477, 'recall': 0.1451823336827926, 'fmeasure': 0.15880367393250502}

4 0 tensor(2.5913, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(1.8623, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.6290, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.8397, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(1.5367, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 20 3.2792160470606917

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2767639583192922, 'recall': 0.1620182392158923, 'fmeasure': 0.1837875218043773}
Rouge_2: {'precision': 0.07353498667057988, 'recall': 0.04139357218238674, 'fmeasure': 0.05010224661770479}
Rouge_L: {'precision': 0.2384810547821514, 'recall': 0.14714097640847876, 'fmeasure': 0.163075475103135}

5 0 tensor(2.6174, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.0704, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(4.2045, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.2807, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.9804, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 20 3.2676939080327245

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2569323332035196, 'recall': 0.15446255049513913, 'fmeasure': 0.17243498625748546}
Rouge_2: {'precision': 0.06323337679269883, 'recall': 0.03843818039349545, 'fmeasure': 0.04454150341900775}
Rouge_L: {'precision': 0.2204917022713633, 'recall': 0.1382797273466109, 'fmeasure': 0.15116376840312665}

6 0 tensor(3.3005, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(2.0282, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(4.0104, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.9912, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.6637, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 20 3.259963246220249

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2660369762064677, 'recall': 0.16066967670990917, 'fmeasure': 0.18134227457587487}
Rouge_2: {'precision': 0.06327373191779971, 'recall': 0.036958492473129566, 'fmeasure': 0.04400550457792417}
Rouge_L: {'precision': 0.22672501697925423, 'recall': 0.1440552567352789, 'fmeasure': 0.15921686525374676}

7 0 tensor(2.1903, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(2.4815, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(3.3230, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(2.0751, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(3.1412, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 20 3.2549227743835774

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2487350995077814, 'recall': 0.14988746661974164, 'fmeasure': 0.167911480897698}
Rouge_2: {'precision': 0.05544718657879175, 'recall': 0.036384595170212, 'fmeasure': 0.041288718029013315}
Rouge_L: {'precision': 0.21884813148522017, 'recall': 0.13793790304948603, 'fmeasure': 0.15158520125587008}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.17194698305809422, 'recall': 0.13769660489646732, 'fmeasure': 0.1422100031605822}
Rouge_2: {'precision': 0.03013200790978569, 'recall': 0.03389182833627278, 'fmeasure': 0.02890652557319224}
Rouge_L: {'precision': 0.15270829993052218, 'recall': 0.1256121581714977, 'fmeasure': 0.12832243802417337}

avg_f1_1: 0.19825544151665653

avg_f1_2: 0.05785957962157093

avg_f1_L: 0.17583850313438854
