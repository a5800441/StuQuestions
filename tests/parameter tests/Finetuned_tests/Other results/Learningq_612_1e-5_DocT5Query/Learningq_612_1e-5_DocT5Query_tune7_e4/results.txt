{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_612_1e-5_DocT5Query', 'EPOCH_SETTING': '5', 'NAME': 'Learningq_612_1e-5_DocT5Query_tune7_e4', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 7, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(3.9349, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.8587, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(2.9316, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(2.8495, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(5.2582, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.365846528845318

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22788539144471345, 'recall': 0.15889309914980038, 'fmeasure': 0.17447481221382447}
Rouge_2: {'precision': 0.03491593152610102, 'recall': 0.025908980330042082, 'fmeasure': 0.02775843336480391}
Rouge_L: {'precision': 0.21220742534301856, 'recall': 0.15280744877636124, 'fmeasure': 0.16576920635553405}

2 0 tensor(3.2542, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.7891, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.8703, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.2876, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.1308, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.333711345317

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2523569650688295, 'recall': 0.1361703097622684, 'fmeasure': 0.16427348981679832}
Rouge_2: {'precision': 0.06349878934624698, 'recall': 0.027556366214716105, 'fmeasure': 0.03613787309876462}
Rouge_L: {'precision': 0.23328916845865993, 'recall': 0.13120019390416435, 'fmeasure': 0.1564955618038856}

3 0 tensor(1.9759, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.3720, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.1386, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.3546, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.1482, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.4467007194535206

ROUGE Scores: (val)
Rouge_1: {'precision': 0.25851008689991745, 'recall': 0.16928921282724285, 'fmeasure': 0.19162594201321914}
Rouge_2: {'precision': 0.053033125067023366, 'recall': 0.032420708403083226, 'fmeasure': 0.037727145711959474}
Rouge_L: {'precision': 0.23977331519704398, 'recall': 0.15940131455504097, 'fmeasure': 0.17920130482472}

4 0 tensor(1.8839, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.1943, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.8952, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(1.5385, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.5284, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.6543877831960128

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2742559202180339, 'recall': 0.1780301636757526, 'fmeasure': 0.20379247391902347}
Rouge_2: {'precision': 0.06530047165640385, 'recall': 0.04311392043579542, 'fmeasure': 0.0490518788707347}
Rouge_L: {'precision': 0.25067288145553357, 'recall': 0.165584724074823, 'fmeasure': 0.18865032489283196}

5 0 tensor(2.1107, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.3613, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.8295, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.1952, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.4604, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 3.8884928266880876

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24225534105105914, 'recall': 0.17321424294572635, 'fmeasure': 0.18840652651412218}
Rouge_2: {'precision': 0.05230424001610442, 'recall': 0.035595420046992544, 'fmeasure': 0.04020819214759862}
Rouge_L: {'precision': 0.20848280473614997, 'recall': 0.15179151763206525, 'fmeasure': 0.16424018973956175}

6 0 tensor(0.4348, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.4293, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.8558, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.1975, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.3848, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 0 4.105289348101212

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20244731727782572, 'recall': 0.13835326987302984, 'fmeasure': 0.15352802503238966}
Rouge_2: {'precision': 0.029579883393442714, 'recall': 0.025149641140341805, 'fmeasure': 0.02541277240403546}
Rouge_L: {'precision': 0.18586912146234175, 'recall': 0.13059706782903896, 'fmeasure': 0.14310085664391636}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.17851685462400696, 'recall': 0.1397770964523348, 'fmeasure': 0.14049851877351194}
Rouge_2: {'precision': 0.02262326217376162, 'recall': 0.021925176808639625, 'fmeasure': 0.01816075952270047}
Rouge_L: {'precision': 0.1482076920768095, 'recall': 0.11586359163404279, 'fmeasure': 0.1173842862791653}

1 0 tensor(3.5749, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.6230, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.1829, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(2.7020, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.4567, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.758543414313619

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2378290408778214, 'recall': 0.1364447626515664, 'fmeasure': 0.16219717199423864}
Rouge_2: {'precision': 0.05122919086333721, 'recall': 0.032424170522977434, 'fmeasure': 0.037581761175164395}
Rouge_L: {'precision': 0.21133236682017165, 'recall': 0.12700967709071126, 'fmeasure': 0.14877927128852564}

2 0 tensor(1.7109, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.8308, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.3353, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.1885, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.4934, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.7437573976633027

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2578546791961426, 'recall': 0.14134787646435718, 'fmeasure': 0.17077569727332156}
Rouge_2: {'precision': 0.053684188440285996, 'recall': 0.031656595381204605, 'fmeasure': 0.03744073353106712}
Rouge_L: {'precision': 0.230449530144652, 'recall': 0.13096957363077294, 'fmeasure': 0.15639775728985184}

3 0 tensor(2.3838, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(1.5851, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.0949, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.6529, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.3397, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.753065658778679

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23894573113169726, 'recall': 0.15326522784373245, 'fmeasure': 0.17418896205859605}
Rouge_2: {'precision': 0.050696830270001006, 'recall': 0.03703358151689345, 'fmeasure': 0.03983846602739231}
Rouge_L: {'precision': 0.2139776217661704, 'recall': 0.14303162362892027, 'fmeasure': 0.16030073055000685}

4 0 tensor(2.7493, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.1732, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.2137, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.8642, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.5198, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 10 3.5605891027101655

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24144304205279812, 'recall': 0.14495665567835525, 'fmeasure': 0.16828061106349054}
Rouge_2: {'precision': 0.058331675099967784, 'recall': 0.037327543057027866, 'fmeasure': 0.04243384680109801}
Rouge_L: {'precision': 0.21219458048726333, 'recall': 0.13211438469409326, 'fmeasure': 0.15177008285074878}

5 0 tensor(2.8315, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.1143, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.5804, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.2067, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.7551, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 10 3.7750391029730075

ROUGE Scores: (val)
Rouge_1: {'precision': 0.23266858480273117, 'recall': 0.1117863177303894, 'fmeasure': 0.14064406335364354}
Rouge_2: {'precision': 0.052426706085242664, 'recall': 0.023869157864664925, 'fmeasure': 0.030896736082829418}
Rouge_L: {'precision': 0.20717409284482458, 'recall': 0.10418783594329573, 'fmeasure': 0.12965591688363196}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.25007952603711836, 'recall': 0.15510497416837335, 'fmeasure': 0.18243713910420892}
Rouge_2: {'precision': 0.05719990603711533, 'recall': 0.03781397571084123, 'fmeasure': 0.043331254042608354}
Rouge_L: {'precision': 0.20761978939133519, 'recall': 0.13080270219852272, 'fmeasure': 0.15338901778396763}

1 0 tensor(5.7015, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(2.0411, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.2660, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(4.7495, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(3.8304, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.544906531350087

ROUGE Scores: (val)
Rouge_1: {'precision': 0.25716562157240114, 'recall': 0.13408888107744057, 'fmeasure': 0.15842875077380464}
Rouge_2: {'precision': 0.046240247511433956, 'recall': 0.025387351494784712, 'fmeasure': 0.03108216402334049}
Rouge_L: {'precision': 0.23152097431758442, 'recall': 0.12629798031912495, 'fmeasure': 0.14682628051582272}

2 0 tensor(3.6085, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.2335, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.2432, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.2227, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.8228, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.2031968044022383

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2658185975982586, 'recall': 0.12756942784881556, 'fmeasure': 0.1575039717461872}
Rouge_2: {'precision': 0.058855221990815214, 'recall': 0.028449144744166672, 'fmeasure': 0.035429147524218746}
Rouge_L: {'precision': 0.23176437498471397, 'recall': 0.11654495619427757, 'fmeasure': 0.14179496846086181}

3 0 tensor(1.9490, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.7361, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.1747, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.4138, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(0.8887, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.2609060694605616

ROUGE Scores: (val)
Rouge_1: {'precision': 0.24450199047656668, 'recall': 0.13758423171511677, 'fmeasure': 0.16394837237688303}
Rouge_2: {'precision': 0.0681189337968999, 'recall': 0.03678111021083785, 'fmeasure': 0.044078030264653595}
Rouge_L: {'precision': 0.20388883715154899, 'recall': 0.12003502226328674, 'fmeasure': 0.1409479062695315}

4 0 tensor(1.5703, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(0.6663, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.2299, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.5217, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(0.4884, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 20 3.5003201032088973

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22745741989261425, 'recall': 0.12565991539578705, 'fmeasure': 0.14999565750325689}
Rouge_2: {'precision': 0.045240461788187046, 'recall': 0.026696292061377802, 'fmeasure': 0.03117782868005883}
Rouge_L: {'precision': 0.19077717452592818, 'recall': 0.11047053337787906, 'fmeasure': 0.13000056923576386}

5 0 tensor(1.7437, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(0.3553, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.5071, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.9980, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.1260, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 20 3.704064127247212

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2296525978729368, 'recall': 0.13035183851009163, 'fmeasure': 0.14949964242309402}
Rouge_2: {'precision': 0.03895283845433396, 'recall': 0.018054324745432424, 'fmeasure': 0.02315740232055796}
Rouge_L: {'precision': 0.19537298859332752, 'recall': 0.11893260490817556, 'fmeasure': 0.1330657693805845}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.16897692298019096, 'recall': 0.14365203975197097, 'fmeasure': 0.1477533867887057}
Rouge_2: {'precision': 0.02254597254597255, 'recall': 0.025102880658436216, 'fmeasure': 0.02314000814000814}
Rouge_L: {'precision': 0.1595633427332774, 'recall': 0.13471453081446205, 'fmeasure': 0.13974127008093684}

avg_f1_1: 0.18064326945150086

avg_f1_2: 0.045187918645495435

avg_f1_L: 0.16525911198622048
