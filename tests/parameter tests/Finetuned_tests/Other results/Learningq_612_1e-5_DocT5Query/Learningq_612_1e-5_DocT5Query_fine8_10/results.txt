{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_612_1e-5_DocT5Query', 'EPOCH_SETTING': '10', 'NAME': 'Learningq_612_1e-5_DocT5Query_fine8_10', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 8, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 1e-05, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(3.2323, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.0721, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.1008, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(4.0897, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.4485, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(3.5265, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.2444, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(5.0519, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.2161, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(4.0512, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(3.1405, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.6104, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(5.3781, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.5922, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.5857, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(2.9709, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.6193, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(3.7853, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.8624, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.2195, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(2.9160, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(3.5701, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.5491, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.2063, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(4.1473, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(2.7295, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.6803, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(3.0767, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.8537, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(3.6223, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(2.6197, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(3.1734, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(3.7209, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(3.0825, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(2.2919, device='cuda:0', grad_fn=<NllLossBackward0>)

8 0 tensor(4.1961, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(3.8198, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(3.0194, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(3.0069, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.8579, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.25582278277894255, 'recall': 0.16619460822754548, 'fmeasure': 0.187998152719182}
Rouge_2: {'precision': 0.06332121756650058, 'recall': 0.0399746142225259, 'fmeasure': 0.04656923877830314}
Rouge_L: {'precision': 0.22437311886534966, 'recall': 0.14945131789078867, 'fmeasure': 0.16729376244818658}

BLEU Scores: (test)
{'bleu-1': 0.09456688780866367, 'bleu-2': 0.018403713351703467, 'bleu-3': 0.0001559423473788553, 'bleu-4': 1.0425227657679195e-308, 'bleu': 1.2367147583180957e-80, 'bleu-corp-1': 0.09456688780866367, 'bleu-corp-2': 0.018403713351703467, 'bleu-corp-3': 0.0001559423473788553, 'bleu-corp-4': 1.0425227657679195e-308, 'bleu-corp': 1.2367147583180957e-80}

METEOR Scores: (test)
{'meteor': 0.1369615374960988}

GLEU Scores: (test)
{'gleu-1': 0.11384393917688237, 'gleu-2': 0.020787675657265, 'gleu-3': 0.000725689404934688, 'gleu-4': 0.0, 'gleu': 0.03778643682285366, 'gleu-corp-1': 0.11384393917688237, 'gleu-corp-2': 0.020787675657265, 'gleu-corp-3': 0.000725689404934688, 'gleu-corp-4': 0.0, 'gleu-corp': 0.03778643682285366}
