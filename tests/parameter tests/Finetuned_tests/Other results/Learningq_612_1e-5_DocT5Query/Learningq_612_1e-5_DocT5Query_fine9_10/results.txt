{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_612_1e-5_DocT5Query', 'EPOCH_SETTING': '10', 'NAME': 'Learningq_612_1e-5_DocT5Query_fine9_10', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 9, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 1e-05, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(3.2323, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.0721, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.1008, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(4.0897, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.4485, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(3.5265, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.2444, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(5.0519, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.2161, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(4.0512, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(3.1405, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.6104, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(5.3781, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.5922, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.5857, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(2.9709, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.6193, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(3.7853, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.8624, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.2195, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(2.9160, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(3.5701, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.5491, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.2063, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(4.1473, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(2.7295, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.6803, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(3.0767, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.8537, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(3.6223, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(2.6197, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(3.1734, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(3.7209, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(3.0825, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(2.2919, device='cuda:0', grad_fn=<NllLossBackward0>)

8 0 tensor(4.1961, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(3.8198, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(3.0194, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(3.0069, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.8579, device='cuda:0', grad_fn=<NllLossBackward0>)

9 0 tensor(2.0193, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(3.0711, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(1.8170, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(1.1137, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(2.4596, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.252638500973684, 'recall': 0.16182565433028967, 'fmeasure': 0.1843926944828534}
Rouge_2: {'precision': 0.060748318767186685, 'recall': 0.037500291159967544, 'fmeasure': 0.044077146663569516}
Rouge_L: {'precision': 0.22163807335838412, 'recall': 0.14567794241930002, 'fmeasure': 0.16415320428115762}

BLEU Scores: (test)
{'bleu-1': 0.08719440587121853, 'bleu-2': 0.012744643170095958, 'bleu-3': 0.00016578436359479675, 'bleu-4': 1.0849706417485595e-308, 'bleu': 1.6948448303015335e-80, 'bleu-corp-1': 0.08719440587121853, 'bleu-corp-2': 0.012744643170095958, 'bleu-corp-3': 0.00016578436359479675, 'bleu-corp-4': 1.0849706417485595e-308, 'bleu-corp': 1.6948448303015335e-80}

METEOR Scores: (test)
{'meteor': 0.1409282288658958}

GLEU Scores: (test)
{'gleu-1': 0.10572007583951358, 'gleu-2': 0.01476308457440533, 'gleu-3': 0.000725689404934688, 'gleu-4': 0.0, 'gleu': 0.03412350475218859, 'gleu-corp-1': 0.10572007583951358, 'gleu-corp-2': 0.01476308457440533, 'gleu-corp-3': 0.000725689404934688, 'gleu-corp-4': 0.0, 'gleu-corp': 0.03412350475218859}
