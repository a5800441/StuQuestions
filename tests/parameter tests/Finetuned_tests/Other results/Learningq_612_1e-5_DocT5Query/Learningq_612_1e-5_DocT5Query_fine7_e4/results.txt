{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_612_1e-5_DocT5Query', 'EPOCH_SETTING': '5', 'NAME': 'Learningq_612_1e-5_DocT5Query_fine7_e4', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 7, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(3.2749, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(2.6028, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(2.7647, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.3172, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.2349, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.2005, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.6287, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.4434, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.6950, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.7353, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(2.2771, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.6750, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.1951, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.8324, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.4145, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(2.0916, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.8857, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.6318, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.3611, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.5851, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(1.2041, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.0844, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.9711, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.8942, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(2.4240, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(1.5200, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.7796, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.4418, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.3334, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.7324, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(1.2778, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(2.0729, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.8742, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.9544, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(0.7262, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.17371336734544282, 'recall': 0.11098446773060683, 'fmeasure': 0.12737878774409492}
Rouge_2: {'precision': 0.012908475172626117, 'recall': 0.011265585347015335, 'fmeasure': 0.010709528204746013}
Rouge_L: {'precision': 0.1485411600977639, 'recall': 0.09818736518561878, 'fmeasure': 0.1113596548077296}

BLEU Scores: (test)
{'bleu-1': 0.05797917978198782, 'bleu-2': 0.0038217252602406574, 'bleu-3': 1.290079572705946e-309, 'bleu-4': 8.61507456233119e-309, 'bleu': 2.4772141465113375e-156, 'bleu-corp-1': 0.05797917978198782, 'bleu-corp-2': 0.0038217252602406574, 'bleu-corp-3': 1.290079572705946e-309, 'bleu-corp-4': 8.61507456233119e-309, 'bleu-corp': 2.4772141465113375e-156}

METEOR Scores: (test)
{'meteor': 0.09552475416059857}

GLEU Scores: (test)
{'gleu-1': 0.07032023655825903, 'gleu-2': 0.004230989136649514, 'gleu-3': 0.0, 'gleu-4': 0.0, 'gleu': 0.020607871497534236, 'gleu-corp-1': 0.07032023655825903, 'gleu-corp-2': 0.004230989136649514, 'gleu-corp-3': 0.0, 'gleu-corp-4': 0.0, 'gleu-corp': 0.020607871497534236}
