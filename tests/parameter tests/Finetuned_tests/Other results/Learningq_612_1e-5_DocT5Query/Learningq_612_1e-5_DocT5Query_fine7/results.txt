{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_612_1e-5_DocT5Query', 'EPOCH_SETTING': '5', 'NAME': 'Learningq_612_1e-5_DocT5Query_fine7', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 7, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 1e-05, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(3.2749, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(2.9693, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.0434, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(4.0415, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.4402, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(3.2805, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.1798, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.9821, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.2443, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(4.1384, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(3.3388, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.5513, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(4.5151, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.5540, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.3461, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(3.0495, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.4796, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(3.6998, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.7458, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.1125, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(2.9124, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(3.5984, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.5671, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.1868, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(4.3019, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(2.7210, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.7151, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(2.9691, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(2.0405, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(3.6252, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(2.6254, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(3.1420, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(3.6731, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(3.2307, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(2.1912, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.24868776815152188, 'recall': 0.16502319283319067, 'fmeasure': 0.18269501298121293}
Rouge_2: {'precision': 0.05826767572050592, 'recall': 0.04193539853625138, 'fmeasure': 0.04461462852320837}
Rouge_L: {'precision': 0.21284785775848336, 'recall': 0.14545478507350118, 'fmeasure': 0.159511371189262}

BLEU Scores: (test)
{'bleu-1': 0.0891153027307917, 'bleu-2': 0.016316884952265687, 'bleu-3': 0.001279033093111111, 'bleu-4': 0.0017152658662092622, 'bleu': 0.003546852386289639, 'bleu-corp-1': 0.0891153027307917, 'bleu-corp-2': 0.016316884952265687, 'bleu-corp-3': 0.001279033093111111, 'bleu-corp-4': 0.0017152658662092622, 'bleu-corp': 0.003546852386289639}

METEOR Scores: (test)
{'meteor': 0.14562829188743248}

GLEU Scores: (test)
{'gleu-1': 0.10580698837940239, 'gleu-2': 0.0185407010878709, 'gleu-3': 0.0038703434929850023, 'gleu-4': 0.0017152658662092624, 'gleu': 0.035987002275896296, 'gleu-corp-1': 0.10580698837940239, 'gleu-corp-2': 0.0185407010878709, 'gleu-corp-3': 0.0038703434929850023, 'gleu-corp-4': 0.0017152658662092624, 'gleu-corp': 0.035987002275896296}
