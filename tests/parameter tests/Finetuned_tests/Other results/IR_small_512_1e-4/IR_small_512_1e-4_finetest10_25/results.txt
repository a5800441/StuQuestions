{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_512_1e-4', 'EPOCH_SETTING': '25', 'NAME': 'IR_small_512_1e-4_finetest10_25', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 10, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(5.7098, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.3244, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.5677, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.8366, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.4280, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.2030, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.9606, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.2031, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.8470, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.8866, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(2.4255, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.0910, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(1.4418, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.2389, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.5860, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(1.8721, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.0024, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.7091, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.5850, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.3388, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(0.8607, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.2690, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.2361, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.2129, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.0402, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(1.3424, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.6909, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.5847, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.3490, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.9670, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(0.9353, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.7098, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.8204, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.6895, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.7198, device='cuda:0', grad_fn=<NllLossBackward0>)

8 0 tensor(2.0228, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(2.7416, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(0.9607, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(1.2653, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.3658, device='cuda:0', grad_fn=<NllLossBackward0>)

9 0 tensor(0.2866, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(0.5196, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(0.3921, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(0.0572, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(0.7629, device='cuda:0', grad_fn=<NllLossBackward0>)

10 0 tensor(0.3909, device='cuda:0', grad_fn=<NllLossBackward0>)

10 100 tensor(0.4078, device='cuda:0', grad_fn=<NllLossBackward0>)

10 200 tensor(0.9547, device='cuda:0', grad_fn=<NllLossBackward0>)

10 300 tensor(0.7735, device='cuda:0', grad_fn=<NllLossBackward0>)

10 400 tensor(0.1811, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.1600193631131478, 'recall': 0.11086461176925291, 'fmeasure': 0.11901485492405327}
Rouge_2: {'precision': 0.024326755375590002, 'recall': 0.02300266018357028, 'fmeasure': 0.022056800054233168}
Rouge_L: {'precision': 0.14205761866694164, 'recall': 0.09662284724446953, 'fmeasure': 0.10452078524560801}

BLEU Scores: (test)
{'bleu-1': 0.06744760614731574, 'bleu-2': 0.011634595632962659, 'bleu-3': 0.0016172506738544475, 'bleu-4': 8.735799300922703e-309, 'bleu': 9.474214806736465e-80, 'bleu-corp-1': 0.06744760614731574, 'bleu-corp-2': 0.011634595632962659, 'bleu-corp-3': 0.0016172506738544475, 'bleu-corp-4': 8.735799300922703e-309, 'bleu-corp': 9.474214806736465e-80}

METEOR Scores: (test)
{'meteor': 0.09960548722953708}

GLEU Scores: (test)
{'gleu-1': 0.07755512003806898, 'gleu-2': 0.012243746071239334, 'gleu-3': 0.0037735849056603774, 'gleu-4': 0.0, 'gleu': 0.026082343900969925, 'gleu-corp-1': 0.07755512003806898, 'gleu-corp-2': 0.012243746071239334, 'gleu-corp-3': 0.0037735849056603774, 'gleu-corp-4': 0.0, 'gleu-corp': 0.026082343900969925}
