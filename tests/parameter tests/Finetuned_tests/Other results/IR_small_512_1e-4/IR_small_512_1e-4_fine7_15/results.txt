{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_512_1e-4', 'EPOCH_SETTING': '15', 'NAME': 'IR_small_512_1e-4_fine7_15', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 7, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(4.5380, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.3021, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.5964, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.1489, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(5.0349, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.6306839979301064

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1909359817300469, 'recall': 0.14183612040475657, 'fmeasure': 0.14564451066370504}
Rouge_2: {'precision': 0.01904425612052731, 'recall': 0.012567128364011908, 'fmeasure': 0.01349597929857152}
Rouge_L: {'precision': 0.1692387559004812, 'recall': 0.12396051847145931, 'fmeasure': 0.12744024862882816}

2 0 tensor(3.3190, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.1049, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.9983, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.7640, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.5038, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.539086351960392

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18808104767725903, 'recall': 0.11081910772378273, 'fmeasure': 0.1272728154245536}
Rouge_2: {'precision': 0.03456477609019982, 'recall': 0.009851452613090467, 'fmeasure': 0.01376083380258299}
Rouge_L: {'precision': 0.16193226016256926, 'recall': 0.09162950979540115, 'fmeasure': 0.10701564132214693}

3 0 tensor(1.8760, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.6072, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.6582, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.3027, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.0574, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.7026512461193537

ROUGE Scores: (val)
Rouge_1: {'precision': 0.20582705893221714, 'recall': 0.14471023938742927, 'fmeasure': 0.15706352626353046}
Rouge_2: {'precision': 0.019089650794536138, 'recall': 0.014894929086028555, 'fmeasure': 0.015312924458485488}
Rouge_L: {'precision': 0.1700018541664656, 'recall': 0.12275100171979614, 'fmeasure': 0.13159833584508962}

4 0 tensor(2.1163, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.2997, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.7638, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(1.6972, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.4089, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.9043099314479504

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1803831609290233, 'recall': 0.1315141646010766, 'fmeasure': 0.13800735157064856}
Rouge_2: {'precision': 0.01090933548560667, 'recall': 0.005780393373654779, 'fmeasure': 0.007474303654312018}
Rouge_L: {'precision': 0.15210567376569364, 'recall': 0.10965405093198768, 'fmeasure': 0.11509116149956829}

5 0 tensor(2.1733, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.8000, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.6127, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(0.9159, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.6377, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 4.162592170602184

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19114758404714613, 'recall': 0.14439146838446648, 'fmeasure': 0.1490715408934458}
Rouge_2: {'precision': 0.026578662194184047, 'recall': 0.021209737477239317, 'fmeasure': 0.020890801517837464}
Rouge_L: {'precision': 0.1562938313967834, 'recall': 0.11910411228205552, 'fmeasure': 0.12167761002520106}

6 0 tensor(0.1743, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.2028, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(1.0735, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.4391, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.3608, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 0 4.523274587372602

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2103552122159344, 'recall': 0.13140434369652892, 'fmeasure': 0.14424166080125972}
Rouge_2: {'precision': 0.01795932927275741, 'recall': 0.015673247592227494, 'fmeasure': 0.01525035201076044}
Rouge_L: {'precision': 0.17317302876440677, 'recall': 0.10624558937526213, 'fmeasure': 0.11722225642470996}

7 0 tensor(0.8540, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.1403, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(0.4983, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(1.3077, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.3998, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 0 4.783318168025906

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18799326155966192, 'recall': 0.1491156741064797, 'fmeasure': 0.1516987498129626}
Rouge_2: {'precision': 0.01922982855186245, 'recall': 0.016522945782535433, 'fmeasure': 0.017029277824261294}
Rouge_L: {'precision': 0.14934830287023138, 'recall': 0.12126617635852514, 'fmeasure': 0.12140679185821522}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.1564882563203143, 'recall': 0.13323348627162854, 'fmeasure': 0.13189916295163154}
Rouge_2: {'precision': 0.010901064344460568, 'recall': 0.01203421924663556, 'fmeasure': 0.00993480912618379}
Rouge_L: {'precision': 0.12839842357191505, 'recall': 0.1081796037340114, 'fmeasure': 0.10708775490703097}

1 0 tensor(4.3629, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(5.1635, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(4.9133, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(2.9610, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.6356, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.449185726119251

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18603316349805063, 'recall': 0.14509865044105036, 'fmeasure': 0.1472805812075585}
Rouge_2: {'precision': 0.029983502535511145, 'recall': 0.026152180177645635, 'fmeasure': 0.02334947198093336}
Rouge_L: {'precision': 0.14767845341289804, 'recall': 0.12108895670388665, 'fmeasure': 0.11997678703022333}

2 0 tensor(1.2537, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.7278, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.5590, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.5374, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(1.1988, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.899097724658687

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1952059550415983, 'recall': 0.10949635550166244, 'fmeasure': 0.1251354009352118}
Rouge_2: {'precision': 0.012712399190161027, 'recall': 0.0070742521022573186, 'fmeasure': 0.008234376377423226}
Rouge_L: {'precision': 0.16334273399881893, 'recall': 0.09018416958836145, 'fmeasure': 0.10296873152976255}

3 0 tensor(2.4334, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.2589, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.2069, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.4948, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(0.9429, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.5684742113439047

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21525494267077303, 'recall': 0.13236555816372098, 'fmeasure': 0.14899357710918837}
Rouge_2: {'precision': 0.025439634890854403, 'recall': 0.011623520134039387, 'fmeasure': 0.01489881614826106}
Rouge_L: {'precision': 0.17046767523513756, 'recall': 0.10641090902303671, 'fmeasure': 0.11935961508145937}

4 0 tensor(1.9350, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(1.5238, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.3828, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.5867, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.3492, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 10 3.8447421920008775

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1873468310068624, 'recall': 0.13319466714699574, 'fmeasure': 0.1415076331754367}
Rouge_2: {'precision': 0.02294165840103902, 'recall': 0.01445666420869071, 'fmeasure': 0.01636215802866112}
Rouge_L: {'precision': 0.1583482126728689, 'recall': 0.11238735727403326, 'fmeasure': 0.11874083609903487}

5 0 tensor(2.7587, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.2532, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.4602, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(0.7762, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.5836, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 10 4.1077291282211865

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2199894776831649, 'recall': 0.1267286403844412, 'fmeasure': 0.14523411745000236}
Rouge_2: {'precision': 0.03938413889633401, 'recall': 0.016764719748453564, 'fmeasure': 0.021390958459260558}
Rouge_L: {'precision': 0.1876400540317325, 'recall': 0.10898680523962025, 'fmeasure': 0.12399283075580947}

6 0 tensor(0.4895, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.8596, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(1.1395, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.9885, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(0.7877, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 10 4.3536641612285525

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18735002086153496, 'recall': 0.1628769061831128, 'fmeasure': 0.1557155888291724}
Rouge_2: {'precision': 0.018378621623635558, 'recall': 0.017656340139146638, 'fmeasure': 0.016034324718298937}
Rouge_L: {'precision': 0.14368053635028344, 'recall': 0.12854056268657177, 'fmeasure': 0.12094929465244081}

7 0 tensor(0.7822, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.3723, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(0.5350, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.1079, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.3792, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 7 10 4.684194749448357

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21019048860579329, 'recall': 0.1435546174338638, 'fmeasure': 0.15655560241469738}
Rouge_2: {'precision': 0.026444097868336756, 'recall': 0.019158615461029857, 'fmeasure': 0.021048861497943137}
Rouge_L: {'precision': 0.17467490114900452, 'recall': 0.12212572736008522, 'fmeasure': 0.1322037048879946}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.21672823377427164, 'recall': 0.14433897213014152, 'fmeasure': 0.15497817439265296}
Rouge_2: {'precision': 0.04306739771856051, 'recall': 0.025289138653077463, 'fmeasure': 0.02918838512081805}
Rouge_L: {'precision': 0.1791789076534605, 'recall': 0.11529570741998216, 'fmeasure': 0.1260243985940865}

1 0 tensor(5.9169, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(2.1465, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.1357, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(5.1044, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.6086, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.317833410481275

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21023561972360316, 'recall': 0.11973081676683447, 'fmeasure': 0.13429550960238046}
Rouge_2: {'precision': 0.031853155947178226, 'recall': 0.013661696000583964, 'fmeasure': 0.017137225270976192}
Rouge_L: {'precision': 0.1918869746131382, 'recall': 0.10472607721859521, 'fmeasure': 0.1187145861347722}

2 0 tensor(3.5435, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.0343, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.3969, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.2835, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.0663, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.323557287959729

ROUGE Scores: (val)
Rouge_1: {'precision': 0.205922798753346, 'recall': 0.1265893418084529, 'fmeasure': 0.13718529252289577}
Rouge_2: {'precision': 0.026527410774669, 'recall': 0.011181090019357826, 'fmeasure': 0.014415536383503093}
Rouge_L: {'precision': 0.18301615491818338, 'recall': 0.11702912162057662, 'fmeasure': 0.12430085081296306}

3 0 tensor(1.4544, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.8861, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.1756, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.6890, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.5827, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.6244594485072765

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2035912907298749, 'recall': 0.09876070376291514, 'fmeasure': 0.1189553228510091}
Rouge_2: {'precision': 0.02421362286766076, 'recall': 0.01217241439311179, 'fmeasure': 0.012729460635741792}
Rouge_L: {'precision': 0.1767345081527534, 'recall': 0.08653109697802967, 'fmeasure': 0.1034886820646219}

4 0 tensor(1.7276, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(0.6509, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.8929, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.7458, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(0.4802, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 20 3.6527513841451227

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2266470332417228, 'recall': 0.13119542486018967, 'fmeasure': 0.14948743464864483}
Rouge_2: {'precision': 0.03436121732482649, 'recall': 0.01924962114261493, 'fmeasure': 0.022013449686995527}
Rouge_L: {'precision': 0.19695274945970226, 'recall': 0.11576917529648703, 'fmeasure': 0.13021443816509976}

5 0 tensor(1.2439, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(0.4485, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.5787, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.3859, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.9859, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 20 3.8030118209830786

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21970303891194537, 'recall': 0.12360518420378486, 'fmeasure': 0.14147870109598515}
Rouge_2: {'precision': 0.03388093295869966, 'recall': 0.015457822144717978, 'fmeasure': 0.01898646313391496}
Rouge_L: {'precision': 0.18096533625287745, 'recall': 0.10262400875597905, 'fmeasure': 0.11709662704670686}

6 0 tensor(1.7241, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.7259, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(1.8422, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.0810, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(1.4766, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 6 20 4.071937597404092

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1718085476473071, 'recall': 0.12223622877761801, 'fmeasure': 0.12573595888809155}
Rouge_2: {'precision': 0.01683102678865391, 'recall': 0.007803177455524762, 'fmeasure': 0.01012251935334434}
Rouge_L: {'precision': 0.139468870064948, 'recall': 0.09953556072855942, 'fmeasure': 0.10108608476283788}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.15003382075703758, 'recall': 0.12198783867600517, 'fmeasure': 0.11402126373888535}
Rouge_2: {'precision': 0.006672113289760349, 'recall': 0.01131687242798354, 'fmeasure': 0.006498951781970651}
Rouge_L: {'precision': 0.1305976526919977, 'recall': 0.10457325059148243, 'fmeasure': 0.09668530394678522}

avg_f1_1: 0.1543688544422909

avg_f1_2: 0.022084574395255453

avg_f1_L: 0.13133882629939467
