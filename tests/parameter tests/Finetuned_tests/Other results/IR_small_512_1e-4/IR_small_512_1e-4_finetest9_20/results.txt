{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_512_1e-4', 'EPOCH_SETTING': '20', 'NAME': 'IR_small_512_1e-4_finetest9_20', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 9, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(5.3219, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.2103, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.5256, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.8203, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.4247, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.7216, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.8075, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.4777, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.7443, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(4.1072, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(2.6662, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.2609, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.1673, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.3181, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.5523, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(1.8289, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.0265, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.8806, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.5213, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.1831, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(0.8767, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.0412, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.6172, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.0785, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.6690, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(1.2418, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.8440, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.5061, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.3724, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.1337, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(0.8654, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.8817, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.8412, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.6223, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.3020, device='cuda:0', grad_fn=<NllLossBackward0>)

8 0 tensor(1.7992, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(2.5985, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(1.0461, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(1.3892, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.3242, device='cuda:0', grad_fn=<NllLossBackward0>)

9 0 tensor(0.3591, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(0.7640, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(0.3043, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(0.3948, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(0.6616, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.15919311376295436, 'recall': 0.14268454808404868, 'fmeasure': 0.1324276350453112}
Rouge_2: {'precision': 0.01774227591375205, 'recall': 0.014410705420694324, 'fmeasure': 0.011460909587982767}
Rouge_L: {'precision': 0.12377541609497919, 'recall': 0.11621793996525588, 'fmeasure': 0.10389255098861383}

BLEU Scores: (test)
{'bleu-1': 0.07761868542924194, 'bleu-2': 0.0027096614887545437, 'bleu-3': 1.72707307880304e-309, 'bleu-4': 1.280809554888733e-308, 'bleu': 1.986336500265002e-156, 'bleu-corp-1': 0.07761868542924194, 'bleu-corp-2': 0.0027096614887545437, 'bleu-corp-3': 1.72707307880304e-309, 'bleu-corp-4': 1.280809554888733e-308, 'bleu-corp': 1.986336500265002e-156}

METEOR Scores: (test)
{'meteor': 0.1159531480883208}

GLEU Scores: (test)
{'gleu-1': 0.08725926458220015, 'gleu-2': 0.003260322835794534, 'gleu-3': 0.0, 'gleu-4': 0.0, 'gleu': 0.0246697663288166, 'gleu-corp-1': 0.08725926458220015, 'gleu-corp-2': 0.003260322835794534, 'gleu-corp-3': 0.0, 'gleu-corp-4': 0.0, 'gleu-corp': 0.0246697663288166}
