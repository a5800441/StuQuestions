{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_512_1e-4', 'EPOCH_SETTING': '20', 'NAME': 'IR_small_512_1e-4_finetest10_20', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 10, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(5.3219, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.2103, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.5256, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.8203, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.4247, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.7216, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.8075, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.4777, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.7443, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(4.1072, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(2.6662, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.2609, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.1673, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.3181, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(2.5523, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(1.8289, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.0265, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.8806, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.5213, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.1831, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(0.8767, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.0412, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.6172, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.0785, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.6690, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(1.2418, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(0.8440, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(0.5061, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.3724, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(2.1337, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(0.8654, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(1.8817, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(1.8412, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.6223, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.3020, device='cuda:0', grad_fn=<NllLossBackward0>)

8 0 tensor(1.7992, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(2.5985, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(1.0461, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(1.3892, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(0.3242, device='cuda:0', grad_fn=<NllLossBackward0>)

9 0 tensor(0.3591, device='cuda:0', grad_fn=<NllLossBackward0>)

9 100 tensor(0.7640, device='cuda:0', grad_fn=<NllLossBackward0>)

9 200 tensor(0.3043, device='cuda:0', grad_fn=<NllLossBackward0>)

9 300 tensor(0.3948, device='cuda:0', grad_fn=<NllLossBackward0>)

9 400 tensor(0.6616, device='cuda:0', grad_fn=<NllLossBackward0>)

10 0 tensor(0.7109, device='cuda:0', grad_fn=<NllLossBackward0>)

10 100 tensor(0.5005, device='cuda:0', grad_fn=<NllLossBackward0>)

10 200 tensor(1.0892, device='cuda:0', grad_fn=<NllLossBackward0>)

10 300 tensor(0.9554, device='cuda:0', grad_fn=<NllLossBackward0>)

10 400 tensor(0.1597, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.1497357913804449, 'recall': 0.13518998967570178, 'fmeasure': 0.13070487351713822}
Rouge_2: {'precision': 0.014090469279148525, 'recall': 0.012502625626931954, 'fmeasure': 0.012055140512182453}
Rouge_L: {'precision': 0.12549869887677118, 'recall': 0.11488422600375996, 'fmeasure': 0.1103108387244477}

BLEU Scores: (test)
{'bleu-1': 0.08356172235493818, 'bleu-2': 0.000928128696313024, 'bleu-3': 1.859310039838155e-309, 'bleu-4': 1.295639703661331e-308, 'bleu': 8.666796516493253e-157, 'bleu-corp-1': 0.08356172235493818, 'bleu-corp-2': 0.000928128696313024, 'bleu-corp-3': 1.859310039838155e-309, 'bleu-corp-4': 1.295639703661331e-308, 'bleu-corp': 8.666796516493253e-157}

METEOR Scores: (test)
{'meteor': 0.10702963529402983}

GLEU Scores: (test)
{'gleu-1': 0.09076009985242214, 'gleu-2': 0.0013822655332089292, 'gleu-3': 0.0, 'gleu-4': 0.0, 'gleu': 0.025396131825547435, 'gleu-corp-1': 0.09076009985242214, 'gleu-corp-2': 0.0013822655332089292, 'gleu-corp-3': 0.0, 'gleu-corp-4': 0.0, 'gleu-corp': 0.025396131825547435}
