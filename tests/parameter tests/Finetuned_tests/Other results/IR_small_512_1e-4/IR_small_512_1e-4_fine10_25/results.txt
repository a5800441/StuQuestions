{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'IR_small_512_1e-4', 'EPOCH_SETTING': '25', 'NAME': 'IR_small_512_1e-4_fine10_25', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 10, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'tune', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(5.3198, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.3393, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.5685, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.2048, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.7225, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 0 3.5661727594116988

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19661325640362215, 'recall': 0.12431741761016524, 'fmeasure': 0.13617998398345996}
Rouge_2: {'precision': 0.009041929425515509, 'recall': 0.008196338303385581, 'fmeasure': 0.007609269261811634}
Rouge_L: {'precision': 0.16737803776162388, 'recall': 0.10601169034381981, 'fmeasure': 0.11532800348876501}

2 0 tensor(2.7947, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.2401, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.9537, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.7763, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(2.9008, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 0 3.574627361055148

ROUGE Scores: (val)
Rouge_1: {'precision': 0.1503469852869024, 'recall': 0.11254005047230635, 'fmeasure': 0.11920174066286186}
Rouge_2: {'precision': 0.006944444444444444, 'recall': 0.005084745762711864, 'fmeasure': 0.005774138995165571}
Rouge_L: {'precision': 0.12691321151663812, 'recall': 0.095550460895872, 'fmeasure': 0.10034745648443358}

3 0 tensor(2.4191, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.8675, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.4890, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.5480, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.4659, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 0 3.7610364808874617

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2876101299830113, 'recall': 0.10963663780081229, 'fmeasure': 0.14183458788216718}
Rouge_2: {'precision': 0.025786924939467312, 'recall': 0.007311339790153349, 'fmeasure': 0.01038135593220339}
Rouge_L: {'precision': 0.26612102586678854, 'recall': 0.09960196907970291, 'fmeasure': 0.1293021215276823}

4 0 tensor(2.2795, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(2.7815, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(2.0690, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.3333, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.6070, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 0 3.8906936241408525

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19425412529783628, 'recall': 0.1213282969032654, 'fmeasure': 0.13639725580211812}
Rouge_2: {'precision': 0.018282454224627705, 'recall': 0.011140122391237468, 'fmeasure': 0.013240455401472352}
Rouge_L: {'precision': 0.1689331656548427, 'recall': 0.10402229908066515, 'fmeasure': 0.11702064530408655}

5 0 tensor(2.5015, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(1.3680, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.7278, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(1.5081, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(1.1210, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 0 4.118764444933099

ROUGE Scores: (val)
Rouge_1: {'precision': 0.15882333952226105, 'recall': 0.10545030873996777, 'fmeasure': 0.11007625328412364}
Rouge_2: {'precision': 0.008762892608723536, 'recall': 0.005822199393721669, 'fmeasure': 0.006643554558000874}
Rouge_L: {'precision': 0.14343552883981373, 'recall': 0.0934748982477393, 'fmeasure': 0.09811102264677785}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.14041472097944438, 'recall': 0.08889316702473739, 'fmeasure': 0.09799041931567003}
Rouge_2: {'precision': 0.00803337124091841, 'recall': 0.0060795881455234305, 'fmeasure': 0.006256001125590472}
Rouge_L: {'precision': 0.12015997848041192, 'recall': 0.07451797066716767, 'fmeasure': 0.08303640507117935}

1 0 tensor(5.6689, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(5.2222, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(5.1915, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(3.1140, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.5770, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 10 3.491541945352787

ROUGE Scores: (val)
Rouge_1: {'precision': 0.19215401405860102, 'recall': 0.13867470742661678, 'fmeasure': 0.14620413934634444}
Rouge_2: {'precision': 0.026498176620127836, 'recall': 0.01396920997661668, 'fmeasure': 0.016732346245547963}
Rouge_L: {'precision': 0.15956701917794158, 'recall': 0.11613638831577121, 'fmeasure': 0.12142481019907912}

2 0 tensor(1.2104, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(2.8305, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(1.9751, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(3.5086, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(1.4650, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 10 3.5355601543333472

ROUGE Scores: (val)
Rouge_1: {'precision': 0.17357125950014646, 'recall': 0.11260826013619825, 'fmeasure': 0.12450262096157681}
Rouge_2: {'precision': 0.03152034777752138, 'recall': 0.016904824781282895, 'fmeasure': 0.02054291133301676}
Rouge_L: {'precision': 0.14530385000067092, 'recall': 0.0921129666882591, 'fmeasure': 0.10252402703291454}

3 0 tensor(1.6729, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(1.3005, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.4233, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(2.4999, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(1.0861, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 10 3.6095873233748645

ROUGE Scores: (val)
Rouge_1: {'precision': 0.21370776823440088, 'recall': 0.13168051917485143, 'fmeasure': 0.1474538805406674}
Rouge_2: {'precision': 0.0357183805354537, 'recall': 0.017196611679488612, 'fmeasure': 0.021323615268441263}
Rouge_L: {'precision': 0.18069233874120946, 'recall': 0.11510358082579263, 'fmeasure': 0.12706216726591385}

4 0 tensor(1.9964, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(1.3007, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(1.6492, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(2.5118, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.2191, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 4 10 4.000112622249417

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2350141565793231, 'recall': 0.1205948346129639, 'fmeasure': 0.13857094177640572}
Rouge_2: {'precision': 0.041894995248653796, 'recall': 0.019017448116348604, 'fmeasure': 0.023534531200796658}
Rouge_L: {'precision': 0.20469457778413452, 'recall': 0.10535064007975319, 'fmeasure': 0.12140833843761399}

5 0 tensor(2.4341, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(0.7743, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(1.3943, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(0.7165, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(0.7397, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 5 10 4.269323604862865

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2126944850526012, 'recall': 0.12280296926073649, 'fmeasure': 0.13755849740077464}
Rouge_2: {'precision': 0.03241414724367983, 'recall': 0.014435067703383528, 'fmeasure': 0.016956663312215813}
Rouge_L: {'precision': 0.18440840656887517, 'recall': 0.10250963480555458, 'fmeasure': 0.11660304310204316}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.22684636423675444, 'recall': 0.13389521181073852, 'fmeasure': 0.14657967643908695}
Rouge_2: {'precision': 0.05295712039898086, 'recall': 0.016830858844378526, 'fmeasure': 0.02204116617810103}
Rouge_L: {'precision': 0.20429581026840613, 'recall': 0.1104970643428236, 'fmeasure': 0.12589069680791112}

1 0 tensor(6.6639, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(2.7510, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.5544, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(5.4485, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(4.8567, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 1 20 3.395586031978413

ROUGE Scores: (val)
Rouge_1: {'precision': 0.22913777879604083, 'recall': 0.11879227161583472, 'fmeasure': 0.13494752710125815}
Rouge_2: {'precision': 0.05248343934784613, 'recall': 0.011773297038271218, 'fmeasure': 0.01749497284965414}
Rouge_L: {'precision': 0.18733909061151016, 'recall': 0.09308156998585186, 'fmeasure': 0.10602807430230961}

2 0 tensor(3.5982, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.2067, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(3.7608, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(2.5831, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(3.2252, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 2 20 3.474412352351819

ROUGE Scores: (val)
Rouge_1: {'precision': 0.2157048583249956, 'recall': 0.09322238060050483, 'fmeasure': 0.11309488100647953}
Rouge_2: {'precision': 0.07067221855357449, 'recall': 0.011286944891018622, 'fmeasure': 0.017638794448146013}
Rouge_L: {'precision': 0.20281248581582131, 'recall': 0.08806766199239639, 'fmeasure': 0.10598266189334826}

3 0 tensor(1.8407, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(2.9194, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(2.4653, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.8580, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(0.9633, device='cuda:0', grad_fn=<NllLossBackward0>)

Validation: 3 20 3.4375027094857167

ROUGE Scores: (val)
Rouge_1: {'precision': 0.18270614928732679, 'recall': 0.094123373915619, 'fmeasure': 0.10739198172605152}
Rouge_2: {'precision': 0.05175721790351586, 'recall': 0.011668378422441176, 'fmeasure': 0.016693992568438197}
Rouge_L: {'precision': 0.1630599230287009, 'recall': 0.08186206449906563, 'fmeasure': 0.09425728404938442}

ROUGE Scores: (test)
Rouge_1: {'precision': 0.16256409764863872, 'recall': 0.10747167146375951, 'fmeasure': 0.11304388675058495}
Rouge_2: {'precision': 0.016460905349794237, 'recall': 0.007910571636061832, 'fmeasure': 0.010050648939537828}
Rouge_L: {'precision': 0.1353617732240921, 'recall': 0.08993883326098669, 'fmeasure': 0.09426453805025431}

avg_f1_1: 0.1414119985080309

avg_f1_2: 0.018137927016805007

avg_f1_L: 0.12079745436530193
