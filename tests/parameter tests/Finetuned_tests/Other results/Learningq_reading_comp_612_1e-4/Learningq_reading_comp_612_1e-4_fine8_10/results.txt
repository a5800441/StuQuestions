{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_reading_comp_612_1e-4', 'EPOCH_SETTING': '10', 'NAME': 'Learningq_reading_comp_612_1e-4_fine8_10', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 8, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 1e-05, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(4.8956, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(4.1646, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.3467, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(4.4455, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.3268, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(3.8708, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.5377, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(5.6583, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.3757, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(4.3177, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(3.3438, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.8541, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(4.6310, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(4.4081, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.7943, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(3.4471, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(4.2921, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(3.6115, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(4.2025, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(3.4217, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(3.4043, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(3.7817, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(3.9131, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(3.5305, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(4.6043, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(2.8793, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.8771, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(3.5661, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(1.8780, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(4.1480, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(2.9302, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(3.7617, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(4.2275, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(3.7637, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(2.8048, device='cuda:0', grad_fn=<NllLossBackward0>)

8 0 tensor(4.5188, device='cuda:0', grad_fn=<NllLossBackward0>)

8 100 tensor(4.3550, device='cuda:0', grad_fn=<NllLossBackward0>)

8 200 tensor(3.2289, device='cuda:0', grad_fn=<NllLossBackward0>)

8 300 tensor(3.1316, device='cuda:0', grad_fn=<NllLossBackward0>)

8 400 tensor(1.2168, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.22513550390908887, 'recall': 0.13333603362014773, 'fmeasure': 0.15618322656502576}
Rouge_2: {'precision': 0.029574861567092423, 'recall': 0.018743481212813533, 'fmeasure': 0.021554042516861605}
Rouge_L: {'precision': 0.1924893502723691, 'recall': 0.11485278942935637, 'fmeasure': 0.13413920343105565}

BLEU Scores: (test)
{'bleu-1': 0.08256320614063417, 'bleu-2': 0.010060458252885715, 'bleu-3': 1.837092316580705e-309, 'bleu-4': 9.95203205477175e-309, 'bleu': 5.218493967623858e-156, 'bleu-corp-1': 0.08256320614063417, 'bleu-corp-2': 0.010060458252885715, 'bleu-corp-3': 1.837092316580705e-309, 'bleu-corp-4': 9.95203205477175e-309, 'bleu-corp': 5.218493967623858e-156}

METEOR Scores: (test)
{'meteor': 0.11601419592708893}

GLEU Scores: (test)
{'gleu-1': 0.10109685093498705, 'gleu-2': 0.01144548919243703, 'gleu-3': 0.0, 'gleu-4': 0.0, 'gleu': 0.03162166032322793, 'gleu-corp-1': 0.10109685093498705, 'gleu-corp-2': 0.01144548919243703, 'gleu-corp-3': 0.0, 'gleu-corp-4': 0.0, 'gleu-corp': 0.03162166032322793}
