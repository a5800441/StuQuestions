{'python version': '3.8.16 (default, Jan 17 2023, 23:13:24) \n[GCC 11.2.0]', 'OS': 'Linux', 'pytorch': '1.10.1+cu113'}
{'MODEL': 'Learningq_reading_comp_612_1e-4', 'EPOCH_SETTING': '', 'NAME': 'Learningq_reading_comp_612_1e-4_fine7_e4', 'TRAIN_BATCH_SIZE': 1, 'VALID_BATCH_SIZE': 1, 'TEST_BATCH_SIZE': 1, 'TRAIN_EPOCHS': 7, 'VAL_EPOCHS': 1, 'LEARNING_RATE': 0.0001, 'MAX_SOURCE_TEXT_LENGTH': 512, 'MAX_TARGET_TEXT_LENGTH': 64, 'SEED': 42, 'type': 'prefix', 'mode': 'test', 'input_dim': '768*20', 'hidden': '800', 'output_dim': '768*100', 'DESC': 'Test'}

1 0 tensor(4.8956, device='cuda:0', grad_fn=<NllLossBackward0>)

1 100 tensor(3.9669, device='cuda:0', grad_fn=<NllLossBackward0>)

1 200 tensor(3.6331, device='cuda:0', grad_fn=<NllLossBackward0>)

1 300 tensor(4.1460, device='cuda:0', grad_fn=<NllLossBackward0>)

1 400 tensor(2.3594, device='cuda:0', grad_fn=<NllLossBackward0>)

2 0 tensor(2.8568, device='cuda:0', grad_fn=<NllLossBackward0>)

2 100 tensor(3.3520, device='cuda:0', grad_fn=<NllLossBackward0>)

2 200 tensor(4.9252, device='cuda:0', grad_fn=<NllLossBackward0>)

2 300 tensor(4.0587, device='cuda:0', grad_fn=<NllLossBackward0>)

2 400 tensor(4.0963, device='cuda:0', grad_fn=<NllLossBackward0>)

3 0 tensor(2.9150, device='cuda:0', grad_fn=<NllLossBackward0>)

3 100 tensor(3.3146, device='cuda:0', grad_fn=<NllLossBackward0>)

3 200 tensor(3.0628, device='cuda:0', grad_fn=<NllLossBackward0>)

3 300 tensor(3.6773, device='cuda:0', grad_fn=<NllLossBackward0>)

3 400 tensor(3.3280, device='cuda:0', grad_fn=<NllLossBackward0>)

4 0 tensor(3.5134, device='cuda:0', grad_fn=<NllLossBackward0>)

4 100 tensor(3.9996, device='cuda:0', grad_fn=<NllLossBackward0>)

4 200 tensor(3.2987, device='cuda:0', grad_fn=<NllLossBackward0>)

4 300 tensor(3.0416, device='cuda:0', grad_fn=<NllLossBackward0>)

4 400 tensor(2.7832, device='cuda:0', grad_fn=<NllLossBackward0>)

5 0 tensor(1.8853, device='cuda:0', grad_fn=<NllLossBackward0>)

5 100 tensor(2.8158, device='cuda:0', grad_fn=<NllLossBackward0>)

5 200 tensor(2.9680, device='cuda:0', grad_fn=<NllLossBackward0>)

5 300 tensor(2.0319, device='cuda:0', grad_fn=<NllLossBackward0>)

5 400 tensor(2.6398, device='cuda:0', grad_fn=<NllLossBackward0>)

6 0 tensor(2.0530, device='cuda:0', grad_fn=<NllLossBackward0>)

6 100 tensor(1.0403, device='cuda:0', grad_fn=<NllLossBackward0>)

6 200 tensor(1.1160, device='cuda:0', grad_fn=<NllLossBackward0>)

6 300 tensor(0.6361, device='cuda:0', grad_fn=<NllLossBackward0>)

6 400 tensor(3.1779, device='cuda:0', grad_fn=<NllLossBackward0>)

7 0 tensor(2.2489, device='cuda:0', grad_fn=<NllLossBackward0>)

7 100 tensor(3.2224, device='cuda:0', grad_fn=<NllLossBackward0>)

7 200 tensor(2.4827, device='cuda:0', grad_fn=<NllLossBackward0>)

7 300 tensor(0.9860, device='cuda:0', grad_fn=<NllLossBackward0>)

7 400 tensor(1.8246, device='cuda:0', grad_fn=<NllLossBackward0>)

ROUGE Scores: (test)
Rouge_1: {'precision': 0.1271006771006771, 'recall': 0.07840398338895926, 'fmeasure': 0.08883512411029006}
Rouge_2: {'precision': 0.006551362683438155, 'recall': 0.005817610062893082, 'fmeasure': 0.006113137078730864}
Rouge_L: {'precision': 0.12166606977927732, 'recall': 0.07442565040119231, 'fmeasure': 0.08425250975708934}

BLEU Scores: (test)
{'bleu-1': 0.05089911436586873, 'bleu-2': 8.323318730737166e-309, 'bleu-3': 1.13254288796665e-309, 'bleu-4': 8.323318730737166e-309, 'bleu': 4.085295985227897e-232, 'bleu-corp-1': 0.05089911436586873, 'bleu-corp-2': 8.323318730737166e-309, 'bleu-corp-3': 1.13254288796665e-309, 'bleu-corp-4': 8.323318730737166e-309, 'bleu-corp': 4.085295985227897e-232}

METEOR Scores: (test)
{'meteor': 0.07789156238869173}

GLEU Scores: (test)
{'gleu-1': 0.06275477236333898, 'gleu-2': 0.0, 'gleu-3': 0.0, 'gleu-4': 0.0, 'gleu': 0.01768526767581461, 'gleu-corp-1': 0.06275477236333898, 'gleu-corp-2': 0.0, 'gleu-corp-3': 0.0, 'gleu-corp-4': 0.0, 'gleu-corp': 0.01768526767581461}
