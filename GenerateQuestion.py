'''
Script to generate questions and test models
Created by: Adam Vere
Date: 07 June 2023

'''

import sys
import os
import platform

currentOS = platform.system() # Get current OS

path = ""

sys.path.insert(0, path)

import torch
from torch.utils.data import Dataset, DataLoader
import pandas as pd
from transformers.models.t5 import T5Tokenizer, T5ForConditionalGeneration
#from transformers.models.t5_alt import T5Tokenizer, T5ForConditionalGeneration # Without prefix-tuning
import pickle
from pickle import load
import random
import json
import numpy as np
import metrics

metrics.main(path)

'''
Dataset class From Ros Et al.
dataframe = dataset
tokenizer = T5tokenizer used to tokenize both the source and target input
source_len = max context length in tokens
target_len = max question length in tokens
source_text = key for context ('context', 'document')
target_text = key for question ('question', 'query')
'''
class DataSet(Dataset):
  def __init__(self, dataframe, tokenizer, source_len, target_len, source_text, target_text):
    self.tokenizer = tokenizer
    self.data = dataframe
    self.source_len = source_len
    self.summ_len = target_len
    self.target_text = self.data[target_text] # Gold Question
    self.source_text = self.data[source_text] # Context

  def __len__(self):
    return len(self.target_text)

  def __getitem__(self, index):
    source_text = str(self.source_text[index])
    target_text = str(self.target_text[index])

    source_text = ' '.join(source_text.split())
    target_text = ' '.join(target_text.split())

    source = self.tokenizer.batch_encode_plus([source_text], max_length= self.source_len, pad_to_max_length=True, truncation=True, padding="max_length", return_tensors='pt')
    target = self.tokenizer.batch_encode_plus([target_text], max_length= self.summ_len, pad_to_max_length=True, truncation=True, padding="max_length", return_tensors='pt')
    
    source_ids = source['input_ids'].squeeze()
    source_mask = source['attention_mask'].squeeze()
    source_mask = torch.ones(100 + self.source_len)
    target_ids = target['input_ids'].squeeze()
    target_mask = target['attention_mask'].squeeze()

    return {
        'source_ids': source_ids.to(dtype=torch.long), 
        'source_mask': source_mask.to(dtype=torch.int8), 
        'target_ids': target_ids.to(dtype=torch.long),
        'target_ids_y': target_ids.to(dtype=torch.long)
    }

'''
Function to load the datasets
'''
def loadData(model_params, tokenizer, mode):
  # Paths to the datasets
  learning_q = path + "/data/QC_Datasets/learningq_data.json"
  ir_small = next(os.walk(path + "/data/IR_Datasets/IR_small/"), (None, None, []))[2]
  reading_comp = [path + "/data/QC_Datasets/drop_data.json", path + "/data/QC_Datasets/narrativeqa_data.json", path + "/data/QC_Datasets/squad_v2_data.json", path + "/data/QC_Datasets/triviaqa_data1.json"]
  
  IR_Datasets = []
  QC_Datasets = []
  QC_Data = []
  IR_Data = []

  IR_training_set = None
  IR_val_set = None
  IR_test_set = None
  IR_training_loader = None
  IR_val_loader = None
  IR_test_loader = None

  QC_training_set = None
  QC_val_set = None
  QC_test_set = None
  QC_training_loader = None
  QC_val_loader = None
  QC_test_loader = None

  # Defining the parameters for creation of dataloaders
  train_params = {
      'batch_size': model_params["TEST_BATCH_SIZE"],
      'shuffle': True,
      'num_workers': 0,
      }

  val_params = {
      'batch_size': model_params["TEST_BATCH_SIZE"],
      'shuffle': False,
      'num_workers': 0,
      }

  test_params = {
      'batch_size': model_params["TEST_BATCH_SIZE"],
      'shuffle': False,
      'num_workers': 0,
      }

  if mode == 'QC':
    for dataset in model_params['QC_DATASETS']:
      if dataset == 'learning-q':
        QC_Datasets.append(learning_q)
      elif dataset == 'reading-comp':
        for set in reading_comp:
          QC_Datasets.append(set)
    
    test_questions = []
    test_context = []
    val_questions = []
    val_context = []
    train_questions = []
    train_context = []

    # Load dataset
    for dataset in QC_Datasets:
      file = json.load(open(dataset))
      print("loading dataset:", file['dataset'], file['length'])
      count = 0

      for entry in file['data']:
        if len(entry['question'].split()) <= 2 or len(entry['context'].split()) <= 20:
          count += 1
          continue
        QC_Data.append(entry)

      if (file['dataset'] == "triviaqa_Part1"):
        file1 = json.load(open(path + "/data/QC_Datasets/triviaqa_data2.json"))
        for entry in file1['data']:
          if len(entry['question'].split()) <= 2 or len(entry['context'].split()) <= 20:
            count += 1
            continue
        QC_Data.append(entry)

      # shuffle dataset
      random.shuffle(QC_Data)
      length = len(QC_Data)

      # split dataset and add to the three sets
      for entry in QC_Data[:int(0.1*length)]:
        test_questions.append(entry['question'])
        test_context.append(entry['context'])

      for entry in QC_Data[int(0.1*length):int(0.2*length)]:
        val_questions.append(entry['question'])
        val_context.append(entry['context'])

      for entry in QC_Data[int(0.2*length):]:
        train_questions.append(entry['question'])
        train_context.append(entry['context'])

      QC_Data = []
      print("entries ignored:", count)
      print("entries added:", (file['length']-count))
    
    # Create data frames
    test_dataset = pd.DataFrame({'context': test_context, 'question': test_questions})
    del test_questions
    del test_context

    val_dataset = pd.DataFrame({'context': val_context, 'question': val_questions})
    del val_questions
    del val_context

    train_dataset = pd.DataFrame({'context': train_context, 'question': train_questions})
    del train_questions
    del train_context

    print(f"TRAIN Dataset: {train_dataset.shape}")
    print(f"VAL Dataset: {val_dataset.shape}")
    print(f"TEST Dataset: {test_dataset.shape}\n")

    # Creating the Training and Validation dataset for further creation of Dataloader
    QC_training_set = DataSet(train_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "context", "question")
    QC_val_set = DataSet(val_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "context", "question")
    QC_test_set = DataSet(test_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "context", "question")
    
    QC_training_loader = DataLoader(QC_training_set, **train_params)
    QC_val_loader = DataLoader(QC_val_set, **val_params)
    QC_test_loader = DataLoader(QC_test_set, **test_params)

    del train_dataset
    del val_dataset
    del test_dataset

  if mode == 'IR':
     # Select IR datasets (i.e. IR-small, IR-large, msmarco-passage)
    for dataset in model_params['IR_DATASETS']:
      if dataset == 'IR-small':
        dir = path + "/data/IR_Datasets/IR_small/"
        for entry in ir_small:
          IR_Datasets.append(dir+entry)

    test_queries = []
    test_document = []
    val_queries = []
    val_document = []
    train_queries = []
    train_document = []

    # Load dataset
    for dataset in IR_Datasets:
      file = json.load(open(dataset))
      print("loading dataset:", file['dataset'], file['length'])
      count = 0

      for entry in file['data']:
        if len(entry['query'].split()) <= 2 or len(entry['document'].split()) <= 20:
          count += 1
          continue
        IR_Data.append(entry)

      # Shuffle data
      random.shuffle(IR_Data)
      length = len(IR_Data)

      # split the individual dataset and add to the three sets
      for entry in IR_Data[:int(0.1*length)]:
        test_queries.append(entry['query'])
        test_document.append(entry['document'])

      for entry in IR_Data[int(0.1*length):int(0.2*length)]:
        val_queries.append(entry['query'])
        val_document.append(entry['document'])

      for entry in IR_Data[int(0.2*length):]:
        train_queries.append(entry['query'])
        train_document.append(entry['document'])

      IR_Data = []
      print("entries ignored:", count)
      print("entries added:", (file['length']-count))
    
    # Create dataframes
    test_dataset = pd.DataFrame({'document': test_document, 'query': test_queries})
    del test_queries
    del test_document

    val_dataset = pd.DataFrame({'document': val_document, 'query': val_queries})
    del val_queries
    del val_document

    train_dataset = pd.DataFrame({'document': train_document, 'query': train_queries})
    del train_queries
    del train_document

    print(f"TRAIN Dataset: {train_dataset.shape}")
    print(f"VAL Dataset: {val_dataset.shape}")
    print(f"TEST Dataset: {test_dataset.shape}\n")

    # Creating the Training and Validation dataset for further creation of Dataloader
    IR_training_set = DataSet(train_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "document", "query")
    IR_val_set = DataSet(val_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "document", "query")
    IR_test_set = DataSet(test_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "document", "query")
    
    IR_training_loader = DataLoader(IR_training_set, **train_params)
    IR_val_loader = DataLoader(IR_val_set, **val_params)
    IR_test_loader = DataLoader(IR_test_set, **test_params)
    
    del train_dataset
    del val_dataset
    del test_dataset

  del IR_Data
  del QC_Data

  return QC_training_loader, QC_val_loader, QC_test_loader, IR_training_loader, IR_val_loader, IR_test_loader


# Generate questions given a document/context (Same as the test function)
def GenerateQuestions(tokenizer, model, device, loader):
  model.eval()
  questions = []
  goldQuestions = []
  
  with torch.no_grad():
      for _, data in enumerate(loader, 0):
          y = data['target_ids'].to(device, dtype = torch.long)
          ids = data['source_ids'].to(device, dtype = torch.long)
          mask = data['source_mask'].to(device, dtype = torch.int8)

          generated_ids = model.generate(
              input_ids = ids,
              attention_mask = mask, 
              max_length=150, 
              num_beams=2,
              repetition_penalty=2.5, 
              length_penalty=1.0, 
              early_stopping=True
              )
          
          # Decode tokenized generated and gold question
          question = [tokenizer.decode(g, skip_special_tokens=True, clean_up_tokenization_spaces=True) for g in generated_ids]
          target = [tokenizer.decode(t, skip_special_tokens=True, clean_up_tokenization_spaces=True)for t in y]

          questions.extend(question)
          goldQuestions.extend(target)

  # Return generated and gold questions 
  return questions, goldQuestions

# Print predictions saved to file (based on testset)
def Predictions(predictions, actuals): 
  preds = pickle.load(open(predictions, 'rb'))
  acts = pickle.load(open(actuals, 'rb'))

  for act, pred in zip(acts, preds):
     print("Ground Truth: " + act)
     print("Generated: " + pred + "\n")

'''
Main function to run the program
'''
def main():
    # Set random seeds
    random.seed(42)
    torch.manual_seed(42)
    np.random.seed(42)
    torch.backends.cudnn.deterministic = True
    
    # Flags to run tests
    flags={
      "printPredictions": False,        # Run the Predictions function for a model
      "TestLearningQ": False,           # Run the LearningQ test set
      "TestLearningReadingComp": False, # Run the Reading-comp  test set
      "TestIR": False,                  # Run the IR-small test set
      "TestSet": False,                 # Run the Lecture-transcript and questions test set
      "FullDataset": False,             # Run the full Lecture-transcript and questions dataset
      "Evaluation": True,               # Run the evaluation on model (used for I.Q.A)
      "Transcripts": True,             # Generate questions from the MIT transcripts
    }

    model_params={
      "MODEL":"IR_small_1024_1e-4_finetuned",      # Base model, (t5-base, doc2query-t5-base-msmarco or the name of the models' folder in the Models directory)
      "TEST_BATCH_SIZE":1,                          # validation batch size
      "MAX_SOURCE_TEXT_LENGTH":512,                 # max length of source text
      "MAX_TARGET_TEXT_LENGTH":64,                  # max length of target text
      "QC_DATASETS": ['learning-q'],                # Select QC Datasets (learning-q, reading-comp)
      "IR_DATASETS": ['IR-small'],                  # Select IR datasets (IR-small)
      "TYPE": '',                                   # Type of data (QC, IR)
    }
    
    # Select datasets
    if flags["TestLearningQ"] == True:
      model_params["QC_DATASETS"] = ['learning-q']
      model_params["TYPE"] = 'QC'
    elif flags["TestLearningReadingComp"] == True:
      model_params["QC_DATASETS"] = ['reading-comp']
      model_params["TYPE"] = 'QC'
    elif flags["TestIR"] == True:
      model_params["TYPE"] = 'IR'

    # Example questions and contexts from the Ros et al. paper
    text_1 = "except that you replace the unary prefix with the gamma code. So that's even less conservative than gamma code, in terms of avoiding the small integers. So that means it's okay if you occasionally see a large number. It's, it's, you know, it's okay with delta code. It's also fine with gamma code. It's really a big loss for unary code, and they are all operating, of course, at different degrees of favoring short favoring small integers. And that also means they would appropriate for sorting distribution. But none of them is perfect for all distributions. And which method works, the best would have to depend on the actual distribution in your data set. For inverted index, compression, people have found that gamma coding seems to work well."
    question_1 = "Does is the delta-code use gamma-code twice recursively?"

    text_2 = "this is basically the same uniform code and binary code are the same. And we're going to use this code to code the remaining part of the value of x. And this is basically, precisely, x minus 1, 2 to the flow of log of x. So the unary code or basically code with a flow of log of x, well, I added one there, and here. But the remaining part will, we using uniform code to actually code the difference between the x and"
    question_2 = "What is the reasoning for making the first (1+logx) unary and the x-2^(logx) uniform? The method for encoding seem random to me."

    text_3 = "We would first choose a diverse set of ranking methods, these are types of retrieval systems. And we hope these methods can help us nominate likely relevance in the documents. So the goal is to pick out the relevant documents.. It means we are to make judgements on relevant documents because those are the most useful documents from the users perspective. So, that way we would have each to return top-K documents. And the K can vary from systems, right. But the point is to ask them to suggest the most likely relevant documents. And then we simply combine all these top-K sets to form a pool of documents for human assessors to judge. So, imagine you have many systems. Each will return K documents, you know, take the top-K documents, and we form the unit. Now, of course there are many documents that are duplicated, because many systems might have retrieved the same random documents. So there will be some duplicate documents. And there are, there are also unique documents that are only returned by one system, so the idea of having diverse set of result ranking methods is to ensure the pool is broad. And can include as many possible random documents as possible. And then the users with the human assessors would make complete the judgements on this data set, this pool. And the other unjudged documents are usually just a assumed to be non-relevant. Now if the pool is large enough, this assumption is okay. But the, if the pool is not very large, this actually has to be reconsidered, and we might use other strategies to deal with them and there are indeed other methods to handle such cases. And such a strategy is generally okay for comparing systems that contribute to the pool. That means if you participate in contributing to the pool then it's unlikely that it will penalize your system because the top ranked documents have all been judged. However, this is problematic for even evaluating a new system that may not have contributed to the pool. In this case, you know, a new system might be penalized because it might have nominated some relevant documents that have not been judged. So those documents might be assumed to be non-relevant."
    question_3 = "What is the risk associated with discarding documents that are potentially relevant?"
    
    text_4 = "Opera refers to a dramatic art form, originating in Europe, in which the emotional content is conveyed to the audience as much through music, both vocal and instrumental, as it is through the lyrics. By contrast, in musical theater an actor's dramatic performance is primary, and the music plays a lesser role. The drama in opera is presented using the primary elements of theater such as scenery, costumes, and acting. However, the words of the opera, or libretto, are sung rather than spoken. The singers are accompanied by a musical ensemble ranging from a small instrumental ensemble to a full symphonic orchestra."
    question_4 = "How does opera convey the emotional content?"
    
    test_params = {
          'batch_size': 1,
          'shuffle': False,
          'num_workers': 0
          }

    modelPath =  path + "/Models/" + model_params["MODEL"] # Path to the model
    outputPath_base = path + "/output/" + model_params["MODEL"]
    
    tokenizer = T5Tokenizer.from_pretrained(modelPath, local_files_only=True)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    modelInfo = None
    if model_params["MODEL"] == 'doc2query-t5-base-msmarco' or model_params["MODEL"] == 't5-base' or os.path.exists(modelPath + "/pytorch_model.bin") == True:
      model = T5ForConditionalGeneration.from_pretrained(modelPath, local_files_only=True)
    else:
      modelInfo = torch.load(modelPath + "/" + model_params["MODEL"] + '.pth') #["model_state_dict"]
      model = T5ForConditionalGeneration.from_pretrained(pretrained_model_name_or_path=None, config=modelPath + "/" + "config.json", state_dict=modelInfo["model_state_dict"], local_files_only=True)
      model.save_pretrained(modelPath)
    model = model.to(device)
    del modelInfo
    
    QC_training_loader, QC_val_loader, QC_test_loader, IR_training_loader, IR_val_loader, IR_test_loader = loadData(model_params, tokenizer, model_params['TYPE'])

    # Test 
    if flags["TestLearningQ"] == True or flags["TestLearningReadingComp"] == True or flags["TestIR"] == True:
      outputPath = outputPath_base + '_test' + "/"

      # Create output folder if it does not exist already
      if os.path.exists(outputPath) == False:
        os.mkdir(outputPath)

      # Create results text file
      file = open(outputPath + "results.txt", "w")
      file.write(str(model_params) + "\n")
      file.close()

      # Generate questions from text sets
      if flags["TestIR"] == False:
        preds, acts = GenerateQuestions(tokenizer, model, device, QC_test_loader)
      else:
        preds, acts = GenerateQuestions(tokenizer, model, device, IR_test_loader)

      # Genertae automatic metrics
      metrics.rougeScore(preds, acts, model_params['TYPE'], outputPath)
      metrics.bleuScore(preds, acts, model_params['TYPE'], outputPath)
      metrics.meteorScore(preds, acts, model_params['TYPE'], outputPath)
      metrics.gleuScore(preds, acts, model_params['TYPE'], outputPath)

    # Get lecture-question pairs
    test_pairs = load(open(path + '/data/text_pairs.pkl', 'rb'))
    shuff_lec_names = list(test_pairs.keys())
    random.shuffle(shuff_lec_names)

    # Create test set from lecture-question pairs
    test_questions = []
    test_lecture_text = []
    if flags["FullDataset"] == True:
      for lecture in shuff_lec_names:
        for question_unit in test_pairs[lecture]:
            test_questions.append(question_unit['text'])
            test_lecture_text.append(question_unit['lecture_text'])
      test_dataset = pd.DataFrame({'lecture': test_lecture_text, 'question': test_questions})
    else:
      count = 0
      for lecture in shuff_lec_names[85:]:
        for question_unit in test_pairs[lecture]:
            test_questions.append(question_unit['text'])
            test_lecture_text.append(question_unit['lecture_text'])
            count += 1

      test_dataset = pd.DataFrame({'lecture': test_lecture_text, 'question': test_questions})

    # Run tests on the full dataset or test set
    if flags["FullDataset"] == True or flags["TestSet"] == True:
      test_set = DataSet(test_dataset, tokenizer, model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], 'lecture', 'question')
      test_loader = DataLoader(test_set, **test_params)
      del test_dataset
      
      # Generate questions
      generated, goldQuestion = GenerateQuestions(tokenizer, model, device, test_loader)
      outputPath = outputPath_base +  '_RosTest' + "/"
      if os.path.exists(outputPath) == False:
        os.mkdir(outputPath)
      file = open(outputPath + "results.txt", "w")
      file.write(str(model_params) + "\n")
      file.close()
      metrics.rougeScore(generated, goldQuestion, 'Test', outputPath)
      metrics.bleuScore(generated, goldQuestion, 'Test', outputPath)
      metrics.meteorScore(generated, goldQuestion, 'Test', outputPath)
      metrics.gleuScore(generated, goldQuestion, 'Test', outputPath)
    
    # I.Q.A evaluation (generate questions from the random 10 samples of lecture-question pairs and LearningQ test set)
    if flags["Evaluation"] == True:
      outputPath = outputPath_base +  '_Evaluation' + "/"
      if os.path.exists(outputPath) == False:
        os.mkdir(outputPath)
      file = open(outputPath + "results.txt", "w")
      file.write(str(model_params) + "\n")

      file.write("4 Example questions \n")
      # Test data
      data = pd.DataFrame({'lecture': [text_1, text_2, text_3, text_4], 'question': [question_1, question_2, question_3, question_4]})
      
      dataset = DataSet(data, tokenizer,  model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "lecture", "question")
      loader = DataLoader(dataset, **test_params)
      
      # Generate questions
      generated, goldQuestion = GenerateQuestions(tokenizer, model, device, loader)

      for i in range(0, len(generated)):
        file.write(str(generated[i]) + "\n")

      file.write("\nLecture-question test set \n")
      jsonFile = json.load(open(path + "/data/LectureTestSet.json"))
      test_questions = []
      test_lecture_text = []
      for entry in jsonFile['data']:
        test_questions.append(entry['question'])
        test_lecture_text.append(entry['context'])
      # Test data
      data = pd.DataFrame({'lecture': test_lecture_text, 'question': test_questions})
      
      dataset = DataSet(data, tokenizer,  model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "lecture", "question")
      loader = DataLoader(dataset, **test_params)
      
      # Generate questions
      generated, goldQuestion = GenerateQuestions(tokenizer, model, device, loader)

      for i in range(0, len(generated)):
        file.write("\nContext: " + test_lecture_text[i] + "\n")
        file.write("\nGold question: " + goldQuestion[i] + "\n")
        file.write("Generated Question: " + generated[i] + "\n")

      file.write("\nLearningQ test set \n")
      jsonFile = json.load(open(path + "/data/LearningQTest.json"))
      test_questions = []
      test_lecture_text = []
      for entry in jsonFile['data']:
        test_questions.append(entry['question'])
        test_lecture_text.append(entry['context'])
      # Test data
      data = pd.DataFrame({'lecture': test_lecture_text, 'question': test_questions})
      
      dataset = DataSet(data, tokenizer,  model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "lecture", "question")
      loader = DataLoader(dataset, **test_params)
      
      # Generate questions
      generated, goldQuestion = GenerateQuestions(tokenizer, model, device, loader)

      for i in range(0, len(generated)):
        file.write("\nContext: " + test_lecture_text[i] + "\n")
        file.write("\nGold question: " + goldQuestion[i] + "\n")
        file.write("Generated Question: " + generated[i] + "\n")

      file.close()
    
    # Generate questions from the MIT lecture transcript snippets
    if flags["Transcripts"] == True:
      outputPath = outputPath_base +  '_Transcripts' + "/"
      if os.path.exists(outputPath) == False:
        os.mkdir(outputPath)
      file = open(outputPath + "results.txt", "w")
      file.write(str(model_params) + "\n")

      jsonFile = json.load(open(path + "/data/Transcripts.json"))
      test_questions = []
      test_lecture_text = []
      for entry in jsonFile['snippests']:
        test_questions.append("")
        test_lecture_text.append(entry)
      # Test data
      data = pd.DataFrame({'lecture': test_lecture_text, 'question': test_questions})
      
      dataset = DataSet(data, tokenizer,  model_params["MAX_SOURCE_TEXT_LENGTH"], model_params["MAX_TARGET_TEXT_LENGTH"], "lecture", "question")
      loader = DataLoader(dataset, **test_params)
      
      # Generate questions
      generated, goldQuestion = GenerateQuestions(tokenizer, model, device, loader)

      for i in range(0, len(generated)):
        file.write("\nContext: " + test_lecture_text[i] + "\n")
        file.write("Generated Question: " + generated[i] + "\n")

    # Print out saved predictions if flag is True
    if (flags["printPredictions"] == True):
      Predictions(modelPath + "/" + model_params["MODEL"] + "_preds.pkl", modelPath + "/" + model_params["MODEL"] + "_acts.pkl")
    
if __name__ == "__main__":
  main()